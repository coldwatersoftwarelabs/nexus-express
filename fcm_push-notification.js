const FCM = require('fcm-node')
const serverKey = 'AAAAHINb_wI:APA91bE-Lv2at3vhloRql_hDzGp_CUY7pHZgGq0XDS7y3oeuLa5rNJC4P7YYAQ7-Lk2qM24brI9Hg8by344MZtGJtX9pxa3rlbYNfGacZro8YhvmMuKv-P7b2jP75OA_Fmd38W2hApzj' // put your server key here
const fcm = new FCM(serverKey)

const sendNotification = (token, title, messageInfo, type) => {
    return new Promise((resolve, reject) => {
        const message = { // this may consty according to the message type (single recipient, multicast, topic, et cetera)
            priority: 'high',
            silent: false,
            notification: {
                title: title,
                body: messageInfo
            }
        };

        (type === 'bulk') ? message.registration_ids = token: message.to = token[0]

        fcm.send(message, function (err, response) {
            if (err) {
                reject({
                    type: 'failure',
                    msg: 'Something has gone wrong!'
                })
            } else {
                resolve({
                    type: 'success',
                    msg: 'Successfully sent!'
                })
            }
        })
    })
}

module.exports = {
    sendNotification
}
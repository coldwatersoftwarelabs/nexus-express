// const createError = require('http-errors')
const express = require('express')
// const session = require('express-session')
const path = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
// const jwt = require('jsonwebtoken')
// const config = require('./configurations/config')
const cors = require('cors')
const logger = require('morgan')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./src/swagger.json')
// const middleware = require('./middleware')
const routes = require('./src/routes/index')
const Label = require('./src/routes/Label')
const Bible = require('./src/routes/Bible')
const Master = require('./src/routes/Master')
const Media = require('./src/routes/Media')
const Menu = require('./src/routes/Menu')
const Page = require('./src/routes/Page')
const Event = require('./src/routes/Event')
const ImageUpload = require('./src/routes/ImageUpload')
const Church = require('./src/routes/Church')
const Help = require('./src/routes/Help')
const Mail = require('./src/routes/Mail')
const Access = require('./src/routes/Access')
const Updates = require('./src/routes/Updates')
const Prayer = require('./src/routes/Prayer')
const PlanDetails = require('./src/routes/PlanDetails')
const Appfeatures = require('./src/routes/Appfeatures')
const Testimony = require('./src/routes/Testimony')
const Download = require('./src/routes/Download')
const Notify = require('./src/routes/Notification')
const Generalization = require('./src/routes/Generalization')

var Logs = require('logger').createLogger('development.log') // logs to a file

const app = express()
// app.set('Secret', config.secret)
// app.use(session({
//   secret: process.env.SECRET_KEY,
//   resave: true,
//   saveUninitialized: true
// }))

const bodyParserJSON = bodyParser.json()
const bodyParserURLEncoded = bodyParser.urlencoded({
  extended: true
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))
app.use(bodyParserJSON)
app.use(bodyParserURLEncoded)
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Credentials', 'true')
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization')
  next()
})

const apiV1Html = swaggerUi.generateHTML(
  swaggerDocument
)
app.use(
  '/swagger',
  swaggerUi.serveFiles(swaggerDocument)
)
app.get('/swagger', (req, res) => {
  res.send(apiV1Html)
})

// app.use('/testing', swaggerUi2.serve, swaggerUi2.setup(swaggerDocument2))
// app.use('/swagger2', swaggerUi.serve, swaggerUi.setup(swaggerDocument2))
// app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use('/user', routes)
app.use('/access', Access)
app.use('/Label', Label)
app.use('/Bible', Bible)
app.use('/Master', Master)
app.use('/Menu', Menu)
app.use('/Media', Media)
app.use('/Page', Page)
app.use('/Event', Event)
app.use('/upload', ImageUpload)
app.use('/Church', Church)
app.use('/Help', Help)
app.use('/Mail', Mail)
app.use('/Updates', Updates)
app.use('/Prayer', Prayer)
app.use('/BiblePlan', PlanDetails)
app.use('/Appfeatures', Appfeatures)
app.use('/Testimony', Testimony)
app.use('/Download', Download)
app.use('/Notify', Notify)
app.use('/Generalization', Generalization)

// app.use('/Phase2', middleware.checkToken, Phase2)
// catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   next(createError(404))
// })
// error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message
//   res.locals.error = req.app.get('env') === 'development' ? err : {}

//   // render the error page
//   res.status(err.status || 500)
//   res.render('error')
// })
Logs.debug('this wont be logged')

module.exports = app

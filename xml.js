const builder = require('xmlbuilder')
const fs = require('fs')
const _ = require('underscore')
const uploadService = require('./src/UploadService')
let dirPath = __dirname + '/..biblexml.xml'
const PlandirPath = __dirname + '/..planxml.xml'
const numberToWords = require('number-to-words')

const bibleTemplate = (bibleContent) => {
    return new Promise((resolve, reject) => {
        const xmlName = bibleContent.name.replace(/ /g, '_')
        const xml = builder.create({
            [xmlName]: bibleContent.content
        }, {
            separateArrayItems: true
        })
        resolve(xml)
    })
}

const planTemplate = (planContent) => {
    return new Promise((resolve, reject) => {
        var firstChar = planContent.plan.charAt(0)
        var conplanName = ''
        if (firstChar <= '9' && firstChar >= '0') {
            const fetchFirstAlpha = planContent.plan.match(/[a-zA-Z]/).pop()
            const conLen = planContent.plan.slice(0, planContent.plan.indexOf(fetchFirstAlpha)).length
            const conversion = numberToWords.toWords(planContent.plan.slice(0, planContent.plan.indexOf(fetchFirstAlpha))).replace(/-/g, '')
            conplanName = conversion + planContent.plan.substr(conLen)
        }
        const xmlName = (firstChar <= '9' && firstChar >= '0') ? conplanName.replace(/ /g, '') : planContent.plan.replace(/ /g, '')
        const xml = builder.create({
            [xmlName]: planContent.planDetails
        }, {
            separateArrayItems: true
        })
        resolve(xml)
    })
}

const generateXML = (data) => {
    return new Promise(async (resolve, reject) => {
        let xmlContent
        if (data.source === 'bible') {
            xmlContent = await bibleTemplate(data)
        } else {
            xmlContent = await planTemplate(data)
        }
        const xmldoc = xmlContent.toString({
            pretty: true
        })
        if (data.source === 'plan') dirPath = PlandirPath
        fs.writeFileSync(dirPath, xmldoc, function (err) {
            if (err) throw err
        })

        const fileLocation = await uploadService.uploadFile({
            originalname: (data.source === 'bible') ? 'biblexml.xml' : data.plan.replace(/ /g, '') + '.xml',
            buffer: (data.source === 'bible') ? fs.readFileSync('..biblexml.xml') : fs.readFileSync('..planxml.xml')
        }, 'word');
        (data.source === 'bible') ?
        fs.unlink('..biblexml.xml', (err) => {
            if (err) throw err
        }): fs.unlink('..planxml.xml', (err) => {
            if (err) throw err
        })
        resolve(fileLocation)
    })
}

module.exports = {
    generateXML
}
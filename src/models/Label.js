const db = require('../../dbconnection') // reference of dbconnection.js

const Label = {

  addLangType: function (payload, callback) {
    return db.query('INSERT INTO language(langType,OriginalLangtext) VALUES (?,?)',
      [payload.data.langType, payload.data.OriginalLangtext], callback)
  },
  addEnglishLabel: function (payload, callback) {
    return db.query('INSERT INTO englishlabel(langId,name,labelDesc, labelImage) VALUES (1,?,?,?)',
      [payload.name, payload.labelDesc, payload.labelImage], callback)
  },
  addAllLabel: (payload, callback) => {
    const addLabelQuery = 'INSERT INTO labellist (langId, labelName, engId) VALUES ?'
    return db.query(addLabelQuery, [payload], callback)
  },
  setDefaultLangId: function (payload, callback) {
    return db.query('ALTER TABLE labellist MODIFY COLUMN langId INT(11) NOT NULL DEFAULT ?;', [payload], callback)
  },
  addCommonEng: function (callback) {
    return db.query('INSERT INTO labellist (engId, labelName) SELECT id, name from englishlabel;', callback)
  },
  deleteLabelTypeData: function (id, callback) {
    return db.query('DELETE from language WHERE langId=?', [id], callback)
  },
  getLanguage: function (callback) {
    return db.query('SELECT * FROM language WHERE status=0', callback)
  },
  getLanguageExcerptEng: function (callback) {
    return db.query('SELECT * FROM language WHERE status=0 AND langId!=1', callback)
  },
  checkLang: function (payload, callback) {
    return db.query('SELECT * FROM language WHERE langId=? AND status=0', payload, callback)
  },
  getEnglishLabel: function (callback) {
    return db.query('SELECT id as engId, langId, id as labelId, name, name as labelName, labelDesc as appId, labelImage, status FROM englishlabel WHERE status=0', callback)
  },
  getEnglishLabelById: function (payload, callback) {
    return db.query('SELECT * FROM englishlabel WHERE status=0 AND id=?', payload, callback)
  },
  getAllLabel: function (callback) {
    return db.query('SELECT l.engId, l.langId, l.labelId, a.name, l.labelName, a.labelDesc as appId, a.labelImage, l.status FROM englishlabel as a INNER JOIN labellist as l ON l.engId = a.id WHERE a.status=0 AND l.status=0 ORDER BY l.labelId ASC', callback)
  },
  getAllLabelById: function (payload, callback) {
    return db.query('SELECT * FROM labellist WHERE status=0 AND labelId=?', payload, callback)
  },
  getAllLabelByLang: function (id, callback) {
    return db.query('SELECT l.engId, l.langId, l.labelId, a.name, l.labelName, a.labelDesc as appId, a.labelImage, l.status FROM englishlabel as a INNER JOIN labellist as l ON l.engId = a.id WHERE l.langId=? AND a.status=0 AND l.status=0 ORDER BY l.labelId ASC', [id], callback)
  },
  updateLangType: function (payload, callback) {
    return db.query('UPDATE language SET langType=?, OriginalLangtext=? WHERE langId=?',
      [payload.langType, payload.OriginalLangtext, payload.langId], callback)
  },
  updateEnglishLabel: function (payload, callback) {
    return db.query('UPDATE englishlabel SET name=?, langId=1, labelDesc=?, labelImage=? WHERE id=?',
      [payload.name, payload.labelDesc, payload.labelImage, payload.engId], callback)
  },
  updateAllLabel: function (payload, callback) {
    return db.query('UPDATE labellist SET langId=?, labelName=?, engId=? WHERE labelId=?',
      [payload.langId, payload.labelName, payload.engId, payload.labelId], callback)
  },
  langStatusForDelete: function (payload, callback) {
    return db.query('UPDATE language SET status=? WHERE langId=?',
      [payload.status, payload.langId], callback)
  },
  engLabelStatusForDelete: function (payload, callback) {
    return db.query('UPDATE englishlabel SET status=1 WHERE id=?',
      [payload.id], callback)
  },
  allLabelStatusForDelete: function (payload, callback) {
    return db.query('UPDATE labellist SET status=1 WHERE labelId=?',
      [payload.labelId], callback)
  }

}
module.exports = Label
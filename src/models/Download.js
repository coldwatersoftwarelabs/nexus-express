const db = require('../../dbconnection')

const Download = {
    getBibleTableName: (payload, callback) => {
        const getBibleDBNameQuery = 'SELECT b.*, l.langType FROM bible b, language l WHERE b.id = ? AND l.langId = b.language'
        return db.query(getBibleDBNameQuery, payload, callback)
    },

    getBibleContent: (payload, callback) => {
        const getBibleContentQuery = `SELECT b.name, b.id, bv.chapterId, bv.verseId, bv.verse FROM ${payload} bv, book b
            WHERE b.id = bv.bookId;`
        return db.query(getBibleContentQuery, callback)
    },

    updateBibleDownloadedUrl: (payload, callback) => {
        const updateUrlQuery = 'UPDATE bible SET xmlurl = ? WHERE id =?'
        db.query(updateUrlQuery, [payload.xmlurl, payload.bibleId], callback)
    },

    updatePlanDownloadedUrl: (payload, callback) => {
        const updateUrlQuery = 'UPDATE plandetails SET xmlurl = ? WHERE id =?'
        db.query(updateUrlQuery, [payload.xmlurl, payload.planId], callback)
    },

    getReadingPlan: (payload, callback) => {
        const getQuery = `SELECT p.*, rp.id as readingPlanId, rp.fromChapter, rp.toChapter, rp.fromVerse, rp.toVerse,rp.fromBook, b1.abbreName as fromAbb, b2.abbreName as toAbb, (select count(verseId) FROM bibleverse WHERE bookId=rp.fromBook AND chapterId=rp.fromChapter) as fromVerseCount,
        (select count(verseId) FROM bibleverse WHERE bookId=rp.toBook AND chapterId=rp.toChapter) as toVerseCount,
        rp.toBook,
        CASE
        WHEN p.byDate = 1 THEN pm.readingDate
        WHEN p.byDate = 0 THEN pm.readingDay
        END AS dayOrDate
        FROM planmodules pm, plandetails p,readingplan rp 
        JOIN book b1 ON b1.id = rp.fromBook
        JOIN book b2 ON b2.id = rp.toBook
        WHERE p.id = ? AND p.id = pm.planId
        AND rp.planModuleId = pm.id;`
        return db.query(getQuery, payload, callback)
    },
    selectByBibleId: function (payload, callback) {
        return db.query('SELECT * FROM bible WHERE id = ?', [payload], callback)
    },
    getXmlUrl: function (payload, callback) {
        return db.query('SELECT xmlurl FROM bible WHERE id = ? AND xmlurl IS NOT NULL', [payload], callback)
    },
    selectByPlanId: function (payload, callback) {
        return db.query('SELECT * FROM plandetails WHERE id = ?', [payload], callback)
    },
    getPlanXmlUrl: function (payload, callback) {
        return db.query('SELECT xmlurl FROM plandetails WHERE id = ? AND xmlurl IS NOT NULL', [payload], callback)
    }
}

module.exports = Download
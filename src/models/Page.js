const db = require('../../dbconnection') // reference of dbconnection.js

const Page = {

  addPage: function (payload, callback) {
    const today = new Date()
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    const dateTime = date + ' ' + time
    return db.query('INSERT INTO page (title, excerpt, content, parentId, showChild, bannerImageUrl, status, language, editedBy, addedBy, createdAt, editedAt, expire)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [payload.title, payload.excerpt, payload.content, payload.parentId, payload.showChild, payload.bannerImageUrl, payload.status, payload.language, payload.editedBy, payload.addedBy, dateTime, dateTime, payload.expire], callback)
  },
  viewAllPublishedPage: function (callback) {
    return db.query('SELECT page.ID, page.title, page.excerpt, page.content, page.parentId, page.showChild, page.bannerImageUrl, page.contentImageUrl, page.contentImagePostition, page.url, page.status, page.language, page.order, page.editedBy, page.addedBy, DATE_FORMAT(page.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(page.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, page.expire, language.langType FROM page INNER JOIN language ON language.langId=page.language WHERE page.status=\'Published\'', callback)
  },
  checkPageExistence: function (payload, callback) {
    return db.query('SELECT * FROM page where pageId=?', [payload], callback)
  },
  viewAllPage: function (callback) {
    return db.query('SELECT page.ID, page.title, page.excerpt, page.content, page.parentId, page.showChild, page.bannerImageUrl, page.contentImageUrl, page.contentImagePostition, page.url, page.status, page.language, page.order, page.editedBy, page.addedBy, DATE_FORMAT(page.createdAt, \'%Y-%m-%d\') as createdAt, DATE_FORMAT(page.editedAt, \'%Y-%m-%d\') as editedAt, page.expire, DATE_FORMAT(page.editedAt, \'%d-%m-%Y\') as updatedOn, language.langType, page.addedBy, user.firstname, user.lastname FROM page INNER JOIN language ON language.langId=page.language INNER JOIN user ON user.userId=page.addedBy', callback)
  },
  updatePage: function (ID, payload, callback) {
    const today = new Date()
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    const dateTime = date + ' ' + time
    return db.query('UPDATE page SET title=?, excerpt=?, content=?, parentId=?, showChild=?, bannerImageUrl=?, status=?, language=?, editedBy=?, editedAt=?, expire=? WHERE ID=?',
      [payload.title, payload.excerpt, payload.content, payload.parentId, payload.showChild, payload.bannerImageUrl, payload.status, payload.language, payload.editedBy, dateTime, payload.expire, ID], callback)
  },
  deletePage: function (id, callback) {
    return db.query('DELETE FROM page WHERE ID=?',
      [id], callback)
  }

}
module.exports = Page
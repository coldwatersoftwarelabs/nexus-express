const db = require('../../dbconnection') // reference of dbconnection.js

const Access = {

  addAccess: function (payload, callback) {
    return db.query('INSERT INTO access (name,status) VALUES (?,?)',
      [payload.name, payload.status], callback)
  },
  getAccess: function (callback) {
    return db.query('SELECT * FROM access', callback)
  },
  editAccess: function (payload, callback) {
    for (let i = 0; i < payload.access.length; i++) {
      if (payload.access.length - 1 === i) {
        return db.query('UPDATE access SET name=?, status=? WHERE ID=?',
          [payload.access[i].name, payload.access[i].status, payload.answers[i].ID], callback)
      } else {
        db.query('UPDATE access SET name=?, status=? WHERE ID=?',
          [payload.access[i].name, payload.access[i].status, payload.answers[i].ID])
      }
    }
  }

}
module.exports = Access

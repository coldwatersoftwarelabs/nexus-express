const db = require('../../dbconnection')

const Appfeatures = {
  addMemoryVerse: (payload, callback) => {
    const addMemoryVerseQuery = 'INSERT INTO memoryverse SET ?'
    return db.query(addMemoryVerseQuery, payload, callback)
  },

  addHighlights: (payload, callback) => {
    const addHighlightsQuery = 'INSERT INTO highlights (userId, highlightsId, bookId, chapterId, verseId, content, color, createdAt) VALUES ?'
    return db.query(addHighlightsQuery, [payload], callback)
  },

  addNotes: (payload, callback) => {
    const addNotesQuery = 'INSERT INTO biblenotes SET ?'
    return db.query(addNotesQuery, payload, callback)
  },

  addReadingHistory: (payload, callback) => {
    const addReadingQuery = 'INSERT INTO readinghistory SET ?'
    return db.query(addReadingQuery, payload, callback)
  },

  // getMemoryVerseByUserId: (payload, callback) => {
  //   const fetchMemoryVerseQuery = 'SELECT * FROM memoryverse WHERE userId = ? AND deleted = 0'
  //   db.query(fetchMemoryVerseQuery, payload, callback)
  // },

  getHighlightsByUserId: (limit, offset, payload, callback) => {
    const fetchHighlightsQuery = 'SELECT m.highlightsId, DATE_FORMAT(m.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, m.bookId, m.chapterId, m.verseId, m.color FROM highlights m, user u WHERE m.userId = ? AND m.userId = u.userId ORDER BY createdAt ASC limit ' + limit + ' OFFSET ' + offset
    db.query(fetchHighlightsQuery, payload, callback)
  },

  getNotesByUserId: (limit, offset, payload, callback) => {
    const fetchNotesQuery = 'SELECT m.ID as notesId, DATE_FORMAT(m.createdAt, \'%Y-%m-%d\') as createdAt, m.content, m.description FROM biblenotes m, user u WHERE m.userId = ? AND m.userId = u.userId ORDER BY createdAt ASC limit ' + limit + ' OFFSET ' + offset
    db.query(fetchNotesQuery, payload, callback)
  },

  GetNotesByDate: (payload, callback) => {
    payload.createdAt = JSON.stringify(payload.createdAt).substr(1, 10)
    const fetchNotesDateQuery = `SELECT * FROM biblenotes WHERE createdAt = ? AND userId = ?;`
    db.query(fetchNotesDateQuery, [payload.createdAt, payload.userId], callback)
  },

  getReadingHistoryByUserId: (limit, offset, payload, callback) => {
    const fetchReadingHistoryQuery = 'SELECT  m.ID as readingHistoryId, DATE_FORMAT(m.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, m.bookId, m.chapterId, m.verseId FROM readinghistory m, user u WHERE m.userId = ? AND m.userId = u.userId ORDER BY createdAt ASC limit ' + limit + ' OFFSET ' + offset
    db.query(fetchReadingHistoryQuery, payload, callback)
  },

  getMemoryVerseByUserId: (limit, offset, payload, callback) => {
    const fetchNotesDateQuery = 'SELECT m.memoryVerseId, DATE_FORMAT(m.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, m.bookChap, m.verse FROM memoryverse m, user u WHERE m.userId = ? AND m.userId = u.userId ORDER BY createdAt ASC limit ' + limit + ' OFFSET ' + offset
    db.query(fetchNotesDateQuery, payload, callback)
  },

  deleteMemoryVerse: (payload, callback) => {
    const deleteMemoryVerseQuery = 'DELETE FROM memoryverse WHERE memoryVerseId = ? AND userId = ?'
    db.query(deleteMemoryVerseQuery, [payload.memoryVerseId, payload.userId], callback)
  },

  deleteHighlights: (payload, callback) => {
    const deleteHighlightsQuery = 'DELETE FROM highlights WHERE highlightsId = ? AND userId = ?'
    db.query(deleteHighlightsQuery, [payload.highlightsId, payload.userId], callback)
  },

  deleteNotes: (payload, callback) => {
    const deleteNotesQuery = 'DELETE FROM biblenotes WHERE notesId = ? AND userId = ?'
    db.query(deleteNotesQuery, [payload.notesId, payload.userId], callback)
  },

  updateNotes: (payload, callback) => {
    payload.createdAt = JSON.stringify(payload.createdAt).substr(1, 10)
    const updateNotesQuery = 'UPDATE biblenotes SET content = ?, description = ? WHERE userId = ? AND createdAt = ?'
    db.query(updateNotesQuery, [payload.content, payload.description, payload.userId, payload.createdAt], callback)
  },

  deleteReadingHistory: (payload, callback) => {
    const deleteReadingHistoryQuery = 'DELETE FROM readinghistory WHERE readinghisId = ? AND userId = ?'
    db.query(deleteReadingHistoryQuery, [payload.readinghisId, payload.userId], callback)
  },

  selectMemoryVerseByUserId: function (payload, callback) {
    return db.query('SELECT * FROM user INNER JOIN memoryverse ON memoryverse.userId=user.userId WHERE memoryverse.userId = ?', [payload], callback)
  },

  selectHighlightsByUserId: function (payload, callback) {
    return db.query('SELECT * FROM user INNER JOIN highlights ON highlights.userId=user.userId WHERE highlights.userId = ?', [payload], callback)
  },

  selectNotesByUserId: function (payload, callback) {
    return db.query('SELECT * FROM user INNER JOIN biblenotes ON biblenotes.userId=user.userId WHERE biblenotes.userId = ?', [payload], callback)
  },

  selectReadingHistoryByUserId: function (payload, callback) {
    return db.query('SELECT * FROM user INNER JOIN readinghistory ON readinghistory.userId=user.userId WHERE readinghistory.userId = ?', [payload], callback)
  },

  getMemoryVerseById: (payload, callback) => {
    const fetchMemoryVerseByIdQuery = 'SELECT * FROM memoryverse WHERE ID IN (?)'
    db.query(fetchMemoryVerseByIdQuery, [payload], callback)
  },

  getHighlightsById: (payload, callback) => {
    const fetchHighlightsByIdQuery = 'SELECT * FROM highlights WHERE ID IN (?)'
    db.query(fetchHighlightsByIdQuery, [payload], callback)
  },

  getNotesById: (payload, callback) => {
    const fetchNotesByIdQuery = 'SELECT * FROM biblenotes WHERE ID = ?'
    db.query(fetchNotesByIdQuery, payload, callback)
  },

  getReadingHistoryById: (payload, callback) => {
    const fetchReadingByIdQuery = 'SELECT * FROM readinghistory WHERE ID = ?'
    db.query(fetchReadingByIdQuery, payload, callback)
  }

  // updateDescription: (payload, callback) => {
  //   const updateDescQuery = 'UPDATE PrayerDescription SET name = ? WHERE id =?'
  //   db.query(updateDescQuery, [payload.name, payload.id], callback)
  // }
}

module.exports = Appfeatures
const db = require('../../dbconnection')
const bcrypt = require('bcryptjs')

const AppUser = {
  createUser: function (payload, callback) {
    // const today = new Date()
    // const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    // const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    // const dateTime = date + ' ' + time
    payload.password = bcrypt.hashSync(payload.password)
    const addUserQuery = 'INSERT INTO appusers SET ?'
    return db.query(addUserQuery, payload, callback)
  },
  addGroup: function (payload, callback) {
    return db.query('INSERT INTO userGroup (groupName,area,grpAddress,grpCity,grpState,grpPincode,countryId,latitude,longitude,meetingDay,startTime,endTime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
      [payload.groupName, payload.area, payload.grpAddress, payload.grpCity, payload.grpState, payload.grpPincode, payload.countryId, payload.latitude, payload.longitude, payload.meetingDay,
        JSON.stringify(payload.startTime), JSON.stringify(payload.endTime)
      ], callback)
  },
  addtoGroupWithUserId: function (payload, callback) {
    return db.query('INSERT INTO groupMembers (groupId,userId) VALUES (?,?)',
      [payload.groupId, payload.userId], callback)
  },
  selectByEmail: function (payload, callback) {
    return db.query('SELECT * FROM appusers WHERE email = ? OR phone = ?', [payload.emailOrPhone, payload.emailOrPhone], callback)
  },
  selectByRegister: function (payload, callback) {
    return db.query('SELECT * FROM appusers WHERE email = ?', [payload.email], callback)
  },
  selectById: function (payload, callback) {
    return db.query('SELECT * FROM appusers WHERE userId = ?', [payload], callback)
  },
  getAllUsers: function (callback) {
    return db.query('SELECT a.userId, a.firstname, a.lastname, a.email, a.password, a.registerDate, a.phone, a.gender, a.dob, a.address, a.city, a.state, a.pincode, a.profilePic, a.roleId, a.status, a.published, r.roleType FROM appusers as a INNER JOIN role as r ON r.roleId = a.roleId ORDER BY a.userId ASC', callback)
  },
  updateProfile: function (payload, callback) {
    return db.query('UPDATE appusers SET firstname=?, lastname=?, phone=?, gender=?, dob=?, address=?, city=?, state=?, pincode=?, profilePic=?, published=? WHERE userId=?',
      [payload.firstname, payload.lastname, payload.phone, payload.gender, payload.dob, payload.address, payload.city, payload.state, payload.pincode, payload.profilePic, payload.published, payload.userId], callback)
  },
  tranferByGrpId: function (payload, callback) {
    return db.query('UPDATE groupMembers SET groupId=? WHERE userId=?',
      [payload.groupId, payload.userId], callback)
  },
  getAllGroups: function (callback) {
    return db.query('SELECT g.groupId, g.groupName, g.area, g.grpAddress, g.grpCity, g.grpState, g.grpPincode, g.status, g.countryId, g.meetingDay, g.startTime, g.endTime, g.latitude, g.longitude, c.countryName FROM userGroup as g INNER JOIN country as c ON c.countryId = g.countryId WHERE g.status = 0', callback)
  },
  getAllGrpMembersByUsrId: function (callback) {
    return db.query('SELECT gm.grpmemberId, a.groupId, a.groupName, a.status, u.userId, u.firstname, u.lastname, u.email, u.phone FROM userGroup as a INNER JOIN groupMembers as gm ON gm.groupId = a.groupId INNER JOIN appusers as u ON u.userId = gm.userId INNER JOIN role as r ON r.roleId = u.roleId  WHERE r.roleType = "User" ORDER BY gm.grpmemberId ASC', callback)
  },
  checkUserExistsInAnyGrp: function (id, callback) {
    return db.query('SELECT u.userId, u.status FROM groupMembers as gm INNER JOIN appusers as u ON gm.userId = u.userId WHERE gm.userId=? AND u.status = 0', [id], callback)
  },
  deleteStatusForGroup: function (payload, callback) {
    return db.query('UPDATE userGroup SET status=? WHERE groupId=?',
      [payload.status, payload.groupId], callback)
  },
  deleteStatusForUser: function (payload, callback) {
    return db.query('UPDATE appusers as u INNER JOIN assignedmembers as a ON a.userId = u.userId SET a.status=?, u.status=?, u.roleId=? WHERE u.userId=?',
      [payload.updateStatusOfAssignedMems, payload.status, payload.roleId, payload.userId], callback)
  },
  deletePersonalInfo: function (payload, callback) {
    return db.query('UPDATE appusers SET firstname=?, lastname=?, email=?, phone=?, gender=?, dob=?, address=?, city=?, state=?, pincode=?, profilePic=?, status=? WHERE userId=?',
      [payload.firstname, payload.lastname, payload.email, payload.phone, payload.gender, payload.dob, payload.address, payload.city, payload.state, payload.pincode, payload.profilePic, payload.status, payload.userId], callback)
  },
  editPassword: function (payload, callback) {
    return db.query('UPDATE appusers set password=? WHERE userId=?',
      [bcrypt.hashSync(payload.newpassword), payload.userId], callback)
  }
}

module.exports = AppUser
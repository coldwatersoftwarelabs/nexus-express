const db = require('../../dbconnection') // reference of dbconnection.js

const Updates = {

  editUpdates: function (id, ver, callback) {
    function appendLeadingZeros (n) {
      if (n <= 9) {
        return '0' + n
      }
      return n
    }
    const today = new Date()
    const date = today.getFullYear() + '-' + appendLeadingZeros(today.getMonth() + 1) + '-' + appendLeadingZeros(today.getDate())
    const time = appendLeadingZeros(today.getHours()) + ':' + appendLeadingZeros(today.getMinutes()) + ':' + appendLeadingZeros(today.getSeconds())
    const dateTime = date + ' ' + time
    return db.query('UPDATE updates SET lastUpdateDate=?, contentversion=? where ID=?',
      [dateTime, ver, id], callback)
  },
  getVersionUpdates: function (payload, callback) {
    return db.query('SELECT contentVersion FROM updates WHERE ID = ?', [payload], callback)
  },
  getUpdates: function (callback) {
    return db.query('SELECT ID, name, DATE_FORMAT(lastUpdateDate, \'%Y-%m-%d\') as lastUpdateDate, DATE_FORMAT(lastUpdateDate, \'%H:%i\') as lastUpdateTime FROM updates', callback)
  },
  getAppUpdates: function (callback) {
    return db.query('SELECT ID, name, DATE_FORMAT(lastUpdateDate, \'%Y-%m-%d\') as lastUpdateDate, DATE_FORMAT(lastUpdateDate, \'%H:%i\') as lastUpdateTime FROM applocalsync', callback)
  },
  addAppUpdates: function (payload, callback) {
    return db.query('INSERT INTO applocalsync (userId, name, lastUpdateDate) VALUES (?,?,?)',
      [payload.userId, payload.name, payload.lastUpdateDate], callback)
  }

}
module.exports = Updates
const db = require('../../dbconnection')
const bcrypt = require('bcryptjs')

const User = {
  createUser: function (payload, callback) {
    payload.password = bcrypt.hashSync(payload.password)
    const addUserQuery = 'INSERT INTO user SET ?'
    return db.query(addUserQuery, payload, callback)
  },
  addGroup: function (payload, callback) {
    return db.query('INSERT INTO userGroup (groupName,area,grpAddress,grpCity,grpState,grpPincode,countryId,latitude,longitude,meetingDay,startTime,endTime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
      [payload.data.groupName, payload.data.area, payload.data.grpAddress, payload.data.grpCity, payload.data.grpState, payload.data.grpPincode, payload.data.countryId, payload.data.latitude, payload.data.longitude, payload.data.meetingDay,
        JSON.stringify(payload.data.startTime), JSON.stringify(payload.data.endTime)
      ], callback)
  },
  addtoGroupWithUserId: function (payload, callback) {
    return db.query('INSERT INTO groupMembers (groupId,userId) VALUES (?,?)',
      [payload.data.groupId, payload.data.userId], callback)
  },
  selectByEmail: function (payload, callback) {
    return db.query('SELECT *, DATE_FORMAT(registerDate, \'%Y-%m-%d %H:%i\') as registerDate, DATE_FORMAT(dob, \'%Y-%m-%d\') as dob, categoryId FROM user WHERE email = ? OR phone = ? AND roleId!=5',
      [payload.emailOrPhone, payload.emailOrPhone], callback)
  },
  getDownloadedBibleCount: function (payload, callback) {
    return db.query('SELECT u.bibleId, u.downloadStatus, b.name, b.langId, b.xmlurl, b.tblName FROM userbiblemap u INNER JOIN bible b ON b.id=u.bibleId WHERE u.userId = ? AND u.downloadStatus!=0 AND b.deleted=0', [payload], callback)
  },
  getDownloadedPlanCount: function (payload, callback) {
    return db.query('SELECT u.planId, u.readStatus, u.downloadStatus, p.name, p.tblName, p.planduration, p.makeupday, p.readingsperday, p.byDate, p.xmlurl, p.ismakeupday FROM userplanmap u INNER JOIN plandetails p ON p.id=u.planId WHERE u.userId = ? AND u.downloadStatus!=0 AND p.deleted=0 AND p.status=\'Published\'', [payload], callback)
  },
  selectByRegister: function (payload, callback) {
    return db.query('SELECT * FROM user WHERE email = ? OR phone = ? AND roleId!=5', [payload.email, payload.phone], callback)
  },
  selectByIdEmail: function (payload, callback) {
    return db.query('SELECT * FROM user WHERE userId = ? AND roleId!=5', [payload.userId], callback)
  },
  selectById: function (payload, callback) {
    return db.query('SELECT *, DATE_FORMAT(registerDate, \'%Y-%m-%d %H:%i\') as registerDate, DATE_FORMAT(dob, \'%Y-%m-%d\') as dob, categoryId FROM user WHERE userId = ? AND roleId!=5', [payload], callback)
  },
  checkUser: function (payload, callback) {
    return db.query('SELECT * FROM user WHERE userId = ? AND roleId!=5', payload, callback)
  },
  getAllUsers: function (callback) {
    return db.query('SELECT a.userId, a.firstname, a.lastname, a.email, a.password, a.registerDate, a.phone, a.gender, a.dob, a.address1, a.address2, a.city, a.state, a.country, a.pincode, a.profilePic, a.roleId, a.status, a.published, a.categoryId, a.approveStatus, a.disable, r.roleType FROM user as a INNER JOIN role as r ON r.roleId = a.roleId ORDER BY a.userId ASC', callback)
  },
  updateProfile: function (payload, callback) {
    return db.query('UPDATE user SET ? WHERE userId=?',
      [payload, payload.userId], callback)
  },
  updateApproveStatus: function (payload, callback) {
    return db.query('UPDATE user SET approveStatus=? WHERE userId=?',
      [payload.approveStatus, payload.userId], callback)
  },
  updateProfilePic: function (payload, callback) {
    return db.query('UPDATE user SET profilePic=? WHERE userId=?',
      [payload.profilePic, payload.userId], callback)
  },
  tranferByGrpId: function (payload, callback) {
    return db.query('UPDATE groupMembers SET groupId=? WHERE userId=?',
      [payload.data.groupId, payload.data.userId], callback)
  },
  getAllGroups: function (callback) {
    return db.query('SELECT g.groupId, g.groupName, g.area, g.grpAddress, g.grpCity, g.grpState, g.grpPincode, g.status, g.countryId, g.meetingDay, g.startTime, g.endTime, g.latitude, g.longitude, c.countryName FROM userGroup as g INNER JOIN country as c ON c.countryId = g.countryId WHERE g.status = 0', callback)
  },
  getAllGrpMembersByUsrId: function (callback) {
    return db.query('SELECT gm.grpmemberId, a.groupId, a.groupName, a.status, u.userId, u.firstname, u.lastname, u.email, u.phone FROM userGroup as a INNER JOIN groupMembers as gm ON gm.groupId = a.groupId INNER JOIN user as u ON u.userId = gm.userId INNER JOIN role as r ON r.roleId = u.roleId  WHERE r.roleType = "User" ORDER BY gm.grpmemberId ASC', callback)
  },
  checkUserExistsInAnyGrp: function (id, callback) {
    return db.query('SELECT u.userId, u.status FROM groupMembers as gm INNER JOIN user as u ON gm.userId = u.userId WHERE gm.userId=? AND u.status = 0', [id], callback)
  },
  deleteStatusForGroup: function (payload, callback) {
    return db.query('UPDATE userGroup SET status=? WHERE groupId=?',
      [payload.data.status, payload.data.groupId], callback)
  },
  deleteStatusForUser: function (payload, callback) {
    return db.query('UPDATE user as u INNER JOIN assignedmembers as a ON a.userId = u.userId SET a.status=?, u.roleId=? WHERE u.userId=?',
      [payload.data.status, payload.data.roleId, payload.data.userId], callback)
  },
  deletePersonalInfo: function (payload, callback) {
    return db.query(`UPDATE user SET firstname=?, lastname=?, email=?, phone=?, roleId=?, 
      gender=?, dob=?, address1=?, address2=?, city=?, state=?, pincode=?, profilePic=?, status=? WHERE userId=?`,
      [payload.firstname, payload.lastname, payload.email, payload.phone, payload.roleId, payload.gender, payload.dob, payload.address1, payload.address2, payload.city, payload.state, payload.pincode, payload.profilePic, payload.status, payload.userId], callback)
  },
  editPassword: function (payload, callback) {
    return db.query('UPDATE user SET password=? WHERE userId=?',
      [bcrypt.hashSync(payload.newpassword), payload.userId], callback)
  },
  updateDeviceToken: function (payload, callback) {
    return db.query('UPDATE user SET deviceToken=? WHERE email=?',
      [payload.deviceToken, payload.emailOrPhone], callback)
  },
  updateAPIToken: function (payload, uId, callback) {
    return db.query('UPDATE user SET apiSecTok=? WHERE userId = ?',
      [payload, uId], callback)
  },
  resetAPIToken: function (payload, payload1, callback) {
    return db.query('UPDATE user SET apiSecTok=? WHERE email = ? OR phone = ?',
      [payload, payload1, payload1], callback)
  },
  insertReason: function (payload, callback) {
    const addReasonQuery = 'INSERT INTO rejectedreason (userId, reason) VALUES (?, ?)'
    return db.query(addReasonQuery, [payload.userId, payload.rejectReason], callback)
  },
}

module.exports = User
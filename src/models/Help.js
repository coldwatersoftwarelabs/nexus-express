const db = require('../../dbconnection') // reference of dbconnection.js

const Help = {
  addHelpQuery: (payload, callback) => {
    const addhelpQuery = 'INSERT INTO helpdeskquery SET ?'
    return db.query(addhelpQuery, payload, callback)
  },
  addHelpAnswer: (payload, callback) => {
    const addhelpanswerQuery = 'INSERT INTO helpdeskanswer SET ?'
    return db.query(addhelpanswerQuery, payload, callback)
  },
  updateHelpStatus: (payload, callback) => {
    const updateQueryStatusQuery = 'UPDATE helpdeskquery set answered=?, status=? WHERE id = ?'
    return db.query(updateQueryStatusQuery, [payload.answered, payload.status, payload.queryId], callback)
  },
  allQuery: (callback) => {
    const getQuestionsQuery = `SELECT helpdeskquery.id, helpdeskquery.userId, querypriority.id as priorityId, querypriority.priority, helpdeskquery.query, DATE_FORMAT(helpdeskquery.createdAt, \'%d-%m-%Y %H:%i\') as webCreatedAt, DATE_FORMAT(helpdeskquery.createdAt, \'%Y-%m-%d %H:%i\') as appCreatedAt, helpdeskquery.answered, helpdeskquery.status, user.firstname, user.lastname, user.phone, user.profilePic from helpdeskquery INNER JOIN user ON user.userId=helpdeskquery.userId INNER JOIN querypriority ON querypriority.id=helpdeskquery.priority order by querypriority.id ASC, createdAt;`
    return db.query(getQuestionsQuery, callback)
  },
  getQueries: (payload, callback) => {
    const getQuestionsQuery = `SELECT helpdeskquery.id, helpdeskquery.query, helpdeskquery.createdAt, querypriority.priority, helpdeskquery.userId, helpdeskquery.answered, user.firstname, user.lastname, user.phone
    from helpdeskquery, querypriority, user where querypriority.priority LIKE '%${payload}%' 
    AND querypriority.id = helpdeskquery.priority AND user.userId = helpdeskquery.userId;`
    return db.query(getQuestionsQuery, payload, callback)
  },
  getAQuery: (payload, callback) => {
    const getQuery = `SELECT helpdeskquery.*, helpdeskattachment.imageUrl, querypriority.priority as qPriority FROM helpdeskquery LEFT JOIN 
    querypriority on querypriority.id = helpdeskquery.priority LEFT JOIN
    helpdeskattachment ON helpdeskattachment.queryId = 
    helpdeskquery.id  WHERE helpdeskquery.id = ?;`
    return db.query(getQuery, payload, callback)
  },
  getChat: (payload, callback) => {
    const getChatQuery = `SELECT helpdeskquery.id as qid,helpdeskquery.query, DATE_FORMAT(helpdeskquery.createdAt, \'%Y-%m-%d %H:%i\') as appqtime, DATE_FORMAT(helpdeskquery.createdAt, \'%d-%m-%Y %H:%i\') as webqtime, helpdeskquery.status, helpdeskquery.answered,
    helpdeskattachment.id as attachmentId, helpdeskattachment.imageUrl, 
    helpdeskanswer.id as aid,helpdeskanswer.answer, DATE_FORMAT(helpdeskanswer.createdAt, \'%Y-%m-%d %H:%i\') as appatime, DATE_FORMAT(helpdeskanswer.createdAt, \'%d-%m-%Y %H:%i\') as webatime, helpanswerattachment.id as ansattachmentId, helpanswerattachment.ansimageUrl FROM helpdeskquery
    LEFT JOIN helpdeskattachment ON helpdeskattachment.queryId = helpdeskquery.id LEFT JOIN helpdeskanswer 
    ON helpdeskanswer.queryId = helpdeskquery.id LEFT JOIN helpanswerattachment ON helpanswerattachment.answerId = helpdeskanswer.id WHERE helpdeskquery.userId = ?;`
    return db.query(getChatQuery, payload, callback)
  },
  getAllAnswered: (payload, callback) => {
    const getChatQuery = 'SELECT helpdeskquery.id as qid,helpdeskquery.query, helpdeskquery.createdAt as qtime, helpdeskattachment.id as attachmentId, helpdeskattachment.imageUrl, helpdeskanswer.id as aid,helpdeskanswer.answer, helpdeskanswer.createdAt as atime, helpanswerattachment.id as ansattachmentId, helpanswerattachment.ansimageUrl, (SELECT firstname from user left join helpdeskquery on helpdeskquery.userId=user.userId where user.userId=helpdeskquery.userId limit 1) as appuser_fname, (SELECT lastname from user left join helpdeskquery on helpdeskquery.userId=user.userId where user.userId=helpdeskquery.userId limit 1) as appuser_lname, (SELECT firstname from user left join helpdeskanswer on helpdeskanswer.userId=user.userId where user.userId=helpdeskanswer.userId limit 1) as adminuser_fname, (SELECT lastname from user left join helpdeskanswer on helpdeskanswer.userId=user.userId where user.userId=helpdeskanswer.userId limit 1) as adminuser_lname, (SELECT phone from user left join helpdeskquery on helpdeskquery.userId=user.userId where user.userId=helpdeskquery.userId limit 1) as appuser_phone FROM helpdeskquery LEFT JOIN helpdeskattachment ON helpdeskattachment.queryId = helpdeskquery.id LEFT JOIN helpdeskanswer ON helpdeskanswer.queryId = helpdeskquery.id LEFT JOIN helpanswerattachment ON helpanswerattachment.answerId = helpdeskanswer.id LEFT JOIN user ON helpdeskquery.userId=user.userId WHERE helpdeskquery.answered=1;'
    return db.query(getChatQuery, payload, callback)
  },
  addAttachment: (payload, callback) => {
    const addAttachmentQuery = 'INSERT INTO helpdeskattachment(queryId, imageUrl) VALUES ?'
    return db.query(addAttachmentQuery, [payload], callback)
  },
  addansAttachment: (payload, callback) => {
    const addansAttachmentQuery = 'INSERT INTO helpanswerattachment(answerId, ansimageUrl) VALUES ?'
    return db.query(addansAttachmentQuery, [payload], callback)
  }

}
module.exports = Help
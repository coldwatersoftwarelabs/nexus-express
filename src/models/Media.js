const db = require('../../dbconnection') // reference of dbconnection.js

const Media = {

  addMedia: function (payload, callback) {
    payload.content = JSON.stringify(payload.content)
    const addMediaQuery = 'INSERT INTO media SET ?'
    return db.query(addMediaQuery, payload, callback)
  },
  viewAllGeneralPublishedMedia: function (callback) {
    return db.query('SELECT media.ID, media.title, media.excerpt, media.content, media.parentId, media.showChild, media.bannerImageUrl, media.videoUrl, media.audioUrl, media.pdfUrl, media.status, media.language, media.mediaClassification, media.quiz, media.order, media.editedBy, media.addedBy, DATE_FORMAT(media.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(media.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, media.expire, media.featured, language.langType FROM media INNER JOIN language ON language.langId=media.language WHERE media.status=\'Published\' AND media.mediaClassification=1 AND ((media.startDate is null AND media.endDate is null) OR (CURRENT_DATE BETWEEN media.startDate AND media.endDate))', callback)
  },
  viewFeaturedMedia: function (callback) {
    return db.query('SELECT media.ID, media.title, media.excerpt, media.content, media.parentId, media.showChild, media.bannerImageUrl, media.videoUrl, media.audioUrl, media.pdfUrl, media.status, media.language, media.mediaClassification, media.quiz, media.order, media.editedBy, media.addedBy, DATE_FORMAT(media.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(media.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, media.expire, media.featured, language.langType FROM media INNER JOIN language ON language.langId=media.language WHERE media.status=\'Published\' AND media.mediaClassification=1 AND ((media.startDate is null AND media.endDate is null) OR (CURRENT_DATE BETWEEN media.startDate AND media.endDate)) AND media.featured=1;', callback)
  },
  viewAllMedia: function (callback) {
    return db.query('SELECT media.ID, media.title, media.excerpt, media.content, media.duration, media.level, media.parentId, media.showChild, media.bannerImageUrl, media.contentImageUrl, media.contentImagePostition, media.videoUrl, media.audioUrl, media.pdfUrl, media.category, media.tags, media.url, media.status, media.language, media.mediaClassification, media.quiz, media.order, media.editedBy, media.addedBy, DATE_FORMAT(media.createdAt, \'%Y-%m-%d\') as createdAt, DATE_FORMAT(media.editedAt, \'%Y-%m-%d\') as editedAt, media.expire, DATE_FORMAT(media.editedAt, \'%d-%m-%Y\') as updatedOn, media.startDate, media.endDate, media.featured, media.tags, language.langType, media.addedBy, user.firstname, user.lastname, media.editedBy FROM media INNER JOIN language ON language.langId=media.language INNER JOIN user ON user.userId=media.addedBy;', callback)
  },
  viewAllMediaByID: function (payload, callback) {
    return db.query('SELECT * FROM media WHERE ID = ?', [payload], callback)
  },
  updateMedia: function (ID, payload, callback) {
    const today = new Date()
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    const dateTime = date + ' ' + time
    return db.query('UPDATE media SET title=?, excerpt=?, content=?, parentId=?, showChild=?, bannerImageUrl=?, status=?, language=?, mediaClassification=?, editedBy=?, editedAt=?, expire=?, videoUrl=?, audioUrl=?, pdfUrl=?, quiz=?, startDate=?, endDate=?, featured=?, category=?, tags=?, duration=?, level=? WHERE ID=?',
      [payload.title, payload.excerpt, payload.content, payload.parentId, payload.showChild, payload.bannerImageUrl, payload.status, payload.language, payload.mediaClassification, payload.editedBy, dateTime, payload.expire, payload.videoUrl, payload.audioUrl, payload.pdfUrl, payload.quiz, payload.startDate, payload.endDate, payload.featured, payload.category, payload.tags, payload.duration, payload.level, ID], callback)
  },
  deleteMedia: function (id, callback) {
    return db.query('DELETE FROM media WHERE ID=?',
      [id], callback)
  }

}
module.exports = Media
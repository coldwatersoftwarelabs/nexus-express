const db = require('../../dbconnection') // reference of dbconnection.js

const Event = {

  addGeneralSettings: (payload, callback) => {
    const addGeneralSetQuery = 'INSERT INTO generalization SET ?'
    return db.query(addGeneralSetQuery, payload, callback)
  },
  viewAllGeneral: function (callback) {
    return db.query('SELECT generalization.*, DATE_FORMAT(editedat, \'%d-%m-%Y\') as updatedOn, user.firstname, user.lastname FROM generalization INNER JOIN user ON user.userId=generalization.addedby', callback)
  },
  updateGeneralization: function (ID, payload, callback) {
    return db.query('UPDATE generalization SET appname = ?, logourl = ? , logosize = ? , iconurl = ? , iconsize = ?, fontcolor = ?, fontsize = ?, fontfamily = ?, fontupload = ?, bgcolor = ?, editedby = ?, editedat = ?, sidebarmainmenufontcolor = ?, sidebarsubmenufontcolor = ?, sidebaractivemenucolor = ?, sidebaractiveiconcolor = ?, sidebarbgcolor = ?, sidebariconcolor = ?, headerfontcolor = ?, headerbgcolor = ?, headerlogosize = ?, footerfontcolor = ?, footerbgcolor = ?, footerlogosize = ?, bgImage = ? WHERE ID=?',
      [payload.appname, payload.logourl, payload.logosize, payload.iconurl, payload.iconsize, payload.fontcolor, payload.fontsize, payload.fontfamily, payload.fontupload, payload.bgcolor, payload.editedby, payload.editedat, payload.sidebarmainmenufontcolor, payload.sidebarsubmenufontcolor, payload.sidebaractivemenucolor, payload.sidebaractiveiconcolor, payload.sidebarbgcolor, payload.sidebariconcolor, payload.headerfontcolor, payload.headerbgcolor, payload.headerlogosize, payload.footerfontcolor, payload.footerbgcolor, payload.footerlogosize, payload.bgImage, ID], callback)
  },
  deleteEvent: function (id, callback) {
    return db.query('DELETE FROM event WHERE ID=?',
      [id], callback)
  }

}
module.exports = Event
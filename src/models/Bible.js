const db = require('../../dbconnection') // reference of dbconnection.js

const Bible = {

  addBible: function (payload, callback) {
    const addXmlbibleQuery = 'INSERT INTO bible SET ?;'
    return db.query(addXmlbibleQuery, payload, callback)
  },
  getAllBible: function (callback) {
    return db.query('SELECT b.bibleId, b.versionId, v.versionName, l.langId, l.langType FROM bibleType as b INNER JOIN version as v ON b.versionId=v.versionId INNER JOIN language as l ON l.langId=v.langId ORDER BY b.bibleId ASC', callback)
  },

  GetXmlBible: (payload, callback) => {
    const getxmlBible = 'SELECT bible.id, bible.name, bible.xmlurl, language.langType, language.langId, bible.tblName, (CASE WHEN (select count(downloadStatus) from userbiblemap WHERE userId=? AND bibleId=bible.id)=0 THEN 0 ELSE (select downloadStatus from userbiblemap WHERE userId=? AND bibleId=bible.id) END) as downloadStatus, (select count(downloadStatus) from userbiblemap WHERE bibleId=bible.id AND (downloadStatus=2 OR downloadStatus=3)) as userCount FROM bible INNER JOIN language ON bible.langId=language.langId WHERE bible.deleted = 0 AND (bible.xmlurl IS NOT NULL);'
    return db.query(getxmlBible, [payload, payload], callback)
  },

  fetchBible: (payload, callback) => {
    const addPlanQuery = 'SELECT * FROM bible WHERE id = ? AND (xmlurl IS NOT NULL) AND deleted=0;'
    return db.query(addPlanQuery, payload, callback)
  },

  getBibleById: (payload, callback) => {
    const addPlanQuery = 'SELECT * FROM bible WHERE id = ? AND deleted=0;'
    return db.query(addPlanQuery, payload, callback)
  },

  addBibleToUser: (payload, callback) => {
    const addBibleToUserQuery = 'INSERT INTO userbiblemap SET ?;'
    return db.query(addBibleToUserQuery, payload, callback)
  },

  GetDistinctBible: (payload, callback) => {
    const getDistinctChapter = 'SELECT DISTINCT chapterId, bookId FROM bibleverse;'
    return db.query(getDistinctChapter, callback)
  },

  updateBible: function (payload, callback) {
    const editBibleQuery = 'UPDATE bible SET ? WHERE id = ?;'
    return db.query(editBibleQuery, [payload, payload.id], callback)
  },

  deleteBible: function (payload, callback) {
    const delBibleQuery = 'UPDATE bible SET deleted=1 WHERE id = ?;'
    return db.query(delBibleQuery, payload, callback)
  },

  checkBibleMappedorNot: (payload, callback) => {
    const addPlanQuery = 'SELECT * FROM userbiblemap WHERE bibleId = ? AND userId=?;'
    return db.query(addPlanQuery, [payload.bibleId, payload.userId], callback)
  },

  getBibleFromServer: function (callback) {
    return db.query('SELECT * FROM kjv', callback)
  },
  getEnglishBooks: function (callback) {
    return db.query('SELECT * FROM biblebooks', callback)
  },
  getChapterByBook: function (payload, callback) {
    return db.query('SELECT DISTINCT c FROM kjv WHERE b=?', [payload], callback)
  },
  updateDownloadStatus: (payload, callback) => {
    const updateStatus = 'UPDATE userbiblemap SET downloadStatus=? WHERE bibleId = ? AND userId = ?;'
    return db.query(updateStatus, [payload.downloadStatus, payload.bibleId, payload.userId], callback)
  },
  updateAlreadySelectedBible: (payload, callback) => {
    const updateStatus = 'UPDATE userbiblemap SET downloadStatus=2 WHERE bibleId = ? AND userId = ? AND downloadStatus = 3;'
    return db.query(updateStatus, [payload.bibleId, payload.userId], callback)
  }

}
module.exports = Bible
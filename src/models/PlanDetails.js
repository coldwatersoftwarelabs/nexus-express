const db = require('../../dbconnection')

const PrayerDetail = {
  addPlan: (payload, callback) => {
    const addPlanQuery = 'INSERT INTO plandetails SET ?;'
    return db.query(addPlanQuery, payload, callback)
  },

  editPlan: (payload, callback) => {
    const addPlanQuery = 'UPDATE plandetails SET ? WHERE id = ?;'
    return db.query(addPlanQuery, [payload, payload.id], callback)
  },

  deletePlan: (payload, callback) => {
    const addPlanQuery = 'UPDATE plandetails SET deleted = 1 WHERE id = ?;'
    return db.query(addPlanQuery, payload, callback)
  },

  listPlan: (payload, callback) => {
    const addPlanQuery = 'SELECT *, DATE_FORMAT(editedAt, \'%d-%m-%Y\') as updatedOn, (select count(downloadStatus) from userplanmap WHERE planId=plandetails.id AND (downloadStatus=2 OR downloadStatus=3)) as userCount FROM plandetails WHERE deleted = 0;'
    return db.query(addPlanQuery, callback)
  },

  listXmlPlans: (payload, callback) => {
    const addPlanQuery = 'SELECT *, (CASE WHEN (select count(downloadStatus) from userplanmap WHERE userId=? AND planId=plandetails.id)=0 THEN 0 ELSE (select downloadStatus from userplanmap WHERE userId=? AND planId=plandetails.id) END) as downloadStatus FROM plandetails WHERE deleted = 0 AND status=\'Published\' AND (xmlurl IS NOT NULL);'
    return db.query(addPlanQuery, [payload, payload], callback)
  },

  fetchPlan: (payload, callback) => {
    const addPlanQuery = 'SELECT id, name FROM plandetails WHERE id = ? AND (xmlurl IS NOT NULL) AND deleted=0;'
    return db.query(addPlanQuery, payload, callback)
  },

  fetchPlanById: (payload, callback) => {
    const addPlanQuery = 'SELECT id, name FROM plandetails WHERE id = ? AND deleted=0;'
    return db.query(addPlanQuery, payload, callback)
  },

  checkPlanMappedorNot: (payload, callback) => {
    const addPlanQuery = 'SELECT * FROM userplanmap WHERE planId = ? AND userId=?;'
    return db.query(addPlanQuery, [payload.planId, payload.userId], callback)
  },

  getBooksList: (payload, callback) => {
    const getBooksQuery = 'SELECT * FROM book WHERE deleted = 0;'
    return db.query(getBooksQuery, '', callback)
  },

  getChaptersList: (payload, callback) => {
    const getChaptersQuery = 'SELECT DISTINCT(chapterId) FROM bibleverse WHERE bookId = ?;'
    return db.query(getChaptersQuery, payload, callback)
  },

  getVerseList: (payload, callback) => {
    const getVerseQuery = 'SELECT verseId FROM bibleverse WHERE bookId = ? AND chapterId = ?;'
    return db.query(getVerseQuery, [payload.bookId, payload.chapterId], callback)
  },

  GetWholeBible: (payload, callback) => {
    const getWholeBible = 'SELECT bookId, chapterId, verseId FROM `bibleverse`;'
    return db.query(getWholeBible, callback)
  },

  GetBibleVerseById: (payload, callback) => {
    const getBibleVerseByIdQuery = 'SELECT bookId, chapterId, verseId FROM bibleverse WHERE bookId = ? AND chapterId = ? AND verseId IN (?);'
    return db.query(getBibleVerseByIdQuery, [payload.bookId, payload.chapterId, payload.verseId], callback)
  },

  GetDistinctBible: (payload, callback) => {
    const getDistinctChapter = 'SELECT DISTINCT chapterId, bookId FROM bibleverse;'
    return db.query(getDistinctChapter, callback)
  },

  addPlanModule: (payload, callback) => {
    const addPlanModQuery = 'INSERT INTO planmodules SET ?;'
    return db.query(addPlanModQuery, payload, callback)
  },

  addReadingPlan: (payload, callback) => {
    const addPlanModQuery = `INSERT INTO readingplan(planModuleId, fromBook, toBook, fromChapter, toChapter,
            fromVerse, toVerse)  VALUES ?;`
    return db.query(addPlanModQuery, [payload], callback)
  },

  ReadingPlanList: (payload, callback) => {
    const getPlanModList = 'SELECT * FROM planmodules WHERE deleted = 0 AND planId = ?;'
    return db.query(getPlanModList, payload, callback)
  },

  // getReadingByPlanId: (payload, callback) => {
  //   const readings = 'SELECT p.*, rp.id as readingId, rp.planModuleId, rp.fromChapter, rp.toChapter, rp.fromVerse, rp.toVerse,rp.fromBook, rp.toBook, CASE WHEN p.byDate = 1 THEN pm.readingDate WHEN p.byDate = 0 THEN pm.readingDay END AS dayOrDate FROM planmodules pm, plandetails p, readingplan rp JOIN book b1 ON b1.id = rp.fromBook JOIN book b2 ON b2.id = rp.toBook WHERE p.id = ? AND p.id = pm.planId AND rp.planModuleId = pm.id;'
  //   return db.query(readings, payload, callback)
  // },

  getReadingByPlanId: (payload, callback) => {
    const readings = 'SELECT p.*, rp.id as readingId, rp.planModuleId, rp.fromChapter, rp.toChapter, rp.fromVerse, rp.toVerse,rp.fromBook, rp.toBook, CASE WHEN p.byDate = 1 THEN pm.readingDate WHEN p.byDate = 0 THEN pm.readingDay END AS dayOrDate FROM planmodules pm, plandetails p, readingplan rp WHERE p.id = ? AND p.id = pm.planId AND rp.planModuleId = pm.id;'
    return db.query(readings, payload, callback)
  },

  getReadingByUserId: (payload, callback) => {
    const userReading = 'SELECT up.userId, CASE WHEN up.readStatus = 1 THEN 1 ELSE 0 END AS completed, CASE WHEN urp.readStatus = 1 THEN 1 ELSE 0 END AS readStatus, p.id as planId, p.name, p.tblName, rp.id as readingId, rp.planModuleId, rp.fromChapter, rp.toChapter, rp.fromVerse, rp.toVerse,rp.fromBook, rp.toBook, CASE WHEN p.byDate = 1 THEN pm.readingDate WHEN p.byDate = 0 THEN pm.readingDay END AS dayOrDate FROM planmodules pm, plandetails p, userplanmap up, userreadingplanmap urp, readingplan rp JOIN book b1 ON b1.id = rp.fromBook JOIN book b2 ON b2.id = rp.toBook WHERE p.deleted = 0 AND (p.xmlurl IS NOT NULL) AND p.id = pm.planId AND rp.planModuleId = pm.id AND up.planId=p.id AND up.planId=p.id AND urp.readingPlanId=rp.id AND up.downloadStatus=2 AND up.userId=?;'
    return db.query(userReading, payload, callback)
  },

  getPlanModDetails: (payload, callback) => {
    const getPlanModDetailQuery = `SELECT planmodules.*, readingplan.* from planmodules, readingplan WHERE 
        planmodules.id = readingplan.planModuleId and planmodules.deleted=0 and planmodules.id = ?;`
    return db.query(getPlanModDetailQuery, payload, callback)
  },

  deletePlanMod: (payload, callback) => {
    const deletePlanQuery = 'UPDATE planmodules SET deleted = 1 WHERE id = ?;'
    return db.query(deletePlanQuery, payload, callback)
  },

  updatePlanMod: (payload, callback) => {
    const updatePlanModQuery = 'UPDATE planmodules SET ? WHERE id = ?'
    return db.query(updatePlanModQuery, [payload, payload.id], callback)
  },

  updateReadinPlan: (payload, callback) => {
    const updateReadingPlanQuery = 'UPDATE readingplan SET ? WHERE id =?'
    return db.query(updateReadingPlanQuery, [payload, payload.id], callback)
  },
  getReadingPlanDetail: (payload, callback) => {
    const getPlanDetailQuery = `SELECT fromBook, toBook, fromChapter, toChapter, fromVerse, toVerse from
            readingplan WHERE id = ?`
    return db.query(getPlanDetailQuery, payload, callback)
  },
  getVerseFromSameChapter: (payload, callback) => {
    const getVerseQuery = `SELECT bv.verseId, bv.verse, bv.chapterId, b.name FROM bibleverse bv, book b WHERE bv.bookId = ? 
            AND bv.chapterId = ? AND bv.verseId BETWEEN ? AND ? AND bv.bookId = b.id`
    return db.query(getVerseQuery, [payload.bookId, payload.chapterId, payload.fromVerse, payload.toVerse], callback)
  },
  getVerseFromDiffChapter: (payload, callback) => {
    const getVerseQuery = `SELECT bv.verseId, bv.verse, bv.chapterId, b.name FROM bibleverse bv, book b WHERE bv.bookId = ?
            AND bv.chapterId = ? AND bv.verseId >= ?
            AND bv.bookId = b.id
            UNION ALL
            SELECT bv.verseId, bv.verse, bv.chapterId, b.name FROM bibleverse bv, book b WHERE bv.bookId = ? 
            AND bv.chapterId = ? AND bv.verseId <= ? AND bv.bookId = b.id`
    return db.query(getVerseQuery, [payload.fromBook, payload.fromChapter, payload.fromVerse, payload.toBook,
      payload.toChapter, payload.toVerse
    ], callback)
  },
  getReadingPlanBooks: (payload, callback) => {
    const getBookQuery = `SELECT rp.id as readingPlanId, b.name FROM planmodules pm, readingplan rp, book b WHERE pm.id = ? AND pm.id = rp.planModuleId
        AND rp.fromBook = b.id;`
    return db.query(getBookQuery, payload, callback)
  },
  getReadingPlanPerPlan: (payload, callback) => {
    const getplanModPerPlanQuery = 'SELECT p.name,rp.id,rp.planModuleId FROM planmodules pm, readingplan rp, plandetails p WHERE pm.planId = ? AND rp.planModuleId = pm.Id AND p.id = pm.planId;'
    return db.query(getplanModPerPlanQuery, payload, callback)
  },

  addPlanToUser: (payload, callback) => {
    const addPlanToUserQuery = 'INSERT INTO userplanmap SET ?;'
    return db.query(addPlanToUserQuery, payload, callback)
  },

  addReadingPlanToUser: (payload, callback) => {
    const addReadingPlanToUserQuery = 'INSERT INTO userreadingplanmap(planId, readingPlanId, userId) VALUES ?;'
    return db.query(addReadingPlanToUserQuery, [payload], callback)
  },

  getReadingPlans: (payload, callback) => {
    const getReadingPlansQuery = `SELECT p.id, rp.id AS readingPlanId FROM plandetails p JOIN planmodules pm ON pm.planId = p.id
      JOIN readingplan rp ON rp.planModuleId = pm.id WHERE p.id = ?;`
    return db.query(getReadingPlansQuery, payload, callback)
  },

  markPlanasRead: (payload, callback) => {
    const markPlanasReadQuery = 'UPDATE userplanmap SET readStatus = 1 WHERE userId =? AND planId = ?;'
    return db.query(markPlanasReadQuery, [payload.userId, payload.planId], callback)
  },

  markReadingPlanAsRead: (payload, callback) => {
    const markRadingPlanAsReadQuery = 'UPDATE userreadingplanmap SET readStatus = 1 WHERE userId=? AND planId=? AND readingPlanId=?;'
    return db.query(markRadingPlanAsReadQuery, [payload.userId, payload.planId, payload.readingPlanId], callback)
  },

  getReadingPlanStatus: (payload, callback) => {
    const getReadingPlanStatusQuery = `SELECT pm1.id, rp1.id, upm.readStatus FROM planmodules pm1 JOIN readingplan rp1 ON rp1.planModuleId = pm1.id
    JOIN userreadingplanmap upm ON upm.readingPlanId = rp1.id WHERE pm1.planId = ?
    AND upm.readStatus = 0 AND upm.userId = ?; `

    return db.query(getReadingPlanStatusQuery, [payload.planId, payload.userId], callback)
  },

  getPlanFromReadingPlan: (payload, callback) => {
    const getPlanQuery = `SELECT p.id FROM userreadingplanmap rpm, readingplan rp, planmodules pm, plandetails p WHERE
    rpm.readingPlanId = ? AND rpm.readingPlanId = rp.id AND rp.planModuleId = pm.id AND pm.PlanId = p.id`
    return db.query(getPlanQuery, payload, callback)
  },
  updateAlreadySelectedPlan: (payload, callback) => {
    const updateStatus = 'UPDATE userplanmap SET downloadStatus=2 WHERE planId = ? AND userId = ? AND downloadStatus = 3;'
    return db.query(updateStatus, [payload.planId, payload.userId], callback)
  },
  updateDownloadStatus: (payload, callback) => {
    const updateStatus = 'UPDATE userplanmap SET downloadStatus=? WHERE planId = ? AND userId = ?;'
    return db.query(updateStatus, [payload.downloadStatus, payload.planId, payload.userId], callback)
  }
}

module.exports = PrayerDetail
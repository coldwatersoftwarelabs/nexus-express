const db = require('../../dbconnection') // reference of dbconnection.js

const Menu = {

  addSideMenu: function (payload, callback) {
    return db.query('INSERT into sidemenu (engId,pageId,menuType,subMenu,parentMenuId,menuOrder,iconLink) VALUES (?,?,?,?,?,?,?)',
      [payload.engId, payload.data.pageId, payload.data.menuType, payload.data.subMenu, payload.data.parentMenuId, payload.data.menuOrder, payload.data.iconLink], callback)
  },
  addFooterMenu: function (payload, callback) {
    return db.query('INSERT into footermenu (engId,pageId,parentMenu,subMenu,parentMenuId,menuOrder) VALUES (?,?,?,?,?,?)',
      [payload.engId, payload.data.pageId, payload.data.parentMenu, payload.data.subMenu, payload.data.parentMenuId, payload.data.menuOrder], callback)
  },
  getSideMenu: function (callback) {
    return db.query('SELECT sidemenu.*, englishlabel.*, language.langType FROM sidemenu INNER JOIN englishlabel ON englishlabel.id=sidemenu.engId INNER JOIN language ON language.langId=englishlabel.langId WHERE englishlabel.status=0', callback)
  },
  viewMainSideMenuByEngLangId: function (id, callback) {
    return db.query('SELECT s.ID, s.menuType, s.subMenu, s.parentMenuId, s.menuOrder, e.name, e.id as engId, e.labelDesc, e.labelImage as iconUrl, (select count(subMenu) from sidemenu WHERE parentMenuId=s.ID) AS submenuCount FROM sidemenu s INNER JOIN englishlabel e ON s.engId=e.id WHERE e.status=0 AND (e.langId=?) ORDER BY s.menuOrder', [id], callback)
  },
  viewMainSideMenuByOtherLangId: function (id, callback) {
    return db.query('SELECT s.ID, s.menuType, s.subMenu, s.parentMenuId, s.menuOrder, l.labelName as name, e.id as engId, e.labelDesc, e.labelImage as iconUrl, (select count(subMenu) from sidemenu WHERE parentMenuId=s.ID) AS submenuCount FROM englishlabel e LEFT JOIN sidemenu s ON s.engId=e.id LEFT JOIN labellist l ON l.engId=e.id WHERE l.status=0 AND e.status=0 AND l.langId=? ORDER BY s.menuOrder', [id], callback)
  },
  viewMainFooterMenuByEngLangId: function (id, callback) {
    return db.query('SELECT s.ID, s.parentMenu, s.subMenu, s.parentMenuId, s.menuOrder, e.name, e.id as engId, e.labelDesc, e.labelImage as iconUrl, (select count(subMenu) from footermenu WHERE parentMenuId=s.ID) AS submenuCount FROM footermenu s INNER JOIN englishlabel e ON s.engId=e.id WHERE e.status=0 AND (e.langId=?) ORDER BY s.menuOrder', [id], callback)
  },
  viewMainFooterMenuByOtherLangId: function (id, callback) {
    return db.query('SELECT s.ID, s.parentMenu, s.subMenu, s.parentMenuId, s.menuOrder, l.labelName as name, e.id as engId, e.labelDesc, e.labelImage as iconUrl, (select count(subMenu) from footermenu WHERE parentMenuId=s.ID) AS submenuCount FROM englishlabel e LEFT JOIN footermenu s ON s.engId=e.id LEFT JOIN labellist l ON l.engId=e.id WHERE l.status=0 AND e.status=0 AND l.langId=? ORDER BY s.menuOrder', [id], callback)
  },
  getFooterMenu: function (callback) {
    return db.query('SELECT footermenu.*, englishlabel.*, language.langType FROM footermenu INNER JOIN englishlabel ON englishlabel.id=footermenu.engId INNER JOIN language ON language.langId=englishlabel.langId WHERE englishlabel.status=0', callback)
  },
  updateSideMenu: function (id, payload, callback) {
    return db.query('UPDATE sidemenu SET pageId=?, parentMenu=?, subMenu=?, parentMenuId=?, menuOrder=?, iconLink WHERE ID=?',
      [payload.pageId, payload.menuType, payload.subMenu, payload.parentMenuId, payload.menuOrder, payload.iconLink, id], callback)
  },
  updateFooterMenu: function (id, payload, callback) {
    return db.query('UPDATE footermenu SET pageId=?, parentMenu=?, subMenu=?, parentMenuId=?, menuOrder=? WHERE ID=?',
      [payload.pageId, payload.parentMenu, payload.subMenu, payload.parentMenuId, payload.menuOrder, id], callback)
  },
  findSideMenu: function (id, callback) {
    return db.query('SELECT * FROM sidemenu WHERE ID=?',
      [id], callback)
  },
  findFooterMenu: function (id, callback) {
    return db.query('SELECT * FROM footermenu WHERE ID=?',
      [id], callback)
  },
  deleteSideMenu: function (id, callback) {
    return db.query('DELETE FROM sidemenu WHERE ID=?',
      [id], callback)
  },
  deleteFooterMenu: function (id, callback) {
    return db.query('DELETE FROM footermenu WHERE ID=?',
      [id], callback)
  }

}
module.exports = Menu
const db = require('../../dbconnection')

const Notification = {
    getAllUsers: (callback) => {
        const getAllUsersQuery = 'SELECT deviceToken from user WHERE roleId = 4 OR roleId=3 OR roleId=2;'
        return db.query(getAllUsersQuery, callback)
    },
    getAUserToken: (payload, callback) => {
        const getAUserTokenQuery = 'SELECT deviceToken from user WHERE userId = ? AND (roleId = 4 OR roleId=3 OR roleId=2);'
        return db.query(getAUserTokenQuery, payload, callback)
    },
    getUserBasedOnLocation: (payload, callback) => {
        const getTokenBasedOnLocation = `SELECT deviceToken FROM user WHERE city LIKE '%${payload}%' AND (roleId = 4 OR roleId = 3 OR roleId=2);`
        return db.query(getTokenBasedOnLocation, payload, callback)
    },
    getUserBasedOnGroup: (payload, callback) => {
        const getGroupUserTokenQuery = 'SELECT user.deviceToken FROM groupusermap INNER JOIN user ON user.userId=groupusermap.userId where groupusermap.deleted=0 AND groupusermap.groupId=? UNION SELECT user.deviceToken FROM groupleadermap INNER JOIN user ON user.userId=groupleadermap.userId where groupleadermap.deleted=0 AND groupleadermap.groupId=?;'
        return db.query(getGroupUserTokenQuery, [payload, payload], callback)
    }
}

module.exports = Notification
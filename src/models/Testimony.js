const db = require('../../dbconnection') // reference of dbconnection.js

const Testimony = {
  addTestimony: (payload, callback) => {
    const appendTestimony = 'INSERT INTO testimony SET ?'
    return db.query(appendTestimony, payload, callback)
  },
  updateHelpStatus: (payload, callback) => {
    const updateQueryStatusQuery = 'UPDATE helpDeskQuery set answered=1 WHERE id = ?'
    return db.query(updateQueryStatusQuery, payload, callback)
  },
  getTestimonyById: (payload, callback) => {
    const getQuestionsQuery = 'SELECT testimony.ID, testimony.testimonytext, testimony.testimonyAudio, testimony.testimonyVideo, testimony.createdAt, user.userId, user.firstname, user.lastname, user.email, user.phone FROM testimony JOIN user ON user.userId = testimony.testimonyBy WHERE testimony.deleted = 0 AND testimony.testimonyBy = ?;'
    return db.query(getQuestionsQuery, payload, callback)
  },
  getAllTestimony: (payload, callback) => {
    const getQuestionsQuery = 'SELECT testimony.ID, testimony.testimonytext, testimony.testimonyAudio, testimony.testimonyVideo, DATE_FORMAT(testimony.createdAt, \'%d-%m-%Y\') as createdAt, user.userId, user.firstname, user.lastname, user.email, user.phone, user.roleId FROM testimony JOIN user ON user.userId = testimony.testimonyBy WHERE testimony.deleted = 0;'
    return db.query(getQuestionsQuery, '', callback)
  }

}
module.exports = Testimony
const db = require('../../dbconnection')

const Prayer = {
  addPrayerGroup: (payload, callback) => {
    // if specific date is 0 then start date and end date will be null
    const addGroupQuery = 'INSERT INTO prayergroup SET ?'
    return db.query(addGroupQuery, payload, callback)
  },

  mapGroupSpecDate: (payload, callback) => {
    const mapGroupUserQuery = 'INSERT INTO prayergroupspecdate SET ?'
    return db.query(mapGroupUserQuery, payload, callback)
  },

  mapTitleSpecDate: (payload, callback) => {
    const mapTitleUserQuery = 'INSERT INTO prayertitlespecdate SET ?'
    return db.query(mapTitleUserQuery, payload, callback)
  },

  addPrayerTitle: (payload, callback) => {
    const addTitleQuery = 'INSERT INTO prayertitle SET ?'
    return db.query(addTitleQuery, payload, callback)
  },

  mapTitleSpecDate: (payload, callback) => {
    const mapTitleGroupQuery = 'INSERT INTO prayertitlespecdate SET ?'
    return db.query(mapTitleGroupQuery, payload, callback)
  },

  addPrayerDescription: (payload, callback) => {
    const addDescriptionQuery = 'INSERT INTO prayerdescription SET ?'
    return db.query(addDescriptionQuery, payload, callback)
  },

  mapDescriptionAndTitle: (payload, callback) => {
    const mapDescAndTitleQuery = 'INSERT INTO prayerdescriptiontitlemap SET ?'
    return db.query(mapDescAndTitleQuery, payload, callback)
  },

  getGroupForAUser: (payload, callback) => {
    const fetchGroupQuery = 'SELECT prayergroup.id, prayergroup.prGroupId, prayergroup.name, prayergroup.status, prayergroup.groupImage, prayergroup.specificDate, pspec.startDate, pspec.endDate, prayergroup.adminAccess, DATE_FORMAT(prayergroup.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, DATE_FORMAT(prayergroup.editedAt, \'%d-%m-%Y\') as updatedOn, DATE_FORMAT(prayergroup.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, user.firstname, prayergroup.thankGroup FROM prayergroup LEFT JOIN prayergroupspecdate as pspec ON pspec.groupId = prayergroup.id JOIN user ON user.userId=prayergroup.userId WHERE (prayergroup.userId=? OR user.roleId=1 OR user.roleId=99) AND (specificDate=0 OR (CURDATE() between  CONCAT(CONCAT(YEAR(now()), -(Case when SUBSTRING_INDEX(startDate,\' \',-1) = \'Jan\' then \'01\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Feb\' then \'02\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Mar\' then \'03\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Apr\' then \'04\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'May\' then \'05\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Jun\' then \'06\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Jul\' then \'07\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Aug\' then \'08\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Sep\' then \'09\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Oct\' then \'10\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Nov\' then \'11\' when SUBSTRING_INDEX(startDate,\' \',-1) = \'Dec\' then \'12\' Else 0 End)), -SUBSTRING_INDEX(startDate,\' \',1)) AND CONCAT(CONCAT(YEAR(now()), -(Case when SUBSTRING_INDEX(endDate,\' \',-1) = \'Jan\' then \'01\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Feb\' then \'02\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Mar\' then \'03\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Apr\' then \'04\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'May\' then \'05\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Jun\' then \'06\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Jul\' then \'07\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Aug\' then \'08\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Sep\' then \'09\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Oct\' then \'10\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Nov\' then \'11\' when SUBSTRING_INDEX(endDate,\' \',-1) = \'Dec\' then \'12\' Else 0 End)), -SUBSTRING_INDEX(endDate,\' \',1)))) order by prayergroup.adminAccess ASC, prayergroup.createdAt ASC;'
    return db.query(fetchGroupQuery, payload, callback)
  },

  getTitleForGroup: (payload, callback) => {
    const fetchTitleQuery = 'SELECT t.id, t.prTitleId, t.name, t.status, t.specificDate, pts.startDate, pts.endDate, t.adminAccess, DATE_FORMAT(t.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, DATE_FORMAT(t.editedAt, \'%d-%m-%Y\') as updatedOn, DATE_FORMAT(t.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, pts.titleId, (case when (t.specificDate=0 OR (CURDATE() between  CONCAT(CONCAT(YEAR(now()), -(Case when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Jan\' then \'01\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Feb\' then \'02\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Mar\' then \'03\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Apr\' then \'04\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'May\' then \'05\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Jun\' then \'06\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Jul\' then \'07\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Aug\' then \'08\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Sep\' then \'09\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Oct\' then \'10\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Nov\' then \'11\' when SUBSTRING_INDEX(pts.startDate,\' \',-1) = \'Dec\' then \'12\' Else 0 End)), -SUBSTRING_INDEX(pts.startDate,\' \',1)) AND CONCAT(CONCAT(YEAR(now()), -(Case when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Jan\' then \'01\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Feb\' then \'02\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Mar\' then \'03\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Apr\' then \'04\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'May\' then \'05\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Jun\' then \'06\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Jul\' then \'07\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Aug\' then \'08\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Sep\' then \'09\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Oct\' then \'10\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Nov\' then \'11\' when SUBSTRING_INDEX(pts.endDate,\' \',-1) = \'Dec\' then \'12\' Else 0 End)), -SUBSTRING_INDEX(pts.endDate,\' \',1)))) then 1 else 0 end) as prayerTitSpecificDateStatus FROM prayertitle t LEFT JOIN prayertitlespecdate pts ON pts.titleId = t.id LEFT JOIN user u ON u.userId=t.userId WHERE t.prGroupId=? AND (t.userId=? OR u.roleId=1 OR u.roleId=99) order by t.createdAt ASC;'
    return db.query(fetchTitleQuery, [payload.prGroupId, payload.userId], callback)
  },

  getDescriptionForTitle: (payload, callback) => {
    const fetchDescQuery = `SELECT prayerdescription.prDescriptionId as descriptionId, prayerdescription.name, prayerdescription.status, prayerdescription.answered, DATE_FORMAT(prayerdescription.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(prayerdescription.editedAt, \'%Y-%m-%d %H:%i\') as editedAt FROM prayerdescription INNER JOIN user u ON prayerdescription.userId=u.userId WHERE prayerdescription.prGroupId = ? AND prayerdescription.prTitleId = ? AND (prayerdescription.userId = ? OR u.roleId = 1 OR u.roleId = 99) AND prayerdescription.answered=0;`
    return db.query(fetchDescQuery, [payload.prGroupId, payload.prTitleId, payload.userId], callback)
  },

  getDescriptionForTitleByAdmin: (payload, callback) => {
    const fetchDescQuery = `SELECT prayerdescription.prDescriptionId as descriptionId, prayerdescription.name, prayerdescription.status, prayerdescription.answered, DATE_FORMAT(prayerdescription.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(prayerdescription.editedAt, \'%Y-%m-%d %H:%i\') as editedAt FROM prayerdescription INNER JOIN user u ON prayerdescription.userId=u.userId WHERE prayerdescription.prGroupId = ? AND prayerdescription.prTitleId = ? AND (prayerdescription.userId = ? OR u.roleId = 1 OR u.roleId = 99);`
    return db.query(fetchDescQuery, [payload.prGroupId, payload.prTitleId, payload.userId], callback)
  },

  getAnsweredPrayer: (callback) => {
    const fetchAnsPrayer = 'SELECT prayerdescription.id, prayerdescription.name, prayerdescription.answered, DATE_FORMAT(prayerdescription.createdAt, \'%Y-%m-%d\') as createdAt, DATE_FORMAT(prayerdescription.answeredat, \'%Y-%m-%d\') as answeredAt, prayertitlegroupmap.groupId, prayertitlegroupmap.titleId FROM prayerdescription JOIN prayerdescriptiontitlemap ON prayerdescriptiontitlemap.descriptionId = prayerdescription.id JOIN prayertitlegroupmap ON prayertitlegroupmap.titleId=prayerdescriptiontitlemap.titleId JOIN prayergroupusermap ON prayergroupusermap.groupId=prayertitlegroupmap.groupId JOIN user ON user.userId=prayergroupusermap.userId WHERE prayerdescription.deleted = 0 AND prayerdescription.answered=1 AND (user.roleId!=99 AND user.roleId!=5);'
    return db.query(fetchAnsPrayer, callback)
  },

  getAnsweredPrayerByuId: (payload, callback) => {
    const fetchAnsUserPrayer = 'SELECT prayerdescription.prGroupId as groupId, prayerdescription.prTitleId as titleId, prayerdescription.prDescriptionId as descriptionId, prayerdescription.name as descriptionName, prayerdescription.answered, DATE_FORMAT(prayerdescription.createdAt, \'%Y-%m-%d\') as createdAt, DATE_FORMAT(prayerdescription.answeredat, \'%Y-%m-%d\') as answeredAt, prayertitle.name as titleName FROM prayerdescription JOIN prayertitle ON prayertitle.prTitleId = prayerdescription.prTitleId JOIN prayergroup ON prayergroup.prGroupId=prayerdescription.prGroupId JOIN user ON user.userId=prayerdescription.userId WHERE prayertitle.prGroupId=prayergroup.prGroupId AND prayerdescription.answered=1 AND (prayerdescription.userId=? AND prayertitle.userId=? AND prayergroup.userId=? OR user.roleId=1) AND user.roleId!=5 order by prayerdescription.answeredat desc'
    return db.query(fetchAnsUserPrayer, [payload, payload, payload], callback)
  },

  getGroup: (payload, callback) => {
    const fetchGroupQuery = 'SELECT * FROM prayergroup WHERE id = ?'
    db.query(fetchGroupQuery, payload, callback)
  },

  getTitle: (payload, callback) => {
    const fetchTitleQuery = 'SELECT * FROM prayertitle WHERE id = ?'
    db.query(fetchTitleQuery, payload, callback)
  },

  getDescription: (payload, callback) => {
    const fetchDescQuery = 'SELECT * FROM prayerdescription WHERE id = ?'
    db.query(fetchDescQuery, payload, callback)
  },

  deleteGroup: (payload, callback) => {
    const deleteGroupQuery = 'DELETE FROM prayergroup WHERE prGroupId = ? AND (userId = ? OR ((select roleId from user u INNER JOIN prayergroup pg ON pg.userId=u.userId where u.userId=? LIMIT 1)=1 OR (select roleId from user u INNER JOIN prayergroup pg ON pg.userId=u.userId where u.userId=? LIMIT 1)=99))'
    db.query(deleteGroupQuery, [payload.prGroupId, payload.userId, payload.userId, payload.userId], callback)
  },

  deleteTitle: (payload, callback) => {
    const deleteTitleQuery = 'DELETE FROM prayertitle WHERE prGroupId = ? AND prTitleId = ? AND userId = ?'
    db.query(deleteTitleQuery, [payload.prGroupId, payload.prTitleId, payload.userId], callback)
  },

  deleteDescription: (payload, callback) => {
    const deleteDescQuery = 'DELETE FROM prayerdescription WHERE prGroupId = ? AND prTitleId = ? AND prDescriptionId = ? AND userId = ?'
    db.query(deleteDescQuery, [payload.prGroupId, payload.prTitleId, payload.prDescriptionId, payload.userId], callback)
  },

  requestAnswered: (payload, callback) => {
    const updateAnsweredQuery = `UPDATE prayerdescription SET answered = 1, answeredat = ? WHERE prDescriptionId = ? AND userId = ?`
    db.query(updateAnsweredQuery, [payload.answeredat, payload.descriptionId, payload.userId], callback)
  },

  requestAnsweredForAdmin: (payload, callback) => {
    const updateAnsweredQuery = `UPDATE prayerdescription SET answered = 1, answeredat = ? WHERE prDescriptionId = ? AND prTitleId = ? AND prGroupId = ? AND userId = ?`
    db.query(updateAnsweredQuery, [payload.answeredat, payload.descriptionId, payload.titleId, payload.groupId, payload.userId], callback)
  },

  updateGroup: (payload, callback) => {
    const updateGroupQuery = 'UPDATE prayergroup SET ? WHERE userId = ? AND prGroupId = ?'
    db.query(updateGroupQuery, [payload, payload.userId, payload.prGroupId], callback)
  },

  prGrpSpecDateUpdate: (payload, callback) => {
    const mapGroupUserQuery = 'UPDATE prayergroupspecdate SET ? WHERE userId = ? AND groupId = ?'
    return db.query(mapGroupUserQuery, [payload, payload.userId, payload.groupId], callback)
  },

  prTitSpecDateUpdate: (payload, callback) => {
    const mapGroupTitleQuery = 'UPDATE prayertitlespecdate SET ? WHERE titleId = ?'
    return db.query(mapGroupTitleQuery, [payload, payload.titleId], callback)
  },

  getPrGrpSpeciDate: (payload, callback) => {
    const getGroupQuery = 'SELECT id, specificDate from prayergroup INNER JOIN user ON user.userId=prayergroup.userId WHERE prayergroup.prGroupId = ? AND (prayergroup.userId = ? OR user.roleId=? OR user.roleId=?)'
    db.query(getGroupQuery, [payload.prGroupId, payload.userId, payload.admin, payload.superAdmin], callback)
  },

  getPrTitSpeciDate: (payload, callback) => {
    const getTitleQuery = 'SELECT prayertitle.id, prayertitle.specificDate from prayertitle INNER JOIN user ON user.userId=prayertitle.userId WHERE prayertitle.prGroupId = ? AND prayertitle.prTitleId = ? AND (prayertitle.userId=? OR user.roleId=? OR user.roleId=?)'
    db.query(getTitleQuery, [payload.prGroupId, payload.prTitleId, payload.userId, payload.admin, payload.superAdmin], callback)
  },

  getPrDescSpeciDate: (payload, callback) => {
    const getDescriptionQuery = 'SELECT id from prayerdescription WHERE prGroupId = ? AND prTitleId = ? AND prDescriptionId = ? AND userId=?'
    db.query(getDescriptionQuery, [payload.prGroupId, payload.prTitleId, payload.prDescriptionId, payload.userId], callback)
  },

  findspecificdateAlreadyExist: (gId, uId, callback) => {
    const updateGroupQuery = 'SELECT * from prayergroupspecdate INNER JOIN user ON user.userId=prayergroupspecdate.userId WHERE prayergroupspecdate.groupId = ? AND (prayergroupspecdate.userId = ? OR user.roleId=1 OR user.roleId=99)'
    db.query(updateGroupQuery, [gId, uId], callback)
  },

  findspecificdateForTitleAlreadyExist: (tId, callback) => {
    const updateTitleQuery = 'SELECT * from prayertitlespecdate WHERE titleId = ?'
    db.query(updateTitleQuery, [tId], callback)
  },

  deletePrGrpSpecificDate: (gId, callback) => {
    const delGroupQuery = 'DELETE FROM prayergroupspecdate WHERE id = ?'
    db.query(delGroupQuery, [gId], callback)
  },

  deletePrTitSpecificDate: (tId, callback) => {
    const delGroupQuery = 'DELETE FROM prayertitlespecdate WHERE titleId = ?'
    db.query(delGroupQuery, [tId], callback)
  },

  updateGroupByAdmin: (payload, callback) => {
    const updateGroupQuery = 'UPDATE prayergroup SET name = ?, groupImage = ?, addedby=?, status=?, editedAt=? WHERE id =?'
    db.query(updateGroupQuery, [payload.name, payload.groupImage, payload.userId, payload.status, payload.editedAt, payload.id], callback)
  },

  updateTitle: (payload, callback) => {
    const updateTitleQuery = 'UPDATE prayertitle SET ? WHERE prGroupId = ? AND prTitleId = ? AND userId = ?'
    db.query(updateTitleQuery, [payload, payload.prGroupId, payload.prTitleId, payload.userId], callback)
  },
  selectGroupById: function (payload, callback) {
    return db.query('SELECT * FROM prayergroup WHERE id = ? AND deleted = 0', [payload], callback)
  },
  selectGroupByUId: function (payload, callback) {
    return db.query('SELECT prayergroup.* FROM prayergroup INNER JOIN prayergroupusermap ON prayergroup.id=prayergroupusermap.groupId INNER JOIN user On user.userId=prayergroupusermap.userId WHERE prayergroup.id = ? AND prayergroup.deleted = 0 AND (prayergroupusermap.userId=?)', [payload.groupId, payload.userId], callback)
  },
  checkGrpIdExist: function (payload, callback) {
    return db.query('SELECT prGroupId FROM prayergroup INNER JOIN user ON user.userId=prayergroup.userId WHERE (prayergroup.userId = ? OR user.roleId = ? OR user.roleId = ?)', [payload.userId, payload.admin, payload.superAdmin], callback)
  },
  checkUserIdGrpIdExist: function (uId, gId, role, callback) {
    return db.query('SELECT prGroupId FROM prayergroup INNER JOIN user ON user.userId=prayergroup.userId WHERE prayergroup.prGroupId = ? AND (prayergroup.userId = ? OR user.roleId=? OR user.roleId=?)', [gId, uId, role.admin, role.superAdmin], callback)
  },
  checkTitIdExist: function (uId, gId, role, callback) {
    return db.query('SELECT prTitleId FROM prayertitle INNER JOIN user ON user.userId=prayertitle.userId WHERE prayertitle.prGroupId = ? AND prayertitle.prTitleId = ? AND (prayertitle.userId = ? OR user.roleId=? OR user.roleId=?)', [gId, role.titleId, uId, role.admin, role.superAdmin], callback)
  },
  checkDescIdExist: function (uId, gId, tId, role, callback) {
    return db.query('SELECT prDescriptionId, prTitleId, prGroupId FROM prayerdescription INNER JOIN user ON user.userId=prayerdescription.userId WHERE prayerdescription.prGroupId = ? AND prayerdescription.prTitleId = ? AND (prayerdescription.userId = ? OR user.roleId=? OR user.roleId=?)', [gId, tId, uId, role.admin, role.superAdmin], callback)
  },
  checkGrpIdHasTitle: function (payload, callback) {
    return db.query('SELECT * FROM prayertitlegroupmap WHERE groupId = ?', [payload], callback)
  },

  selectTitleById: function (payload, callback) {
    return db.query('SELECT * FROM prayertitle WHERE id = ? AND deleted = 0', [payload], callback)
  },
  selectDescriptionById: function (uId, payload, callback) {
    return db.query('SELECT * FROM prayerdescription WHERE userId = ? AND prDescriptionId IN (?)', [uId, payload], callback)
  },

  selectDescriptionByIdForAdmin: function (uId, payload, callback) {
    return db.query('SELECT * FROM prayerdescription WHERE userId = ? AND prDescriptionId IN (?) AND prTitleId IN (?) AND prGroupId IN (?)', [uId, payload.desc, payload.title, payload.group], callback)
  },

  updateDescription: (payload, callback) => {
    const updateDescQuery = 'UPDATE prayerdescription SET ? WHERE prGroupId =? AND prTitleId = ? AND prDescriptionId = ? AND userId = ?'
    db.query(updateDescQuery, [payload, payload.prGroupId, payload.prTitleId, payload.prDescriptionId, payload.userId], callback)
  }
}

module.exports = Prayer

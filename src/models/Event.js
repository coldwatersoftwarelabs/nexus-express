const db = require('../../dbconnection') // reference of dbconnection.js

const Event = {

  addEvent: function (payload, callback) {
    const today = new Date()
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    const dateTime = date + ' ' + time
    return db.query('INSERT INTO event (title, excerpt, content, bannerImageUrl, status, language, editedBy, addedBy, createdAt, editedAt, expire, location, latitude, longitude, startDate, endDate, featured, eventDate)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [payload.title, payload.excerpt, payload.content, payload.bannerImageUrl, payload.status, payload.language, payload.editedBy, payload.addedBy, dateTime, dateTime, payload.expire, payload.location, payload.latitude, payload.longitude, payload.startDate, payload.endDate, payload.featured, payload.eventDate], callback)
  },
  viewAllPublishedEvent: function (callback) {
    return db.query('SELECT event.ID, event.title, event.excerpt, event.content, event.bannerImageUrl, event.contentImageUrl, event.contentImagePostition, event.url, event.status, event.language, event.location, event.latitude, event.longitude, DATE_FORMAT(event.startDate, \'%Y-%m-%d %H:%i\') as startDate, DATE_FORMAT(event.endDate, \'%Y-%m-%d %H:%i\') as endDate, event.order, event.editedBy, event.addedBy, DATE_FORMAT(event.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(event.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, event.featured, event.expire, language.langType FROM event INNER JOIN language ON language.langId=event.language  WHERE event.status=\'Published\' AND (CURRENT_DATE BETWEEN event.startDate AND event.endDate)', callback)
  },
  viewFeaturedEvent: function (payload, callback) {
    return db.query('SELECT event.ID, event.title, event.excerpt, event.content, event.bannerImageUrl, event.contentImageUrl, event.contentImagePostition, event.url, event.status, event.language, event.location, event.latitude, event.longitude, DATE_FORMAT(event.startDate, \'%Y-%m-%d %H:%i\') as startDate, DATE_FORMAT(event.endDate, \'%Y-%m-%d %H:%i\') as endDate, event.order, event.editedBy, event.addedBy, DATE_FORMAT(event.createdAt, \'%Y-%m-%d %H:%i\') as createdAt, DATE_FORMAT(event.editedAt, \'%Y-%m-%d %H:%i\') as editedAt, event.featured, event.expire, language.langType FROM event INNER JOIN language ON language.langId=event.language  WHERE event.status=\'Published\' AND (CURRENT_DATE BETWEEN event.startDate AND event.endDate) AND event.featured=1 AND language=?;', [payload], callback)
  },
  viewAllEvent: function (callback) {
    return db.query('SELECT event.ID, event.title, event.eventDate, event.excerpt, event.content, event.bannerImageUrl, event.contentImageUrl, event.contentImagePostition, event.url, event.status, event.language, event.location, event.latitude, event.longitude, event.startDate, event.endDate, event.order, event.editedBy, event.addedBy, DATE_FORMAT(event.createdAt, \'%Y-%m-%d\') as createdAt, DATE_FORMAT(event.editedAt, \'%Y-%m-%d\') as editedAt, event.expire, DATE_FORMAT(event.editedAt, \'%d-%m-%Y\') as updatedOn, event.featured, event.addedBy, user.firstname, user.lastname, language.langType FROM event INNER JOIN language ON language.langId=event.language INNER JOIN user ON user.userId=event.addedBy;', callback)
  },
  updateEvent: function (ID, payload, callback) {
    const today = new Date()
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
    const dateTime = date + ' ' + time
    return db.query('UPDATE event SET title = ?, eventDate = ?, excerpt = ? , content = ? , bannerImageUrl = ? , status = ?, language = ?, editedBy = ?, editedAt = ?, expire = ?, location = ?, latitude = ?, longitude = ?, startDate = ? , endDate = ?, featured = ? WHERE ID=?',
      [payload.title, payload.eventDate, payload.excerpt, payload.content, payload.bannerImageUrl, payload.status, payload.language, payload.editedBy, dateTime, payload.expire, payload.location, payload.latitude, payload.longitude, payload.startDate, payload.endDate, payload.featured, ID], callback)
  },
  deleteEvent: function (id, callback) {
    return db.query('DELETE FROM event WHERE ID=?',
      [id], callback)
  }

}
module.exports = Event
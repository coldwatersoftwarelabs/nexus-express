const db = require('../../dbconnection')

const Church = {
  addChurch: function (payload, callback) {
    return db.query('INSERT into church SET ?', payload.data, callback)
  },
  deleteRole: function (payload, callback) {
    return db.query('delete from role where roleId=?', [payload.roleId], callback)
  },
  getAllChurchList: function (callback) {
    return db.query('SELECT c.churchId, c.churchName, c.denominationId, c.countryId, c.churchAddress1, c.churchAddress2, c.churchAddress3, cit.cityId, cit.cityName, s.stateId, s.stateName, c.churchPincode, c.contactName1, c.contactNo1, c.contactDesignation1, c.contactName2, c.contactNo2, c.contactDesignation2, c.signedStatus, c.latitude, c.longitude, c.status, con.countryName, d.denominationName FROM church as c INNER JOIN country as con ON c.countryId = con.countryId INNER JOIN denomination as d ON c.denominationId = d.denominationId INNER JOIN state as s ON s.stateId=c.stateId INNER JOIN city as cit ON cit.cityId=c.cityId', callback)
  },
  updateChurch: function (payload, callback) {
    return db.query('UPDATE church set ? WHERE churchId=?',
      [payload.data, payload.data.churchId], callback)
  },
  deleteStatusForChurch: function (payload, callback) {
    return db.query('UPDATE church set status=? WHERE churchId=?',
      [payload.data.status, payload.data.churchId], callback)
  },
  grpProvinceList: (callback) => {
    const groupproListQuery = 'SELECT *, (select count(userId) from groupusermap INNER JOIN appusergroup ON appusergroup.id=groupusermap.groupId WHERE appusergroup.stateId=state.stateId AND groupusermap.deleted=0 AND appusergroup.deleted=0) as userCount, (select count(id) from appusergroup WHERE appusergroup.stateId=state.stateId AND appusergroup.deleted=0) as groupCount FROM state WHERE status=0;'
    return db.query(groupproListQuery, callback)
  },
}

module.exports = Church

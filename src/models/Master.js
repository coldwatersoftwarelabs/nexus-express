const db = require('../../dbconnection')

const Master = {
  getAllRoleType: function (callback) {
    return db.query('SELECT * FROM role  WHERE status=0', callback)
  },
  createRole: function (payload, callback) {
    return db.query('INSERT INTO role (roleType) VALUES (?)', [payload.data.roleType], callback)
  },
  updateRole: function (payload, callback) {
    return db.query('UPDATE role SET roleType=? WHERE roleId=?',
      [payload.roleType, payload.roleId], callback)
  },
  roleStatusForDelete: function (payload, callback) {
    return db.query('UPDATE role SET status=? WHERE roleId=?',
      [payload.data.status, payload.data.roleId], callback)
  },
  getAllLevel: function (callback) {
    return db.query('SELECT * FROM level WHERE status=0', callback)
  },
  createLevel: function (payload, callback) {
    return db.query('INSERT INTO level (levelName) VALUES (?)', [payload.data.levelName], callback)
  },
  updateLevel: function (payload, callback) {
    return db.query('UPDATE level SET levelName=? WHERE levelId=?',
      [payload.levelName, payload.levelId], callback)
  },
  levelStatusForDelete: function (payload, callback) {
    return db.query('UPDATE level SET status=? WHERE levelId=?',
      [payload.data.status, payload.data.levelId], callback)
  },
  getAllCategory: function (callback) {
    return db.query('SELECT * FROM categories WHERE status=0', callback)
  },
  createCategory: function (payload, callback) {
    return db.query('INSERT INTO categories (categoryName) VALUES (?)', [payload.data.categoryName], callback)
  },
  updateCategory: function (payload, callback) {
    return db.query('UPDATE categories SET categoryName=? WHERE categoryId=?',
      [payload.categoryName, payload.categoryId], callback)
  },
  categoryStatusForDelete: function (payload, callback) {
    return db.query('UPDATE categories SET status=? WHERE categoryId=?',
      [payload.data.status, payload.data.categoryId], callback)
  },
  getAllCountry: function (callback) {
    return db.query('SELECT * FROM country WHERE status=0', callback)
  },
  createCountry: function (payload, callback) {
    return db.query('INSERT INTO country (countryId, countryName) VALUES (?,?)', [payload.data.countryId, payload.data.countryName], callback)
  },
  updateCountry: function (payload, callback) {
    return db.query('UPDATE country SET countryName=? WHERE countryId=?',
      [payload.countryName, payload.countryId], callback)
  },
  countryStatusForDelete: function (payload, callback) {
    return db.query('UPDATE country SET status=? WHERE countryId=?',
      [payload.data.status, payload.data.countryId], callback)
  },
  getAllState: function (callback) {
    return db.query('SELECT state.*, country.countryName FROM state INNER JOIN country ON country.countryId=state.countryId WHERE state.status=0', callback)
  },
  createState: function (payload, callback) {
    return db.query('INSERT INTO state (countryId, stateName) VALUES (?,?)', [payload.data.countryId, payload.data.stateName], callback)
  },
  updateState: function (payload, callback) {
    return db.query('UPDATE state SET stateName=?, countryId=? WHERE stateId=?',
      [payload.stateName, payload.countryId, payload.stateId], callback)
  },
  stateStatusForDelete: function (payload, callback) {
    return db.query('UPDATE state SET status=? WHERE stateId=?',
      [payload.data.status, payload.data.stateId], callback)
  },
  getAllCity: function (callback) {
    return db.query('SELECT ci.cityId, ci.countryId, s.stateId, ci.cityName, ci.status, s.stateName, co.countryName FROM city as ci INNER JOIN state as s ON s.stateId=ci.stateId INNER JOIN country as co ON co.countryId=ci.countryId WHERE ci.status=0 AND co.status=0 AND s.status=0', callback)
  },
  createCity: function (payload, callback) {
    return db.query('INSERT INTO city (countryId, stateId, cityName) VALUES (?,?,?)', [payload.data.countryId, payload.data.stateId, payload.data.cityName], callback)
  },
  updateCity: function (payload, callback) {
    return db.query('UPDATE city SET countryId=?, stateId=?, cityName=? WHERE cityId=?',
      [payload.countryId, payload.stateId, payload.cityName, payload.cityId], callback)
  },
  cityStatusForDelete: function (payload, callback) {
    return db.query('UPDATE city SET status=? WHERE cityId=?',
      [payload.data.status, payload.data.cityId], callback)
  },
  getAllDenomination: function (callback) {
    return db.query('SELECT * FROM denomination WHERE status=0', callback)
  },
  createDenomination: function (payload, callback) {
    return db.query('INSERT INTO denomination (denominationId, denominationName) VALUES (?,?)', [payload.data.denominationId, payload.data.denominationName], callback)
  },
  updateDenomination: function (payload, callback) {
    return db.query('UPDATE denomination SET denominationName=? WHERE denominationId=?',
      [payload.denominationName, payload.denominationId], callback)
  },
  denominationStatusForDelete: function (payload, callback) {
    return db.query('UPDATE denomination SET status=? WHERE denominationId=?',
      [payload.data.status, payload.data.denominationId], callback)
  },
  deleteRole: function (payload, callback) {
    return db.query('DELETE from role WHERE roleId=?', [payload.roleId], callback)
  },
  getAllMediaClassify: function (callback) {
    return db.query('SELECT * FROM mediaclassification WHERE status=0', callback)
  },
  getAllPriority: function (callback) {
    return db.query('SELECT * FROM querypriority WHERE status=0', callback)
  },
  createMediaClassify: function (payload, callback) {
    return db.query('INSERT INTO mediaclassification (mediaClassifyName) VALUES (?)', [payload.data.mediaClassifyName], callback)
  },
  createPriority: function (payload, callback) {
    return db.query('INSERT INTO querypriority (priority) VALUES (?)', [payload.data.priority], callback)
  },
  updateMediaClassification: function (payload, callback) {
    return db.query('UPDATE mediaclassification SET mediaClassifyName=? WHERE mediaClassifyId=?',
      [payload.mediaClassifyName, payload.mediaClassifyId], callback)
  },
  updatePriority: function (payload, callback) {
    return db.query('UPDATE querypriority SET priority=? WHERE id=?',
      [payload.priority, payload.priorityId], callback)
  },
  mediaClassificationDelete: function (payload, callback) {
    return db.query('UPDATE mediaclassification SET status=? WHERE mediaClassifyId=?',
      [payload.status, payload.mediaClassifyId], callback)
  },
  priorityDelete: function (payload, callback) {
    return db.query('UPDATE querypriority SET status=? WHERE id=?',
      [payload.data.status, payload.data.priorityId], callback)
  }
}

module.exports = Master
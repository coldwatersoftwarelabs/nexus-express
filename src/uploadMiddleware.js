const multer = require('multer')
const ImageStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './../uploads/images')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname.split(' ').join('-'))
  }
})

const VideoStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './../uploads/videos')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname.split(' ').join('-'))
  }
})

const PDFStorage = multer.diskStorage({
  destination: function (req, pdf, cb) {
    cb(null, './../uploads/pdf/')
  },
  filename: function (req, pdf, cb) {
    cb(null, Date.now() + pdf.originalname.split(' ').join('-'))
  }
})

const DocStorage = multer.diskStorage({
  destination: function (req, word, cb) {
    cb(null, './../uploads/docs/')
  },
  filename: function (req, word, cb) {
    cb(null, Date.now() + word.originalname.split(' ').join('-'))
  }
})

const AudioStorage = multer.diskStorage({
  destination: function (req, word, cb) {
    cb(null, './../uploads/audio/')
  },
  filename: function (req, word, cb) {
    cb(null, Date.now() + word.originalname.split(' ').join('-'))
  }
})

const IconStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './../uploads/icon/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname.split(' ').join('-'))
  }
})

const uploadImage = multer({
  storage: ImageStorage,
  limits: {
    fileSize: process.env.IMAGE_UPLOAD * 1024 * 1024
  }
})

const uploadIcon = multer({
  storage: IconStorage,
  limits: {
    fileSize: process.env.IMAGE_UPLOAD * 32 * 32
  }
})

const uploadVideo = multer({
  storage: VideoStorage
})

const uploadAudio = multer({
  storage: AudioStorage
})

const uploadPDF = multer({
  storage: PDFStorage
  // limits: {
  //   fileSize: process.env.PDF_UPLOAD * 1024 * 1024
  // }
})

const uploadDocs = multer({
  storage: DocStorage
  // limits: {
  //   fileSize: process.env.DOC_UPLOAD * 1024 * 1024
  // }
})

module.exports = {
  uploadImage,
  uploadVideo,
  uploadPDF,
  uploadDocs,
  uploadAudio,
  uploadIcon
}
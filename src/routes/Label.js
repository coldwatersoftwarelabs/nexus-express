const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()
const Label = require('../models/Label')
const User = require('../models/User')
const Updates = require('../models/Updates')
const _ = require('underscore')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')


// get only english label
engLabFunc = () => {
  return new Promise((resolve, reject) => {
    let arrData
    Label.getEnglishLabel((err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}


// get all the label by langId excerpt english
allotherLabFunc = (lb) => {
  return new Promise((resolve, reject) => {
    let arrData
    Label.getAllLabelByLang(lb, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}



getLabelData = (row) => {
  return new Promise(async (resolve, reject) => {
    const labelArray = []
    const promiseArray = []
    _.each(_.groupBy(row, 'langId'), label => {
      const arrData = [];
      async function getData() {
        if(label[0].langId===1) {
          // call english label func
          return await engLabFunc();
        } else {
          // call other label func
          return await allotherLabFunc(label[0].langId);
        }
      }  
      (async () => {
        // calling async function getData
        const Data = await getData()
          _.each(Data, e => {
            arrData.push({
              engId: e.engId,
              langId: e.langId,
              labelId: e.labelId,
              name: e.name,
              labelName: e.labelName,
              appId: e.appId,
              labelImage: e.labelImage,
              status: e.status
            })
          })
          return arrData;
      })()
      promiseArray.push(
          getData().then(result=> {
            const labelData = {
              langId: label[0].langId,
              langType: label[0].langType,
              OriginalLangtext: label[0].OriginalLangtext,
              status: label[0].status,
              data: result
            }
            labelArray.push(labelData)
          })
        )
    })
    Promise.all(promiseArray).then(() => {
      resolve(_.sortBy(Array.prototype.concat.apply([], labelArray), 'langId'))
    })
  })
}


// create language
router.post('/LangType', VerifyToken, jsonParser, [
  check('data.OriginalLangtext').notEmpty().withMessage('Should not be empty'),
  check('data.langType').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user existence
      const userCount = await userCheck(req.query.userId)
      if ((userCount > 0)) {
        // add language
        Label.addLangType(req.body, function (err, count) {
          if (err) {
            res.json(err)
          } else {
            if (count.insertId) {
              // set default langId as last inserted id
              Label.setDefaultLangId(count.insertId, async function (err, cou) {
                if (err) throw err
                // add english label as common
                Label.addCommonEng(async function (err, co) {
                  if (err) throw err
                  // add updates in update table
                  var contver = await getVersion(1)
                  var dataVer = parseInt(contver.slice(5, 11)) + 1
                  contver = contver.slice(0, 5) + '' + dataVer
                  Updates.editUpdates(1, contver, function (err, rrr) {
                    if (err) return res.status(500).send('Server error!')
                    if (rrr.length > 0) {
                      res.json(req.body)
                    } else {
                      res.json(req.body)
                    }
                  })
                })
              })
            }
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// add english label
router.post('/EnglishLabel', VerifyToken, jsonParser, [
  check('langId').isNumeric().withMessage('Must be only numbers'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('name').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user and language existence
      const userCount = await userCheck(req.query.userId)
      const langCount = await langCheck(req.body.langId)
      if ((userCount > 0) && (langCount > 0)) {
        Label.addEnglishLabel(req.body, function (err, count) {
          if (err) {
            res.json(err)
          } else {
            Label.getLanguageExcerptEng((err, rows) => {
              if (err) throw err
              if (rows.length > 0) {
                const data = []
                _.each(rows, r => {
                  data.push([
                    r.langId,
                    req.body.name,
                    count.insertId
                  ])
                })
                // insert last inserted english label to all other label
                Label.addAllLabel(data, async function (err, co) {
                  if (err) {
                    res.json(err)
                  } else {
                    // add updates in update table
                    var contver = await getVersion(2)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(2, contver, function (err, rrr) {
                      if (err) return res.status(500).send('Server error!')
                      if (rrr.length > 0) {
                        res.json(count.insertId)
                      } else {
                        res.json(count.insertId)
                      }
                    })
                  }
                })
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// get all languages in system
router.get('/language', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.query.userId, function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (rows.length === 1) {
        Label.getLanguage(function (err, rows) {
          if (err) return res.status(500).send('Server error!')
          res.json({
            type: 'success',
            msg: 'Got successfully',
            rows
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User found'
        })
      }
    })
  })
})

// get all labels based on langId in a system
router.get('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // get languages
    Label.getLanguage(async (err, lang) => {
      if (err) return res.status(500).send('Server error!')
      if(lang.length>0) {
        const rows = await getLabelData(lang)
        res.json({ type: 'success', msg: 'Got successfully', rows})
      } else {
        res.json({
          type: 'success',
          msg: 'No data exist',
          rows
        })
      }
    })
  })
})

// get all english labels in a system
router.get('/getEnglishLabel', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.query.userId, function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (rows.length === 1) {
        Label.getEnglishLabel(function (err, rows) {
          if (err) return res.status(500).send('Server error!')
          res.json({
            type: 'success',
            msg: 'Got successfully',
            rows
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User found'
        })
      }
    })
  })
})

// get label based on langId
router.get('/getAllLabel/:id', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  },
  id: {
    in: ['path'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user and language existence
    const userCount = await userCheck(req.query.userId)
    const langCount = await langCheck(req.params.id)
    if ((userCount > 0) && (langCount > 0)) {
      Label.getAllLabelByLang(req.params.id, function (err, rows) {
        if (err) return res.status(500).send('Server error!')
        res.json({
          type: 'success',
          msg: 'Got successfully',
          rows
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// update language
router.post('/:id', VerifyToken, jsonParser, [
  check('OriginalLangtext').notEmpty().withMessage('Should not be empty'),
  check('langType').notEmpty().withMessage('Should not be empty'),
  check('langId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user and language existence
      const userCount = await userCheck(req.query.userId)
      const langCount = await langCheck(req.body.langId)
      if ((userCount > 0) && (langCount > 0)) {
        Label.updateLangType(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(1)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(1, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// update english label
router.post('/EnglishLabel/:id', VerifyToken, jsonParser, [
  check('name').notEmpty().withMessage('Should not be empty'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('langId').isNumeric().withMessage('Must be only numbers'),
  check('engId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user, language and eng label existence
      const userCount = await userCheck(req.query.userId)
      const langCount = await langCheck(req.body.langId)
      const EngCount = await engCheck(req.body.engId)
      if ((userCount > 0) && (langCount > 0) && (EngCount > 0)) {
        Label.updateEnglishLabel(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(2)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(2, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// update all label excerpt english
router.post('/updateAllLabel/:id', VerifyToken, jsonParser, [
  check('labelName').notEmpty().withMessage('Should not be empty'),
  check('labelId').isNumeric().withMessage('Must be only numbers'),
  check('langId').isNumeric().withMessage('Must be only numbers'),
  check('engId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // chheck user, language, english label and all other label existence
      const userCount = await userCheck(req.query.userId)
      const langCount = await langCheck(req.body.langId)
      const EngCount = await engCheck(req.body.engId)
      const labelCount = await labelCheck(req.body.labelId)
      if ((userCount > 0) && (langCount > 0) && (EngCount > 0) && (labelCount > 0)) {
        Label.updateAllLabel(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(2)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(2, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// delete language
router.put('/deletelang', VerifyToken, jsonParser, [
  check('langId').isNumeric().withMessage('Must be only numbers')
], checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user and language existence
      const userCount = await userCheck(req.query.userId)
      const langCount = await langCheck(req.body.langId)
      if ((userCount > 0) && (langCount > 0)) {
        Label.langStatusForDelete(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(1)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(1, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// delete english label
router.put('/delete-english', VerifyToken, jsonParser, [
  check('id').isNumeric().withMessage('Must be only numbers')
], checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user and english label existence
      const userCount = await userCheck(req.query.userId)
      const engCount = await engCheck(req.body.id)
      if ((userCount > 0) && (engCount > 0)) {
        Label.engLabelStatusForDelete(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(2)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(2, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// delete all other label excerpt english
router.put('/delete-allLabel', VerifyToken, jsonParser, [
  check('labelId').isNumeric().withMessage('Must be only numbers')
], checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if ((typeof (req.query.cms) === 'string') && req.query.userId) {
      // check user and all label existence
      const userCount = await userCheck(req.query.userId)
      const labelCount = await labelCheck(req.body.labelId)
      if ((userCount > 0) && (labelCount > 0)) {
        Label.allLabelStatusForDelete(req.body, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            // add updates in update table
            var contver = await getVersion(2)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(2, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

module.exports = router

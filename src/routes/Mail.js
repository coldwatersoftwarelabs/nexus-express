const express = require('express')
const router = express.Router()
const User = require('../models/User')
const {
  check,
  validationResult
} = require('express-validator')
const sgMail = require('@sendgrid/mail')

// forgot password.
// mail will be send to required email
router.post('/forgot', [
  check('email').isEmail().withMessage('Must be the valid email address')
], function (req, res) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectByRegister(req.body, function (err, rows) {
    if (err) throw err
    if (rows.length === 1) {
      sgMail.setApiKey(process.env.SENDGRID_API_KEY)
      const msg = {
        to: req.body.email,
        from: process.env.SUPPORT_EMAIL, // Use the email address or domain you verified above
        subject: 'Password Reset',
        html: '<p>Click <a href="'+process.env.DOMAIN + '/app/resetpassword/'+rows[0].userId+'">here</a> to reset your password.</p>'
        // text: 'To reset your password, please click the link below.\n\n' + process.env.DOMAIN + '/resetpassword/' + rows[0].userId
      }
      // ES6
      sgMail
        .send(msg)
        .then(m => {
          res.status(200).send({
            type: 'success',
            msg: 'Link sent to your email',
            data: m
          })
        })
        .catch(error => {
          // Log friendly error
          res.status(200).send({
            type: 'failure',
            msg: 'Email not sent',
            error: error
          })
        })
    } else {
      res.status(200).send({
        type: 'failure',
        msg: 'You have not registered yet'
      })
    }
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const User = require('../models/User')
const Church = require('../models/Church')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')

// get all church list
router.get('/', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Church.getAllChurchList(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'User not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add church
router.post('/', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Church.addChurch(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update church
router.put('/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Church.updateChurch(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete church
router.post('/delete', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Church.deleteStatusForChurch(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// get group details based on province
router.get('/province', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Church.grpProvinceList((err, list) => {
      if (err) throw err
      res.json(list)
    })
  })
})

module.exports = router

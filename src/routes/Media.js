const express = require('express')
const router = express.Router()
const _ = require('underscore')
const Media = require('../models/Media')
const Event = require('../models/Event')
const User = require('../models/User')
const Label = require('../models/Label')
const Updates = require('../models/Updates')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')
const { Validator } = require('node-input-validator');

// get featured events
fetchEvents = (langId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Event.viewFeaturedEvent(langId, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}

// get published featured media and event data
getFeatureData = (lang, row) => {
  return new Promise(async(resolve, reject) => {
    const rows = []
    for(const l of lang) {
      const mediaArray = []
      _.each(row.filter(x => x.language==l.langId), me => {
        mediaArray.push({
          ID: me.ID,
          title: me.title,
          excerpt: me.excerpt,
          content: me.content,
          parentId: me.parentId,
          showChild: me.showChild,
          bannerImageUrl: me.bannerImageUrl,
          videoUrl: me.videoUrl,
          audioUrl: me.audioUrl,
          pdfUrl: me.pdfUrl,
          status: me.status,
          mediaClassification: me.mediaClassification,
          featured: me.featured,
          quiz: me.quiz,
          order: me.order,
          editedBy: me.editedBy,
          addedBy: me.addedBy,
          createdAt: me.createdAt,
          editedAt: me.editedAt,
          expire: me.expire,
          langType: me.langType,
          type: 'Media'
        })
      })

      const events = await fetchEvents(l.langId);
      for(var me of events.filter(x => x.language==l.langId)) {
        mediaArray.push({
          ID: me.ID,
          title: me.title,
          excerpt: me.excerpt,
          content: me.content,
          parentId: me.parentId,
          showChild: me.showChild,
          bannerImageUrl: me.bannerImageUrl,
          videoUrl: me.videoUrl,
          audioUrl: me.audioUrl,
          pdfUrl: me.pdfUrl,
          status: me.status,
          mediaClassification: me.mediaClassification,
          featured: me.featured,
          quiz: me.quiz,
          order: me.order,
          editedBy: me.editedBy,
          addedBy: me.addedBy,
          createdAt: me.createdAt,
          editedAt: me.editedAt,
          expire: me.expire,
          langType: me.langType,
          type: 'Event'
        })
      }
       
      const mediaData = {
        langId: l.langId,
        data: mediaArray
      }
      rows.push(mediaData)
    }
    resolve(rows)
  })
}


// get published general media data
getMediaData = (lang, row) => {
  return new Promise((resolve, reject) => {
    const rows = []
    _.each(_.groupBy(lang, 'langId'), l => {
      const mediaArray = []
      _.each(row.filter(x => x.language==l[0].langId), me => {
        mediaArray.push({
          ID: me.ID,
          title: me.title,
          excerpt: me.excerpt,
          content: me.content,
          parentId: me.parentId,
          showChild: me.showChild,
          bannerImageUrl: me.bannerImageUrl,
          videoUrl: me.videoUrl,
          audioUrl: me.audioUrl,
          pdfUrl: me.pdfUrl,
          status: me.status,
          mediaClassification: me.mediaClassification,
          featured: me.featured,
          quiz: me.quiz,
          order: me.order,
          editedBy: me.editedBy,
          addedBy: me.addedBy,
          createdAt: me.createdAt,
          editedAt: me.editedAt,
          expire: me.expire,
          langType: me.langType
        })
      })
      const mediaData = {
        langId: l[0].langId,
        data: mediaArray
      }
      rows.push(mediaData)
    })
    resolve(rows)
  })
}


// Media create, update & delete
router.post('/', VerifyToken, function (req, res, next) {
 // if deleteAccess = 1, then we need to delete. 
  // if ID not exist then add. otherwise update
  if(req.body.deleteAccess == 0) {
    let v = new Validator(req.body, {
      editedBy: 'required|numeric',
      excerpt: 'required',
      language: 'required|numeric',
      status: 'required',
      title: 'required',
      category: 'required|numeric',
      mediaClassification: 'required|numeric',
      startDate: 'required|date',
      endDate: 'required|date'
    });
    if(!req.body.ID) {
      v = new Validator(req.body, {
        addedBy: 'required|numeric'
      });
    } else {
      v = new Validator(req.body, {
        ID: 'required|numeric'
      });
    }

    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(401).send('No user found.')
          if ((typeof (req.query.cms) === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            const langCount = await langCheck(req.body.language)
            if ((userCount > 0) && (langCount > 0)) {
              if(!req.body.ID) {
                // add new media
                delete req.body.deleteAccess
                Media.addMedia(req.body, async function (err, data) {
                  if (err) {
                    res.json(err)
                  } else {
                    req.body.ID = data.insertId
                    if (data.insertId !== '') {
                      // add updates in update table
                      var contver = await getVersion(7)
                      var dataVer = parseInt(contver.slice(5, 11)) + 1
                      contver = contver.slice(0, 5) + '' + dataVer
                      Updates.editUpdates(7, contver, function (err, row) {
                        if (err) return res.status(500).send('Server error!')
                        res.json(req.body)
                      })
                    } else {
                      res.json(req.body)
                    }
                  }
                })
              } else {
                // update media
                Media.updateMedia(req.body.ID, req.body, async function (err, row) {
                  if (err) {
                    res.json(err)
                  } else {
                    // add updates in update table
                    var contver = await getVersion(7)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(7, contver, function (err, rrr) {
                      if (err) return res.status(500).send('Server error!')
                      res.json({
                        type: 'success',
                        msg: 'Updated successfully'
                      })
                    })
                  }
                })
              }
            } else {
              res.json({
                type: 'failure',
                msg: 'incorrect data'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    })
  } else {
    let v = new Validator(req.body, {
      ID: 'required|numeric'
    });

    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(401).send('No user found.')
          if ((typeof (req.query.cms) === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            if ((userCount > 0)) {
              // delete media
              Media.deleteMedia(req.body.ID, async function (err, row) {
                if (err) {
                  res.json(err)
                } else {
                  var contver = await getVersion(7)
                  var dataVer = parseInt(contver.slice(5, 11)) + 1
                  contver = contver.slice(0, 5) + '' + dataVer
                  Updates.editUpdates(7, contver, function (err, rrr) {
                    if (err) return res.status(500).send('Server error!')
                    res.json({
                      type: 'success',
                      msg: 'Deleted successfully'
                    })
                  })
                }
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'incorrect data. userId not found'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    })
  }
})

// get all media in a system
router.get('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(401).send('No user found.')
    if (typeof (req.query.cms) === 'string') {
      // if web application, get all media
      Media.viewAllMedia(function (err, row) {
        if (err) return res.status(500).send('Server error!')
        res.json({
          type: 'success',
          msg: 'Got Successfully',
          rows: row
        })
      })
    } else {
      // if mobile application, get general media
      Label.getLanguage(function (err, lang) {
        if (err) return res.status(500).send('Server error!')
        Media.viewAllGeneralPublishedMedia(async (err, row) => {
          if (err) return res.status(500).send('Server error!')
          if(row.length>0){
            const rows = await getMediaData(lang, row)
            res.json({ type: 'success', msg: 'Got successfully', rows})
          } else {
            res.json({type: 'success', msg: 'No data exist', rows})
          }
        })
      })
    }
  })
})

// get all featured media and events in a system
router.get('/featured', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(401).send('No user found.')
    Label.getLanguage(function (err, lang) {
      if (err) return res.status(500).send('Server error!')
      Media.viewFeaturedMedia(async (err, row) => {
        if (err) return res.status(500).send('Server error!')
        if(row.length>0) {
          const rows = await getFeatureData(lang, row)
          res.json({ type: 'success', msg: 'Got successfully', rows})
        } else {
          res.json({type: 'success', msg: 'No data exist', rows})
        }
      })
    })
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const Appfeatures = require('../models/Appfeatures')
const PlanDetails = require('../models/PlanDetails')
const _ = require('underscore')
const {
  check,
  validationResult
} = require('express-validator')
const User = require('../models/User')
var VerifyToken = require('./VerifyToken')
const { Validator } = require('node-input-validator');


const vali = (dataValid, indx) => {
  return new Promise((resolve, reject) => {
    var errData = ''
    var v = new Validator(dataValid.memoryverse[indx], {
      bookChap: 'required',
      verse: 'required',
      createdAt: 'required|datetime'
    }, {
        bookChap: 'The memoryverse['+indx+'].bookChap is required',
        verse: 'The memoryverse['+indx+'].verse is required',
        createdAt: 'The memoryverse['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
    });
    v.check().then((matched) => {
      if (!matched) {
        errData = v.errors
      } else {
        const memData = {
          userId: uId,
          memoryVerseId: mem[0].memoryVerseId,
          bookChap: mem[0].bookChap,
          verse: mem[0].verse,
          createdAt: mem[0].createdAt
        }
        Appfeatures.addMemoryVerse(memData, async (err, result) => {
          if (err) throw err
        })
      }
      resolve(errData)
    })
  })
}

// add/delete memory verse functionality
const cdMemoryVerse = (res, dataValid, uId, memver) => {
  return new Promise(async (resolve, reject) => {
    const rows = {errors: [], failedId: []}
    const promiseArray = []
    function returnres() {
      return new Promise(async(resolve, reject) => {
        for (var mem of memver) {
        // _.each(_.groupBy(memver, 'memoryVerseId'), async mem => {
          const indx = memver.findIndex(x=>x.memoryVerseId==mem.memoryVerseId)
          function doreturn() {
            return new Promise((resolve, reject) => {
              Appfeatures.selectMemoryVerseByUserId(uId, async (err, mData) => {
                  if (err) throw err
                  // checking memory verse id exist or not
                  var checkmemId = mData.filter(x => x.memoryVerseId == mem.memoryVerseId)
                  // if deleteAccess is 0, then check checkmemId's length. if it's length is zero then add prayer group
                  if (mem.deleteAccess == 0 && checkmemId.length == 0) {
                    var v = new Validator(dataValid.memoryverse[indx], {
                      bookChap: 'required',
                      verse: 'required',
                      createdAt: 'required|datetime'
                    }, {
                        bookChap: 'The memoryverse['+indx+'].bookChap is required',
                        verse: 'The memoryverse['+indx+'].verse is required',
                        createdAt: 'The memoryverse['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                    });
                    // async function getData() {
                      function doCall() {
                        return new Promise((resolve, reject) => {
                          let obj
                          v.check().then(async(matched) => {
                            if (!matched) {
                              obj = v.errors
                            } else {
                              const memData = {
                                userId: uId,
                                memoryVerseId: mem.memoryVerseId,
                                bookChap: mem.bookChap,
                                verse: mem.verse,
                                createdAt: mem.createdAt
                              }
                              Appfeatures.addMemoryVerse(memData, (err, result) => {
                                if (err) throw err
                              })
                            }
                            resolve(obj)
                          })
                        })
                      }
                      const result = await doCall();
                      if(result!=undefined) {
                        rows.errors.push(result)
                      }
                  } else if(mem.deleteAccess == 1 && checkmemId.length == 1) {
                    const delData = {
                      userId: uId,
                      memoryVerseId: mem.memoryVerseId
                    }
                    Appfeatures.deleteMemoryVerse(delData, (err, result) => {
                      if (err) throw err
                    })
                  } else {
                    if(mem.deleteAccess != 0 && checkmemId.length != 1){
                      rows.failedId.push(mem.memoryVerseId)
                    }
                  }
                  resolve(rows)
              })
            })
          }
          const returnPro = await doreturn()
          promiseArray.push(returnPro);
        }
        resolve(promiseArray)
      })
    }
    const finalRes = await returnres()
    resolve(finalRes[memver.length-1])
  })
}

// add/delete highlights functionality
const cdHighlights = (req, res, dataValid, uId, high) => {
  return new Promise(async (resolve, reject) => {
    var rows = {errors: [], failedId: []}
    const promiseArray = []
    function returnres() {
      return new Promise(async(resolve, reject) => {
        for (var hi of high) {
        // _.each(_.groupBy(high, 'highlightsId'), hi => {
          const indx = high.findIndex(x=>x.highlightsId==hi.highlightsId)
          function doreturn() {
            return new Promise((resolve, reject) => {
              Appfeatures.selectHighlightsByUserId(uId, async (err, hData) => {
                if (err) throw err
                // checking highlights id exist or not
                var checkhighlightsId = hData.filter(x => x.highlightsId == hi.highlightsId)
                // if deleteAccess is 0, then check checkhighlightsId's length. if it's length is zero then add prayer group
                if (hi.deleteAccess == 0 && checkhighlightsId.length == 0) {
                    var v = new Validator(dataValid.highlights[indx], {
                        bookId: 'required',
                        chapterId: 'required',
                        verse: 'required|array',
                        'verse.*.verseId': 'required',
                        'verse.*.content': 'required',
                        color: 'required',
                        createdAt: 'required|datetime'
                    }, {
                      bookId: 'The highlights['+indx+'].bookId is required.',
                      chapterId: 'The highlights['+indx+'].chapterId is required.',
                      color: 'The highlights['+indx+'].color is required.',
                      createdAt: 'The highlights['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                    });
                    function doCall() {
                      return new Promise((resolve, reject) => {
                        let obj
                        v.check().then(async (matched) => {
                          if (!matched) {
                              // res.status(422).send(v.errors);
                              obj = v.errors;
                          } else {
                            const hiData = {
                              userId: uId,
                              highlightsId: hi.highlightsId,
                              bookId: hi.bookId,
                              chapterId: hi.chapterId,
                              color: hi.color,
                              createdAt: hi.createdAt
                            }
                            hiData.verseId = []
                            _.each(hi.verse, h => {
                              hiData.verseId.push(
                                h.verseId
                              )
                            })
                            function doReturnErr() {
                              return new Promise((resolve, reject) => {
                                PlanDetails.GetBibleVerseById(hiData, (err, rowsHi) => {
                                  if (err) throw err
                                  if (rowsHi.length == hiData.verseId.length) {
                                    const arr = []
                                    _.each(hi.verse, ar => {
                                      arr.push([
                                        hiData.userId,
                                        hiData.highlightsId,
                                        hiData.bookId,
                                        hiData.chapterId,
                                        ar.verseId,
                                        ar.content,
                                        hiData.color,
                                        hiData.createdAt
                                      ])
                                    })
                                    Appfeatures.addHighlights(arr, (err, result) => {
                                      if (err) throw err
                                    })
                                  } else {
                                    obj = {
                                      failed: true,
                                      msg: 'Incorrect bookId/chapterId/verseId'
                                    }
                                  }
                                  resolve(obj)
                                })
                              })
                            }
                            const results = await doReturnErr()
                            obj = results
                          }
                          resolve(obj);
                        })
                      })
                    }
                    const result = await doCall()
                    if(result!=undefined) {
                      if(result.failed==true) {
                        rows.failedId.push(result.msg)
                      } else {
                        rows.errors.push(result)
                      }
                    }
                } else if (hi.deleteAccess == 1 && checkhighlightsId.length == 1) {
                  const delData = {
                    userId: uId,
                    highlightsId: hi.highlightsId
                  }
                  Appfeatures.deleteHighlights(delData, (err, result) => {
                    if (err) throw err
                  })
                } else {
                  if(hi.deleteAccess != 0 && checkhighlightsId.length != 1){
                    rows.failedId.push(hi.highlightsId)
                  }
                }
                resolve(rows)
              })
            })
          }
          const returnPro = await doreturn()
          promiseArray.push(returnPro);
        }
        resolve(promiseArray)
      })
    }
    const finalRes = await returnres()
    resolve(finalRes[high.length-1])
  })
}

// add/update/delete notes functionality
const cudNotes = (req, res, dataValid, uId, note) => {
  return new Promise(async (resolve, reject) => {
    var rows = {errors: [], failedId: []}
    const promiseArray = []
    function returnres() {
      return new Promise(async(resolve, reject) => {
        for (var n of note) {
        // _.each(_.groupBy(note, 'notesId'), n => {
          const indx = note.findIndex(x=>x.notesId==n.notesId)
          function doreturn() {
            return new Promise((resolve, reject) => {
              Appfeatures.selectNotesByUserId(uId, async (err, nData) => {
                if (err) throw err
                // checking notes id exist or not
                var checknotesId = nData.filter(x => x.notesId == n.notesId)
                // if deleteAccess is 0, then check checknotesId's length. if it's length is zero then add prayer group
                if (n.deleteAccess == 0) {
                    var v = new Validator(dataValid.notes[indx], {
                        content: 'required',
                        description: 'required',
                        createdAt: 'required|date'
                    }, {
                      content: 'The notes['+indx+'].content is required',
                      description: 'The notes['+indx+'].description is required',
                      createdAt: 'The notes['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD).'
                    });
                    function doCall() {
                      return new Promise((resolve, reject) => {
                          let obj
                          v.check().then((matched) => {
                            if (!matched) {
                              obj = v.errors
                              // res.status(422).send(v.errors);
                            } else {
                              const notData = {
                                userId: uId,
                                notesId: n.notesId,
                                content: n.content,
                                description: n.description,
                                createdAt: n.createdAt
                              }
                              Appfeatures.GetNotesByDate(notData, (err, rows) => {
                                if (err) throw err
                                if (rows.length === 0) {
                                  Appfeatures.addNotes(notData, async (err, result) => {
                                    if (err) throw err
                                  })
                                } else {
                                  // if notes in same date, then update it
                                  Appfeatures.updateNotes(notData, async (err, result) => {
                                    if (err) throw err
                                  })
                                }
                              })
                            }
                            resolve(obj)
                          })
                      })
                    }
                    const result = await doCall();
                    if(result!=undefined) {
                      rows.errors.push(result)
                    }
                } else if (n.deleteAccess == 1 && checknotesId.length == 1) {
                    const delData = {
                      userId: uId,
                      notesId: n.notesId
                    }
                    Appfeatures.deleteNotes(delData, (err, result) => {
                      if (err) throw err
                    })
                } else {
                  rows.failedId.push(n.notesId)
                }
                resolve(rows)
              })
            })
          }
          const returnPro = await doreturn()
          promiseArray.push(returnPro)
        }
        resolve(promiseArray)
      })
    }
    const finalRes = await returnres()
    resolve(finalRes[note.length-1])
  })
}

// add/delete reading history functionality
const cdReadingHistory = (req, res, dataValid, uId, readhis) => {
  return new Promise(async (resolve, reject) => {
      var rows = {errors: [], failedId: []}
      const promiseArray = []
      function returnres() {
          return new Promise(async(resolve, reject) => {
            for (var read of readhis) {
                  const indx = readhis.findIndex(x=>x.readinghisId==read.readinghisId)
                  function doreturn() {
                      return new Promise((resolve, reject) => {
                          Appfeatures.selectReadingHistoryByUserId(uId, async (err, readData) => {
                              if (err) throw err
                              // checking readinghis id exist or not
                              var checkreadingsId = readData.filter(x => x.readinghisId == read.readinghisId)
                              // if deleteAccess is 0, then check checkreadingsId's length. if it's length is zero then add
                              if (read.deleteAccess == 0 && checkreadingsId.length == 0) {
                                  v = new Validator(dataValid.readinghis[indx], {
                                      bookId: 'required',
                                      chapterId: 'required',
                                      verseId: 'required',
                                      createdAt: 'required|datetime'
                                  }, {
                                      bookId: 'The readinghis['+indx+'].bookId is required.',
                                      chapterId: 'The readinghis['+indx+'].chapterId is required.',
                                      verseId: 'The readinghis['+indx+'].verseId is required.',
                                      createdAt: 'The readinghis['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                                  });
                                  function doCall() {
                                      return new Promise((resolve, reject) => {
                                          let obj
                                          v.check().then(async (matched) => {
                                              if (!matched) {
                                                obj = v.errors;
                                                  // res.status(422).send(v.errors);
                                              } else {
                                                  const rhData = {
                                                      userId: uId,
                                                      readinghisId: read.readinghisId,
                                                      bookId: read.bookId,
                                                      chapterId: read.chapterId,
                                                      verseId: read.verseId,
                                                      createdAt: read.createdAt
                                                  }
                                                  function doReturnErr() {
                                                      return new Promise((resolve, reject) => {
                                                          PlanDetails.GetBibleVerseById(rhData, (err, rows) => {
                                                              if (err) throw err
                                                              if (rows.length === 1) {
                                                                  Appfeatures.addReadingHistory(rhData, async (err, result) => {
                                                                      if (err) throw err
                                                                  })
                                                              } else {
                                                                  obj = {
                                                                      failed: true,
                                                                      msg: 'Incorrect bookId/chapterId/verseId'
                                                                  }
                                                              
                                                              }
                                                              resolve(obj)
                                                          })
                                                      })
                                                  }
                                                  const results = await doReturnErr()
                                                  obj = results
                                              }
                                              resolve(obj);
                                          })
                                      })
                                  }
                                  const result = await doCall()
                                  if(result!=undefined) {
                                      if(result.failed==true) {
                                          rows.failedId.push(result.msg)
                                      } else {
                                          rows.errors.push(result)
                                      }
                                  }
                              } else if (read.deleteAccess == 1 && checkreadingsId.length == 1) {
                                  const delData = {
                                      userId: uId,
                                      readinghisId: read.readinghisId
                                  }
                                  Appfeatures.deleteReadingHistory(delData, (err, result) => {
                                      if (err) throw err
                                  })
                              } else {
                                  if(read.deleteAccess != 0 && checkreadingsId.length != 1){
                                     rows.failedId.push(read.readinghisId)
                                  }
                              }
                              resolve(rows)
                          })
                      })
                  }
                  const returnPro = await doreturn()
                  promiseArray.push(returnPro);
            }
            resolve(promiseArray)
          })
      }
      const finalRes = await returnres()
      resolve(finalRes[readhis.length-1])
  })
}

// fetch all memory verse by userId
const getMemversedata = (req, res) => {
  return new Promise((resolve, reject) => {
    let data = ''
    Appfeatures.selectMemoryVerseByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getMemoryVerseByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount: row.length,
            pageNumber: page,
            result: results
          }
          data = JSON.parse(JSON.stringify(jsonResult))
          resolve(data)
        })
      } else {
        resolve([])
      }
    })
  })
}

// fetch all highlights by userId
const getHighlightdata = (req, res) => {
  return new Promise((resolve, reject) => {
    Appfeatures.selectHighlightsByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getHighlightsByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount: row.length,
            pageNumber: page,
            result: results
          }
          resolve(JSON.parse(JSON.stringify(jsonResult)))
        })
      } else {
        resolve(row)
      }
    })
  })
}

// fetch all notes by userId
const getNotesdata = (req, res) => {
  return new Promise((resolve, reject) => {
    Appfeatures.selectNotesByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getNotesByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount: row.length,
            pageNumber: page,
            result: results
          }
          resolve(JSON.parse(JSON.stringify(jsonResult)))
        })
      } else {
        resolve(row)
      }
    })
  })
}

// fetch all reading history by userId
const getReadingHisdata = (req, res) => {
  return new Promise((resolve, reject) => {
    Appfeatures.selectReadingHistoryByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getReadingHistoryByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount: row.length,
            pageNumber: page,
            result: results
          }
          resolve(JSON.parse(JSON.stringify(jsonResult)))
        })
      } else {
        resolve(row)
      }
    })
  })
}

// add/delete memory verse
router.post('/memoryverse', VerifyToken, function (req, res, next) {
  var v = new Validator(req.body, {
    'userId': 'required|integer',
    'memoryverse': 'required|array',
    'memoryverse.*.memoryVerseId': 'required',
    'memoryverse.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
    if (!matched) {
        res.status(422).send(v.errors);
    } else {
      User.selectById(req.userId, function (err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.')
        if (!user) return res.status(404).send('No user found.')
        // check user existence
        User.selectById(req.body.userId, async (err, results) => {
          if (err) throw err
          if (results.length > 0) {
            const resp = await cdMemoryVerse(res, req.body, req.body.userId, req.body.memoryverse)
            // add updates in update table
            if(resp.errors.length == 0 && resp.failedId.length == 0) {
              res.json({
                type: 'success',
                msg: 'Successfully synced'
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'Not success',
                rows: resp
              })
            }
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No User exist with this user id'
            })
          }
        })
      })
    }
  })
})

// add highlights
router.post('/highlights', VerifyToken, function (req, res, next) {
  var v = new Validator(req.body, {
    'userId': 'required|integer',
    'highlights': 'required|array',
    'highlights.*.highlightsId': 'required',
    'highlights.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
    if (!matched) {
        res.status(422).send(v.errors);
    } else {
      User.selectById(req.userId, function (err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.')
        if (!user) return res.status(404).send('No user found.')
        // check user existence
        User.selectById(req.body.userId, async (err, results) => {
          if (err) throw err
          if (results.length > 0) {
            const resp = await cdHighlights(req, res, req.body, req.body.userId, req.body.highlights)
            if(resp.errors.length == 0 && resp.failedId.length == 0) {
              res.json({
                type: 'success',
                msg: 'Successfully synced'
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'Not success',
                rows: resp
              })
            }
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No User exist with this user id'
            })
          }
        })
      })
    }
  })
})

// add notes
router.post('/notes', VerifyToken, function (req, res, next) {
  var v = new Validator(req.body, {
    'userId': 'required|integer',
    'notes': 'required|array',
    'notes.*.notesId': 'required',
    'notes.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
    if (!matched) {
        res.status(422).send(v.errors);
    } else {
      User.selectById(req.userId, function (err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.')
        if (!user) return res.status(404).send('No user found.')
        // check user existence
        User.selectById(req.body.userId, async (err, results) => {
          if (err) throw err
          if (results.length > 0) {
            const resp = await cudNotes(req, res, req.body, req.body.userId, req.body.notes)
            if(resp.errors.length == 0 && resp.failedId.length == 0) {
              res.json({
                type: 'success',
                msg: 'Successfully synced'
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'Not success',
                rows: resp
              })
            }
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No User exist with this user id'
            })
          }
        })
      })
    }
  })
})

// add readinghistory
router.post('/readinghistory', VerifyToken, function (req, res, next) {
  var v = new Validator(req.body, {
    'userId': 'required|integer',
    'readinghis': 'required|array',
    'readinghis.*.readinghisId': 'required',
    'readinghis.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
    if (!matched) {
        res.status(422).send(v.errors);
    } else {
      User.selectById(req.userId, function (err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.')
        if (!user) return res.status(404).send('No user found.')
        // check user existence
        User.selectById(req.body.userId, async (err, results) => {
          if (err) throw err
          if (results.length > 0) {
            const resp = await cdReadingHistory(req, res, req.body, req.body.userId, req.body.readinghis)
            if(resp.errors.length == 0 && resp.failedId.length == 0) {
              res.json({
                type: 'success',
                msg: 'Successfully synced'
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'Not success',
                rows: resp
              })
            }
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No User exist with this user id'
            })
          }
        })
      })
    }
  })
})

// get memory verse by userId with pagination(10 by 10)
router.get('/', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('page').isInt({
    min: 1
  }).withMessage('Must be only greater than zero')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const memory = await getMemversedata(req, res);
    const highlights = await getHighlightdata(req, res);
    const notes = await getNotesdata(req, res);
    const readinghis = await getReadingHisdata(req, res);
    res.json({
      type: "success",
      msg: "Received data successfully",
      memoryVerse: memory,
      highlights: highlights,
      notes: notes,
      readingHistory: readinghis
    })
  })
})

// get memory verse by userId with pagination(10 by 10)
router.get('/memoryverse', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('page').isInt({
    min: 1
  }).withMessage('Must be only greater than zero')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Appfeatures.selectMemoryVerseByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const countPg = (row.length / 10) + 1;
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getMemoryVerseByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount:  Math.trunc(countPg),
            pageNumber: page,
            rows: results
          }
          res.json({
            type: 'success',
            rows: JSON.parse(JSON.stringify(jsonResult))
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No Memory Verse exist with this user id'
        })
      }
    })
  })
})

// get highlights by userId with pagination(10 by 10)
router.get('/highlights', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('page').isInt({
    min: 1
  }).withMessage('Must be only greater than zero')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Appfeatures.selectHighlightsByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const countPg = (row.length / 10) + 1;
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getHighlightsByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount:  Math.trunc(countPg),
            pageNumber: page,
            rows: results
          }
          res.json({
            type: 'success',
            rows: JSON.parse(JSON.stringify(jsonResult))
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No Highlights exist with this user id'
        })
      }
    })
  })
})

// get notes by userId with pagination(10 by 10)
router.get('/notes', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('page').isInt({
    min: 1
  }).withMessage('Must be only greater than zero')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Appfeatures.selectNotesByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const countPg = (row.length / 10) + 1;
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getNotesByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount:  Math.trunc(countPg),
            pageNumber: page,
            rows: results
          }
          res.json({
            type: 'success',
            rows: JSON.parse(JSON.stringify(jsonResult))
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No Notes exist with this user id'
        })
      }
    })
  })
})

// get reading history by userId with pagination(10 by 10)
router.get('/readinghistory', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('page').isInt({
    min: 1
  }).withMessage('Must be only greater than zero')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Appfeatures.selectReadingHistoryByUserId(req.query.userId, (err, row) => {
      if (err) throw err
      if (row.length > 0) {
        const countPg = (row.length / 10) + 1;
        const limit = 10
        const page = req.query.page
        const offset = (page - 1) * limit
        Appfeatures.getReadingHistoryByUserId(limit, offset, req.query.userId, function (error, results) {
          if (error) throw error
          var jsonResult = {
            pageCount:  Math.trunc(countPg),
            pageNumber: page,
            rows: results
          }
          res.json({
            type: 'success',
            rows: JSON.parse(JSON.stringify(jsonResult))
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No Reading History exist with this user id'
        })
      }
    })
  })
})

module.exports = router

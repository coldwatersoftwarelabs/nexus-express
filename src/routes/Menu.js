const express = require('express')
const router = express.Router()
const Menu = require('../models/Menu')
const Updates = require('../models/Updates')
const User = require('../models/User')
const bodyParser = require('body-parser')
const Label = require('../models/Label')
const _ = require('underscore')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')


// get only english sidebar menu
engSidebarMenuFunc = (lId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Menu.viewMainSideMenuByEngLangId(lId, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}


// get other sidebar menu
allotherSidebarMenuFunc = (lId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Menu.viewMainSideMenuByOtherLangId(lId, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}


// get only english footer menu
engFooterMenuFunc = (lId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Menu.viewMainFooterMenuByEngLangId(lId, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}


// get other footer menu
allotherFooterMenuFunc = (lId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Menu.viewMainFooterMenuByOtherLangId(lId, (err, en) => {
      if (err) throw err
      arrData = en
      resolve(arrData)
    })
  })
}


getMenuData = (row) => {
  return new Promise(async (resolve, reject) => {
    const sideMenuArray = []
    const promiseArray = []
    _.each(_.groupBy(row, 'langId'), label => {

      // fetch sidebar menu
      async function getSidebarData() {
        if(label[0].langId===1) {
          // call english sidebar menu func
          return await engSidebarMenuFunc(label[0].langId);
        } else {
          // call other sidebar menu func
          return await allotherSidebarMenuFunc(label[0].langId);
        }
      }

      // fetch footer menu
      async function getFooterData() {
        if(label[0].langId===1) {
          // call english footer menu func
          return await engFooterMenuFunc(label[0].langId);
        } else {
          // call other footer menu func
          return await allotherFooterMenuFunc(label[0].langId);
        }
      }

      const sidebarArr = []
      const socialIcon = []
      promiseArray.push(
        // calling func getSidebarData()
        getSidebarData().then(result=> {
          const parentSMenuData = result.filter(x=>x.menuType===1)
          _.each(parentSMenuData, e => {
            const engMain = {
              ID: e.ID,
              menuType: e.menuType,
              subMenu: e.subMenu,
              parentMenuId: e.parentMenuId,
              menuOrder: e.menuOrder,
              name: e.name,
              engId: e.engId,
              labelDesc: e.labelDesc,
              iconUrl: e.iconUrl,
              submenuCount: e.submenuCount,
              submenuData: result.filter(x=>x.parentMenuId===e.ID)
            }
            sidebarArr.push(engMain)
          })
          const socialMenuData = result.filter(x=>x.menuType===3)
          _.each(socialMenuData, e => {
            socialIcon.push({
              ID: e.ID,
              menuType: e.menuType,
              subMenu: e.subMenu,
              parentMenuId: e.parentMenuId,
              menuOrder: e.menuOrder,
              name: e.name,
              engId: e.engId,
              labelDesc: e.labelDesc,
              iconUrl: e.iconUrl,
              iconLink: e.iconLink
            })
          })
        }),

        // calling func getFooterData()
        getFooterData().then(fresult => {
          const parentFMenuData = fresult.filter(x=>x.parentMenu===1)
          const footerArr = []
          _.each(parentFMenuData, e => {
            const engMain = {
              ID: e.ID,
              parentMenu: e.parentMenu,
              subMenu: e.subMenu,
              parentMenuId: e.parentMenuId,
              menuOrder: e.menuOrder,
              name: e.name,
              engId: e.engId,
              labelDesc: e.labelDesc,
              iconUrl: e.iconUrl,
              submenuCount: e.submenuCount,
              submenuData: fresult.filter(x=>x.parentMenuId===e.ID)
            }
            footerArr.push(engMain)
          })
          
          const labelData = {
            langId: label[0].langId,
            langType: label[0].langType,
            OriginalLangtext: label[0].OriginalLangtext,
            status: label[0].status,
            sidemenu: sidebarArr,
            footermenu: footerArr,
            socialMenu: socialIcon
          }
          sideMenuArray.push(labelData)
        })
      )
    })
    Promise.all(promiseArray).then(() => {
      resolve(_.sortBy(Array.prototype.concat.apply([], sideMenuArray), 'langId'))
    })
  })
}

// create sidebar menu
router.post('/sidemenu', VerifyToken, [
  check('name').notEmpty().withMessage('Should not be empty'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('labelImage').notEmpty().withMessage('Should not be empty'),
  check('menuOrder').isNumeric().withMessage('Must be only numbers'),
  check('menuType').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      // check user existence
      const userCount = await userCheck(req.query.userId)
      if ((userCount > 0)) {
        // add english label
        Label.addEnglishLabel(req.body, function (err, data) {
          if (err) {
            res.json(err)
          } else {
            const mapPayload = {
              engId: data.insertId,
              data: req.body
            }
            // add sidebar menu
            Menu.addSideMenu(mapPayload, async (err, mapResult) => {
              if (err) throw err
              req.body.ID = mapResult.insertId
              if (mapResult.insertId !== '') {
                // add updates in update table
                var contver = await getVersion(5)
                var dataVer = parseInt(contver.slice(5, 11)) + 1
                contver = contver.slice(0, 5) + '' + dataVer
                Updates.editUpdates(5, contver, function (err, row) {
                  if (err) return res.status(500).send('Server error!')
                  Label.getLanguageExcerptEng((err, rows) => {
                    if (err) throw err
                    if (rows.length > 0) {
                      const arr = []
                      _.each(rows, r => {
                        arr.push([
                          r.langId,
                          req.body.name,
                          data.insertId
                        ])
                      })
                      // add all label excerpt english
                      Label.addAllLabel(arr, async function (err, co) {
                        if (err) {
                          res.json(err)
                        } else {
                          // add updates in update table
                          var contver = await getVersion(7)
                          var dataVer = parseInt(contver.slice(5, 11)) + 1
                          contver = contver.slice(0, 5) + '' + dataVer
                          Updates.editUpdates(7, contver, function (err, rrr) {
                            if (err) return res.status(500).send('Server error!')
                            if (rrr.length > 0) {
                              res.json(req.body)
                            } else {
                              res.json(req.body)
                            }
                          })
                        }
                      })
                    }
                  })
                })
              } else {
                res.json(req.body)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// create footer menu
router.post('/footermenu', VerifyToken, [
  check('name').notEmpty().withMessage('Should not be empty'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('labelImage').notEmpty().withMessage('Should not be empty'),
  check('menuOrder').isNumeric().withMessage('Must be only numbers'),
  check('parentMenu').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      // check user existence
      const userCount = await userCheck(req.query.userId)
      if ((userCount > 0)) {
        // add english label
        Label.addEnglishLabel(req.body, function (err, data) {
          if (err) {
            res.json(err)
          } else {
            const mapPayload = {
              engId: data.insertId,
              data: req.body
            }
            // add footer menu
            Menu.addFooterMenu(mapPayload, async function (err, mapResult) {
              if (err) throw err
              req.body.ID = mapResult.insertId
              if (mapResult.insertId !== '') {
                // add updates in update table
                var contver = await getVersion(6)
                var dataVer = parseInt(contver.slice(5, 11)) + 1
                contver = contver.slice(0, 5) + '' + dataVer
                Updates.editUpdates(6, contver, function (err, row) {
                  if (err) return res.status(500).send('Server error!')
                  Label.getLanguageExcerptEng((err, rows) => {
                    if (err) throw err
                    if (rows.length > 0) {
                      const arr = []
                      _.each(rows, r => {
                        arr.push([
                          r.langId,
                          req.body.name,
                          data.insertId
                        ])
                      })
                      // add all label excerpt english
                      Label.addAllLabel(arr, async function (err, co) {
                        if (err) {
                          res.json(err)
                        } else {
                          // add updates in update table
                          var contver = await getVersion(7)
                          var dataVer = parseInt(contver.slice(5, 11)) + 1
                          contver = contver.slice(0, 5) + '' + dataVer
                          Updates.editUpdates(7, contver, function (err, rrr) {
                            if (err) return res.status(500).send('Server error!')
                            if (rrr.length > 0) {
                              res.json(req.body)
                            } else {
                              res.json(req.body)
                            }
                          })
                        }
                      })
                    }
                  })
                })
              } else {
                res.json(req.body)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})


// get menu
router.get('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms) === 'string') {
      // if web application
      Menu.getSideMenu(function (err, row) {
        if (err) return res.status(500).send('Server error!')
        Menu.getFooterMenu(function (err, rows) {
          if (err) return res.status(500).send('Server error!')
          res.json({
            type: 'success', 
            msg: 'Got successfully',
            sidemenu: row,
            footermenu: rows
          })
        })
      })
    } else {
      //if mobile application
      Label.getLanguage(async (err, lang) => {
        if (err) return res.status(500).send('Server error!')
        if(lang.length>0) {
          const rows = await getMenuData(lang)
          res.json({ type: 'success', msg: 'Got successfully', rows})
        } else {
          res.json({
            type: 'success',
            msg: 'No data exist',
            rows
          })
        }
      })
    }
  })
})

// update sidebar menu
router.put('/sidemenu/:id', VerifyToken, jsonParser, [
  check('name').notEmpty().withMessage('Should not be empty'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('labelImage').notEmpty().withMessage('Should not be empty'),
  check('menuOrder').isNumeric().withMessage('Must be only numbers'),
  check('menuType').isNumeric().withMessage('Must be only numbers'),
  check('engId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      const userCount = await userCheck(req.query.userId)
      const engCount = await engCheck(req.body.engId)
      if ((userCount > 0) && (engCount > 0)) {
        Label.updateEnglishLabel(req.body, function (err, row) {
          if (err) {
            res.json(err)
          } else {
            Menu.updateSideMenu(req.params.id, req.body, async function (err, rowd) {
              if (err) throw err
              var contver = await getVersion(5)
              var dataVer = parseInt(contver.slice(5, 11)) + 1
              contver = contver.slice(0, 5) + '' + dataVer
              Updates.editUpdates(5, contver, function (err, rrr) {
                if (err) return res.status(500).send('Server error!')
                if (rrr.length > 0) {
                  res.json(req.body)
                } else {
                  res.json(req.body)
                }
              })
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// update footer menu
router.put('/footermenu/:id', VerifyToken, jsonParser, [
  check('name').notEmpty().withMessage('Should not be empty'),
  check('labelDesc').notEmpty().withMessage('Should not be empty'),
  check('labelImage').notEmpty().withMessage('Should not be empty'),
  check('menuOrder').isNumeric().withMessage('Must be only numbers'),
  check('parentMenu').isNumeric().withMessage('Must be only numbers'),
  check('engId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      const userCount = await userCheck(req.query.userId)
      const engCount = await engCheck(req.body.engId)
      if ((userCount > 0) && (engCount > 0)) {
        Label.updateEnglishLabel(req.body, function (err, row) {
          if (err) {
            res.json(err)
          } else {
            Menu.updateFooterMenu(req.params.id, req.body, async function (err, rowd) {
              if (err) throw err
              var contver = await getVersion(6)
              var dataVer = parseInt(contver.slice(5, 11)) + 1
              contver = contver.slice(0, 5) + '' + dataVer
              Updates.editUpdates(6, contver, function (err, rrr) {
                if (err) return res.status(500).send('Server error!')
                if (rrr.length > 0) {
                  res.json()
                } else {
                  res.json()
                }
              })
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// delete sidebar menu
router.put('/deletesidemenu/:id', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  },
  id: {
    in: ['path'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      const userCount = await userCheck(req.query.userId)
      const engCount = await engCheck(req.params.id)
      if ((userCount > 0) && (engCount > 0)) {
        Label.engLabelStatusForDelete(req.params, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            var contver = await getVersion(5)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(5, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

// delete footer menu
router.put('/deletefootermenu/:id', VerifyToken, jsonParser, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  },
  id: {
    in: ['path'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      const userCount = await userCheck(req.query.userId)
      const engCount = await engCheck(req.params.id)
      if ((userCount > 0) && (engCount > 0)) {
        Label.engLabelStatusForDelete(req.params, async function (err, row) {
          if (err) {
            res.json(err)
          } else {
            var contver = await getVersion(6)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(6, contver, function (err, rrr) {
              if (err) return res.status(500).send('Server error!')
              if (rrr.length > 0) {
                res.json(row)
              } else {
                res.json(row)
              }
            })
          }
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Incorrect data'
        })
      }
    } else {
      res.status(403).end()
    }
  })
})

module.exports = router

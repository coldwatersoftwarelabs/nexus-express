const express = require('express')
const router = express.Router()
var Logs = require('logger').createLogger('development.log') // logs to a file
const User = require('../models/User')
const bcrypt = require('bcryptjs')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
var jwt = require('jsonwebtoken')
var config = require('../../config')
var VerifyToken = require('./VerifyToken')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')


// get downloaded bible count
getDownloadedBible = (userId) => {
  return new Promise((resolve, reject) => {
    let bibleCount = ''
    User.getDownloadedBibleCount(userId, (err, ble) => {
      if (err) throw err
      bibleCount = ble
      resolve(bibleCount)
    })
  })
}

// get downloaded paln count
getDownloadedPlan = (userId) => {
  return new Promise((resolve, reject) => {
    let planCount = ''
    User.getDownloadedPlanCount(userId, (err, pl) => {
      if (err) throw err
      planCount = pl
      resolve(planCount)
    })
  })
}

// to register
router.post('/register', [
  check('email').optional(),
  check('password').isLength({
    min: 8
  }).withMessage('Password must be atleast 8 characters'),
  check('firstname').notEmpty().withMessage('This field is required'),
  check('lastname').notEmpty().withMessage('This field is required'),
  check('phone').optional()
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check already registered
  User.selectByRegister(req.body, function (err, rows) {
    if (err) throw err
    if (rows.length === 0) {
      User.createUser(req.body, function (err, row) {
        if (err) throw err
        if(row.insertId){
          req.body.email = req.body.email=='' ? row.insertId:req.body.email;
          req.body.phone = req.body.phone=='' ? row.insertId:req.body.phone;
          req.body.userId = row.insertId
          User.updateProfile(req.body, (err, tok) => {
            if (err) throw err
            const expiresIn = 24 * 60 * 60
            // const accessToken = jwt.sign({ id: row.insertId }, {
            //   expiresIn: expiresIn
            // })
            res.status(200).send({
              userId: row.insertId,
              expires_in: expiresIn,
              type: 'success',
              msg: 'Registered Success'
            })
          })
        }
      })
    } else {
      res.status(200).send({
        type: 'failure',
        msg: 'Email id/Phone no already exists'
      })
    }
  })
})

// to login
router.post('/auth', [
  check('emailOrPhone').isLength({
    min: 10
  }).withMessage('Must be the valid email address or phone'),
  check('password').isLength({
    min: 8
  }).withMessage('Password must be atleast 8 characters')
  // check('deviceToken').notEmpty().withMessage('Device Token should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check email/phone or password matches
  User.selectByEmail(req.body, async (err, rows) => {
    if (err) throw err
    if (!rows) {
      return res.status(200).send({
        type: 'failure',
        msg: 'User not found'
      })
    }
    if (rows.length > 0 && rows[0].deviceToken === req.body.deviceToken) {
      const result = bcrypt.compareSync(req.body.password, rows[0].password)
      if (!result) {
        return res.status(401).send({
          type: 'failure',
          msg: 'Email/Password not valid'
        })
      }
      // const expiresIn = 86400
      const accessToken = jwt.sign({
        id: rows[0].userId
      }, config.secret)
      if(rows[0].approveStatus === 1 && rows[0].disable === 0) {
        if (rows[0].apiSecTok === null) {
          // if api security token not exist (null) in db, update newly created token
          User.updateAPIToken(accessToken, rows[0].userId, async (err, tok) => {
            if (err) throw err
            if (tok.affectedRows === 1) {
              res.status(200).send({
                type: 'success',
                msg: 'Successfully logged in !',
                auth: true,
                token: accessToken,
                user: rows,
                // bibledownload: await getDownloadedBible(rows[0].userId),
                // plandownload: await getDownloadedPlan(rows[0].userId)
                bibledownload: [],
                plandownload: []
              })
            }
          })
        } else {
          // if api security token exist in db, then you have logged in already
          res.status(200).send({
            type: 'success',
            msg: 'You have already logged in',
            auth: false,
            token: accessToken,
            user: rows,
            bibledownload: [],
            plandownload: []
            // bibledownload: await getDownloadedBible(rows[0].userId),
            // plandownload: await getDownloadedPlan(rows[0].userId)
          })
        }
      } else {
        res.status(200).send({
          type: 'failure',
          auth: false,
          msg: 'User not approved yet!'
        })
      }
    } else if (rows.length > 0 && rows[0].deviceToken !== req.body.deviceToken) {
      // if device token changed, then update it
      User.updateDeviceToken(req.body, async (err, row) => {
        if (err) throw err
        const result = bcrypt.compareSync(req.body.password, rows[0].password)
        if (!result) {
          return res.status(200).send({
            type: 'failure',
            msg: 'Email/Password not valid'
          })
        }
        const accessToken = jwt.sign({
          id: rows[0].userId
        }, config.secret)
        if(rows[0].approveStatus === 1 && rows[0].disable === 0) {
          if (rows[0].apiSecTok === null) {
            // if api security token not exist (null) in db, update newly created token
            User.updateAPIToken(accessToken, rows[0].userId, async (err, tok) => {
              if (err) throw err
              if (tok.affectedRows === 1) {
                res.status(200).send({
                  type: 'success',
                  msg: 'Successfully logged in !',
                  auth: true,
                  token: accessToken,
                  user: rows,
                  // bibledownload: await getDownloadedBible(rows[0].userId),
                  // plandownload: await getDownloadedPlan(rows[0].userId)
                  bibledownload: [],
                  plandownload: []
                })
              }
            })
          } else {
            res.status(200).send({
              type: 'success',
              msg: 'You have already logged in',
              auth: false,
              token: accessToken,
              user: rows,
              // bibledownload: await getDownloadedBible(rows[0].userId),
              // plandownload: await getDownloadedPlan(rows[0].userId)
              bibledownload: [],
              plandownload: []
            })
          }
        } else {
          res.status(200).send({
            type: 'failure',
            auth: false,
            msg: 'User not approved yet!'
          })
        }
      })
    } else {
      res.status(200).send({
        type: 'failure',
        msg: 'Email/Password not valid'
      })
    }
  })
})

// update profile
router.put('/edit-profile', VerifyToken, [
  check('userId').isNumeric().withMessage('Accept only numbers'),
  check('firstname').isAlpha().withMessage('Must be only alphabetical chars'),
  check('lastname').isAlpha().withMessage('Must be only alphabetical chars'),
  check('email').isEmail().withMessage('Must be the valid email address'),
  check('phone').isNumeric().withMessage('Must be only numbers').isLength({
    min: 10,
    max: 10
  }).withMessage('Phone number must be 10 characters'),
  check('gender').isAlpha().withMessage('Must be only alphabetical chars'),
  check('dob').isISO8601().toDate().withMessage('Must be in yyyy-mm-dd format'),
  check('published').isBoolean().withMessage('Accept only boolean value 0 or 1')
], jsonParser, function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByRegister(req.body, function (err, rows) {
      if (err) throw err
      if (rows.length === 0) {
        User.updateProfile(req.body, function (err, row) {
          if (err) throw err
          res.json({
            type: 'success',
            msg: 'Updated successfully'
          })
        })
      } else if (rows.length === 1) {
        if (rows[0].userId === parseInt(req.body.userId)) {
          if (err) throw err
          User.updateProfile(req.body, function (err, row) {
            if (err) throw err
            res.json({
              type: 'success',
              msg: 'Updated successfully'
            })
          })
        } else {
          res.status(401).send('Updated failed. This email id is already taken')
        }
      }
    })
  })
})

// update profile picture
router.put('/change-profilepic', VerifyToken, [
  check('userId').isNumeric().withMessage('Accept only numbers')
], jsonParser, function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByRegister(req.body, function (err, rows) {
      if (err) throw err
      if (rows.length === 0) {
        User.updateProfilePic(req.body, function (err, row) {
          if (err) throw err
          res.json({
            type: 'success',
            msg: 'Updated successfully'
          })
        })
      } else if (rows.length === 1) {
        if (rows[0].userId === parseInt(req.body.userId)) {
          if (err) throw err
          User.updateProfilePic(req.body, function (err, row) {
            if (err) throw err
            res.json({
              type: 'success',
              msg: 'Updated successfully'
            })
          })
        } else {
          res.status(401).send('Updated failed. This email id is already taken')
        }
      }
    })
  })
})

// change password
router.post('/edit-password', VerifyToken, [
  check('userId').isNumeric().withMessage('Accept only numbers'),
  check('emailOrPhone').isEmail().withMessage('Must be the valid email address'),
  check('oldpassword').isLength({
    min: 8
  }).withMessage('oldpassword must be atleast 8 characters'),
  check('newpassword').isLength({
    min: 8
  }).withMessage('newpassword must be atleast 8 characters')
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.body, function (err, rows) {
      if (err) throw err
      if (rows.length === 1) {
        const result = bcrypt.compareSync(req.body.oldpassword, rows[0].password)
        if (result) {
          User.editPassword(req.body, function (row) {
            res.status(200).send({
              type: 'success',
              msg: 'password changed successfully'
            })
          })
        } else {
          res.json({
            type: 'failure',
            msg: 'Check your old password'
          })
        }
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'Not found'
        })
      }
    })
  })
})

// reset password
router.post('/reset-password', [
  check('userId').isNumeric().withMessage('Accept only numbers'),
  check('newpassword').isLength({
    min: 8
  }).withMessage('newpassword must be atleast 8 characters')
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.query.userId, function (err, rows) {
    if (err) return err
    if (rows.length) {
      User.editPassword(req.body, function (err, row) {
        if (err) return err
        res.status(200).send({
          type: 'success',
          msg: 'password reset successfully'
        })
      })
    } else {
      res.status(401).send({
        type: 'failure',
        msg: 'No User information'
      })
    }
  })
})

// delete user permanently
router.post('/deletePersonalInfo', VerifyToken, jsonParser, [
  check('userId').isNumeric().withMessage('Accept only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.body, function (err, row) {
      if (err) throw err
      if (row.length === 1) {
        const data = {
          userId: req.body.userId,
          firstname: 'Anonymous User',
          lastname: 'Not Available',
          email: 'Not Available',
          phone: '1000000000',
          gender: 'Not Available',
          dob: '1000-01-01',
          address1: 'Not Available',
          address2: 'Not Available',
          city: 'Not Available',
          state: 'Not Available',
          pincode: '000000',
          profilePic: null,
          status: 1,
          roleId: 5
        }
        User.deletePersonalInfo(data, function (err, row) {
          if (err) throw err
          res.json({
            type: 'success',
            msg: 'Deleted successfully!'
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// when you logout the app, the api security token will be updated to null
router.get('/logout', [
  check('emailOrPhone').isLength({
    min: 10
  }).withMessage('Must be the valid email address or phone')
], function (req, res) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // req.session.destroy(function () {
  var nullVal = null;
  User.resetAPIToken(nullVal, req.query.emailOrPhone, function (err, row) {
    if (err) throw err
    if (row.affectedRows > 0) {
      Logs.info('User logged out')
      res.status(200).send({
        type: 'success',
        msg: 'User logged out'
      })
    }
  })
  // })
})

// if already login, call this api
router.post('/apiTokenReset', [
  check('emailOrPhone').isLength({
    min: 10
  }).withMessage('Must be the valid email address or phone'),
  check('token').notEmpty().withMessage('ApI security Token should not be empty')
], function (req, res) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // req.session.destroy(function () {
  var nullVal = null;
  User.resetAPIToken(nullVal, req.body.emailOrPhone, function (err, row) {
    if (err) throw err
    if (row.changedRows === 1) {
      User.selectByEmail(req.body, function (err, rows) {
        if (err) throw err
        if (!rows) {
          return res.status(404).send({
            type: 'failure',
            msg: 'User not found'
          })
        }
        if(rows[0].approveStatus === 1 && rows[0].disable === 0) {
          if (rows[0].apiSecTok === null) {
            User.updateAPIToken(req.body.token, rows[0].userId, async (err, tok) => {
              if (err) throw err
              if (tok.affectedRows === 1) {
                res.status(200).send({
                  type: 'success',
                  msg: 'Successfully logged in !',
                  auth: true,
                  token: req.body.token,
                  user: rows,
                  // bibledownload: await getDownloadedBible(rows[0].userId),
                  // plandownload: await getDownloadedPlan(rows[0].userId)
                  bibledownload: [],
                  plandownload: []
                })
              }
            })
          }
        } else {
          return res.json({
            type: 'failure',
            auth: false,
            msg: 'Not approved yet'
          })
        }
      })
    } else {
      return res.json({
        type: 'failure',
        msg: 'User not found'
      })
    }
  })
  // })
})

// get user by userId web app
router.get('/apiToken', checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectByIdEmail(req.query, function (err, row) {
    if (err) throw err
    if (row.length > 0) {
      User.selectById(req.query.userId, (err, result) => {
        if (err) throw err
        return res.status(200).json({
          type: 'success',
          msg: 'Got the user information successfully',
          result: result
        })
      })
    } else {
      res.status(401).send({
        type: 'failure',
        msg: 'No User exist with this user id'
      })
    }
  })
})

// get user by userId
router.get('/', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.query, function (err, row) {
      if (err) throw err
      if (row.length > 0) {
        User.selectById(req.query.userId, (err, result) => {
          if (err) throw err
          if(result.length) {
            result[0].categoryId = JSON.parse(result[0].categoryId)
          }
          return res.status(200).json({
            type: 'success',
            msg: 'Got the user information successfully',
            result: result
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// get all user info
router.get('/alluser', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // if (user) {
    User.getAllUsers(function (err, rows) {
      function appendLeadingZeros (n) {
        if (n <= 9) {
          return '0' + n
        }
        return n
      }
      if (err) throw err
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'User not found!'
        })
      }
      if (rows.length > 0) {
        const data = []
        rows.forEach(e => {
          const date = e.registerDate.getFullYear() + '-' + appendLeadingZeros(e.registerDate.getMonth() + 1) + '-' + appendLeadingZeros(e.registerDate.getDate())
          const time = appendLeadingZeros(e.registerDate.getHours()) + ':' + appendLeadingZeros(e.registerDate.getMinutes()) + ':' + appendLeadingZeros(e.registerDate.getSeconds())
          const dateTime = date + ' ' + time
          const formatData = {
            userId: e.userId,
            firstname: e.firstname,
            lastname: e.lastname,
            email: e.email,
            password: e.password,
            registerDate: dateTime,
            phone: e.phone,
            gender: e.gender,
            dob: e.dob,
            address1: e.address1,
            address2: e.address2,
            city: e.city,
            state: e.state,
            country: e.country,
            pincode: e.pincode,
            profilePic: e.profilePic,
            roleId: e.roleId,
            status: e.status,
            roleType: e.roleType,
            published: e.published,
            categoryId: JSON.parse(e.categoryId),
            approveStatus: e.approveStatus,
            disable: e.disable
          }
          data.push(formatData)
        })
        res.json(data)
      } else {
        Logs.error('404 error, Cannot get user information', err)
        res.status(404).send({
          type: 'failure',
          msg: 'User not found!'
        })
      }
    })
    // }
  })
})

// update approval status 0=unapprove, 1= approve, 2= reject
router.put('/approval', VerifyToken, [
  check('userId').isNumeric().withMessage('Accept only numbers'),
  check('approveStatus').isNumeric().withMessage('Accept only numbers')
], jsonParser, function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.body, function (err, rows) {
      if (err) throw err
      if (rows.length === 1) {
        User.updateApproveStatus(req.body, function (err, row) {
          if (err) throw err
          if(req.body.approveStatus==2) {
            User.insertReason(req.body, function (err, row) {
              if (err) throw err
            })
          }
          res.json({
            type: 'success',
            msg: 'Updated successfully'
          })
        })
      }
    })
  })
})

module.exports = router

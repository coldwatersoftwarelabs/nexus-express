const express = require('express');
const router = express.Router();
var multer  = require('multer');
const uploadService = require('../UploadService');

router.post('/image', multer({limits:
  {fileSize: process.env.IMAGE_UPLOAD * 1024 * 1024}
  }).single('file'), (async(req, res) => {
    if (!req.file) {
      return res.status(401).json({
        error: 'Please provide an image'
      });
    }

    let fileLocation = await uploadService.uploadFile(req.file, 'images');
    res.json({file: fileLocation});
}));

router.post('/video', multer().single('file'), async function (req, res) {
  if (!req.file) {
    return res.status(401).json({
      error: 'Please provide a video'
    });
  }

  let fileLocation = await uploadService.uploadFile(req.file, 'videos');
  res.json({file: fileLocation});
})

router.post('/audio', multer().single('file'), async function (req, res) {
  if (!req.file) {
    return res.status(401).json({
      error: 'Please provide an audio'
    });
  }

  let fileLocation = await uploadService.uploadFile(req.file, 'audio');
  res.json({file: fileLocation});
})

router.post('/pdf', multer({limits:
  { fileSize: process.env.PDF_UPLOAD * 1024 * 1024 }
  }).single('file'), async function (req, res) {
  if (!req.file) {
    return res.status(401).json({
      error: 'Please provide a pdf'
    })
  }

  let fileLocation = await uploadService.uploadFile(req.file, 'pdf');
  res.json({file: fileLocation});
})

router.post('/doc', multer({limits:
  { fileSize: process.env.DOC_UPLOAD * 1024 * 1024 }
  }).single('file'), async function (req, res) {
    if (!req.file) {
      return res.status(401).json({
        error: 'Please provide a doc'
      })
    }

    let fileLocation = await uploadService.uploadFile(req.file, 'word');
    res.json({file: fileLocation});
})

// router.post('/icon', upload.uploadIcon.single('file'), async function (req, res) {
//   if (!req.file) {
//     res.status(401).json({
//       error: 'Please provide an image'
//     })
//   }
//   return res.status(200).json({
//     name: req.file.filename
//   })
// })

module.exports = router

const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()
const Generalization = require('../models/Generalization')
const Updates = require('../models/Updates')
const User = require('../models/User')
const _ = require('underscore')
const jsonParser = bodyParser.json()
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')

// create generalization
router.post('/', VerifyToken, jsonParser, [
  check('appname').notEmpty().withMessage('Should not be empty'),
  check('bgcolor').notEmpty().withMessage('Should not be empty'),
  check('fontcolor').notEmpty().withMessage('Should not be empty'),
  check('iconsize').notEmpty().withMessage('Should not be empty'),
  check('fontsize').notEmpty().withMessage('Should not be empty'),
  check('logosize').notEmpty().withMessage('Should not be empty'),
  check('logourl').notEmpty().withMessage('Should not be empty'),
  check('iconurl').notEmpty().withMessage('Should not be empty'),
  check('addedBy').isNumeric().withMessage('Must be a number')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Generalization.addGeneralSettings(req.body, async function (err, data) {
      if (err) {
        res.json(err)
      } else {
        // add updates in update table
        var contver = await getVersion(8)
        var dataVer = parseInt(contver.slice(5, 11)) + 1
        contver = contver.slice(0, 5) + '' + dataVer
        Updates.editUpdates(8, contver, function (err, rrr) {
          if (err) return res.status(500).send('Server error!')
          if (rrr.length > 0) {
            res.json(req.body)
          } else {
            res.json(req.body)
          }
        })
      }
    })
  })
})

// get all generalization data
router.get('/', function (req, res, next) {
  Generalization.viewAllGeneral(function (err, row) {
    if (err) return res.status(500).send('Server error!')
    const rows = [];
    _.each(_.groupBy(row, 'ID'), c => {
      const genData = {
        ID: c[0].ID,
        addedat: c[0].addedat,
        addedby: c[0].addedby,
        appname: c[0].appname,
        bgImage: c[0].bgImage,
        bgcolor: c[0].bgcolor,
        editedat: c[0].editedat,
        editedby: c[0].editedby,
        firstname: c[0].firstname,
        fontcolor: c[0].fontcolor,
        fontfamily: c[0].fontfamily,
        fontsize: c[0].fontsize,
        fontupload: c[0].fontupload,
        iconsize: c[0].iconsize,
        iconurl: c[0].iconurl,
        lastname: c[0].lastname,
        logosize: c[0].logosize,
        logourl: c[0].logourl,
        updatedOn: c[0].updatedOn
      }
      if(typeof (req.query.cms) === 'string') {
        genData.bgImage = c[0].bgImage,
        genData.sidebarbgcolor = c[0].sidebarbgcolor,
        genData.sidebarmainmenufontcolor = c[0].sidebarmainmenufontcolor,
        genData.sidebarsubmenufontcolor = c[0].sidebarsubmenufontcolor,
        genData.sidebaractivemenucolor = c[0].sidebaractivemenucolor,
        genData.sidebaractiveiconcolor = c[0].sidebaractiveiconcolor,
        genData.sidebariconcolor = c[0].sidebariconcolor,
        genData.footerbgcolor = c[0].footerbgcolor,
        genData.footerfontcolor = c[0].footerfontcolor,
        genData.footerlogosize = c[0].footerlogosize,
        genData.headerbgcolor = c[0].headerbgcolor,
        genData.headerfontcolor = c[0].headerfontcolor,
        genData.headerlogosize = c[0].headerlogosize
      }
      rows.push(genData);
    })
    res.json({
      type: 'success',
      msg: 'Got successfully',
      row: rows
    })
  })
})

// update generalization
router.put('/:id', VerifyToken, jsonParser, [
  check('appname').notEmpty().withMessage('Should not be empty'),
  check('bgcolor').notEmpty().withMessage('Should not be empty'),
  check('fontcolor').notEmpty().withMessage('Should not be empty'),
  check('iconsize').notEmpty().withMessage('Should not be empty'),
  check('fontsize').notEmpty().withMessage('Should not be empty'),
  check('logosize').notEmpty().withMessage('Should not be empty'),
  check('logourl').notEmpty().withMessage('Should not be empty'),
  check('iconurl').notEmpty().withMessage('Should not be empty'),
  check('editedby').isNumeric().withMessage('Must be a number')
], checkSchema({
  id: {
    in: ['path'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check generalization existence
    const generalCount = await generalCheck(req.params.id)
    if ((generalCount > 0)) {
      Generalization.updateGeneralization(req.params.id, req.body, async function (err, row) {
        if (err) {
          res.json(err)
        } else {
          // add updates in update table
          var contver = await getVersion(8)
          var dataVer = parseInt(contver.slice(5, 11)) + 1
          contver = contver.slice(0, 5) + '' + dataVer
          Updates.editUpdates(8, contver, function (err, rrr) {
            if (err) return res.status(500).send('Server error!')
            if (rrr.length > 0) {
              res.json(req.body)
            } else {
              res.json(req.body)
            }
          })
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

module.exports = router

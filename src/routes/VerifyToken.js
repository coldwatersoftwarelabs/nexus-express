var jwt = require('jsonwebtoken')
var config = require('../../config')
const User = require('../models/User')
const Label = require('../models/Label')
const Page = require('../models/Page')
const Media = require('../models/Media')
const Event = require('../models/Event')
const Bible = require('../models/Bible')
const PlanDetails = require('../models/PlanDetails')
const Generalization = require('../models/Generalization')
const Updates = require('../models/Updates')
const Appfeatures = require('../models/Appfeatures')
const Menu = require('../models/Menu')

function verifyToken(req, res, next) {
  var token = req.headers['x-access-token']
  if (!token) {
    return res.status(403).send({
      type: 'failure',
      auth: false,
      message: 'No token provided.'
    })
  }

  jwt.verify(token, config.secret, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        type: 'failure',
        auth: false,
        message: 'Failed to authenticate token.'
      })
    }

    // if everything good, save to request for use in other routes
    req.userId = decoded.id
    next()
  })
}

userCheck = (userId) => {
  return new Promise((resolve, reject) => {
    let userCount = ''
    User.checkUser(userId, (err, usr) => {
      if (err) throw err
      userCount = usr.length
      resolve(userCount)
    })
  })
}

langCheck = (langId) => {
  return new Promise((resolve, reject) => {
    let langCount = ''
    Label.checkLang(langId, (err, lang) => {
      if (err) throw err
      langCount = lang.length
      resolve(langCount)
    })
  })
}

pageCheck = (pageId) => {
  return new Promise((resolve, reject) => {
    let pageCount = ''
    Page.viewPageByID(pageId, (err, pg) => {
      if (err) throw err
      pageCount = pg.length
      resolve(pageCount)
    })
  })
}

mediaCheck = (mediaId) => {
  return new Promise((resolve, reject) => {
    let mediaCount = ''
    Media.viewAllMediaByID(mediaId, (err, m) => {
      if (err) throw err
      mediaCount = m.length
      resolve(mediaCount)
    })
  })
}

eventCheck = (eventId) => {
  return new Promise((resolve, reject) => {
    let eventCount = ''
    Event.viewEventByID(eventId, (err, ev) => {
      if (err) throw err
      eventCount = ev.length
      resolve(eventCount)
    })
  })
}

engCheck = (engId) => {
  return new Promise((resolve, reject) => {
    let engCount = ''
    Label.getEnglishLabelById(engId, (err, en) => {
      if (err) throw err
      engCount = en.length
      resolve(engCount)
    })
  })
}

footerCheck = (id) => {
  return new Promise((resolve, reject) => {
    let engCount = ''
    Menu.findFooterMenu(id, (err, en) => {
      if (err) throw err
      engCount = en.length
      resolve(engCount)
    })
  })
}

labelCheck = (labelId) => {
  return new Promise((resolve, reject) => {
    let labelCount = ''
    Label.getAllLabelById(labelId, (err, lbl) => {
      if (err) throw err
      labelCount = lbl.length
      resolve(labelCount)
    })
  })
}

bibleCheck = (bibleId) => {
  return new Promise((resolve, reject) => {
    let bibleCount = ''
    Bible.getBibleById(bibleId, (err, bi) => {
      if (err) throw err
      bibleCount = bi.length
      resolve(bibleCount)
    })
  })
}

planxmlCheck = (planId) => {
  return new Promise((resolve, reject) => {
    let planCount = ''
    PlanDetails.fetchPlan(planId, (err, pl) => {
      if (err) throw err
      planCount = pl.length
      resolve(planCount)
    })
  })
}

planCheck = (planId) => {
  return new Promise((resolve, reject) => {
    let planCount = ''
    PlanDetails.fetchPlanById(planId, (err, pl) => {
      if (err) throw err
      planCount = pl.length
      resolve(planCount)
    })
  })
}

planModCheck = (planmodId) => {
  return new Promise((resolve, reject) => {
    let planCount = ''
    PlanDetails.getPlanModDetails(planmodId, (err, pl) => {
      if (err) throw err
      planCount = pl.length
      resolve(planCount)
    })
  })
}

readingCheck = (readId) => {
  return new Promise((resolve, reject) => {
    let readCount = ''
    PlanDetails.getReadingPlanDetail(readId, (err, red) => {
      if (err) throw err
      readCount = red.length
      resolve(readCount)
    })
  })
}

generalCheck = (gId) => {
  return new Promise((resolve, reject) => {
    let generalCount = ''
    Generalization.viewAllGeneral((err, g) => {
      if (err) throw err
      generalCount = g.length
      resolve(generalCount)
    })
  })
}


getVersion = (data) => {
  return new Promise((resolve, reject) => {
    let csmCount = ''
    Updates.getVersionUpdates(data, (err, csq) => {
      if (err) throw err
      csmCount = csq[0].contentVersion
      resolve(csmCount)
    })
  })
}

getMemverseByUser = (uId) => {
  return new Promise((resolve, reject) => {
    Appfeatures.selectMemoryVerseByUserId(uId, async (err, mData) => {
      if (err) throw err
      resolve(mData)
    })
  })
}


module.exports = verifyToken
const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()
const _ = require('underscore')
const Page = require('../models/Page')
const Label = require('../models/Label')
const Updates = require('../models/Updates')
const User = require('../models/User')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')
const { Validator } = require('node-input-validator');


// get published page data
getPageData = (lang, row) => {
  return new Promise((resolve, reject) => {
    const rows = []
    _.each(_.groupBy(lang, 'langId'), l => {
      const pageArray = []
      _.each(row.filter(x => x.language==l[0].langId), p => {
        pageArray.push({
          ID: p.ID,
          title: p.title,
          excerpt: p.excerpt,
          content: p.content,
          parentId: p.parentId,
          showChild: p.showChild,
          bannerImageUrl: p.bannerImageUrl,
          status: p.status,
          order: p.order,
          editedBy: p.editedBy,
          addedBy: p.addedBy,
          createdAt: p.createdAt,
          editedAt: p.editedAt,
          expire: p.expire,
          updatedOn: p.updatedOn,
          langType: p.langType
        })
      })
      const pageData = {
        langId: l[0].langId,
        data: pageArray
      }
      rows.push(pageData)
    })
    resolve(rows)
  })
}


// Pages create, update & delete
router.post('/', VerifyToken, jsonParser, function (req, res, next) {
  // if deleteAccess = 1, then we need to delete. 
  // if ID not exist then add. otherwise update
  if(req.body.deleteAccess == 0) {
    let v = new Validator(req.body, {
      editedBy: 'required|integer',
      excerpt: 'required',
      language: 'required|integer',
      status: 'required',
      title: 'required'
    });

    if(!req.body.ID) {
      v = new Validator(req.body, {
        addedBy: 'required|numeric'
      });
    } else {
      v = new Validator(req.body, {
        ID: 'required|numeric'
      });
    }

    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(404).send('No user found.')
          if (typeof (req.query.cms === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            const langCount = await langCheck(req.body.language)
            if ((userCount > 0) && (langCount > 0)) {
              if(!req.body.ID) {
                // add new pages
                Page.addPage(req.body, async function (err, data) {
                  if (err) {
                    res.json(err)
                  } else {
                    req.body.ID = data.insertId
                    if (data.insertId !== '') {
                      // add updates in update table
                      var contver = await getVersion(4)
                      var dataVer = parseInt(contver.slice(5, 11)) + 1
                      contver = contver.slice(0, 5) + '' + dataVer
                      Updates.editUpdates(4, contver, function (err, row) {
                        if (err) return res.status(500).send('Server error!')
                        res.json({
                          type: 'success',
                          msg: 'Added Successfully'
                        })
                      })
                    } else {
                      res.json({
                        type: 'success',
                        msg: 'Added Successfully'
                      })
                    }
                  }
                })
              } else {
                // update page
                Page.updatePage(req.body.ID, req.body, async function (err, row) {
                  if (err) {
                    res.json(err)
                  } else {
                    // add updates in update table
                    var contver = await getVersion(4)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(4, contver, function (err, rrr) {
                      if (err) return res.status(500).send('Server error!')
                      res.json({
                        type: 'success',
                        msg: 'updated successfully'
                      })
                    })
                  }
                })
              }
            } else {
              res.json({
                type: 'failure',
                msg: 'Incorrect data'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    });
  } else {
    const v = new Validator(req.body, {
      ID: 'required|integer'
    });
    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(404).send('No user found.')
          if (typeof (req.query.cms === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            if ((userCount > 0)) {
              // delete pages
              Page.deletePage(req.body.ID, async function (err, row) {
                if (err) {
                  res.json(err)
                } else {
                  var contver = await getVersion(4)
                  var dataVer = parseInt(contver.slice(5, 11)) + 1
                  contver = contver.slice(0, 5) + '' + dataVer
                  Updates.editUpdates(4, contver, function (err, rrr) {
                    if (err) return res.status(500).send('Server error!')
                    res.json({
                      type: 'success',
                      msg: 'deleted successfully'
                    })
                  })
                }
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'Incorrect data. UserId not found.'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    });
  }
})

// get all pages
router.get('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms) === 'string') {
      //if web application
      Page.viewAllPage(async (err, row) => {
        if (err) return res.status(500).send('Server error!')
        if(row.length>0){
          res.json({ type: 'success', msg: 'Got successfully', row})
        } else {
          res.json({type: 'success', msg: 'No data exist', row})
        }
      })
    } else {
      //if mobile application
      Label.getLanguage(function (err, lang) {
        if (err) return res.status(500).send('Server error!')
        Page.viewAllPublishedPage(async (err, row) => {
          if (err) return res.status(500).send('Server error!')
          if(row.length>0){
            const rows = await getPageData(lang, row)
            res.json({ type: 'success', msg: 'Got successfully', rows})
          } else {
            res.json({type: 'success', msg: 'No data exist', rows})
          }
        })
      })
    }
  })
})


module.exports = router

const express = require('express')
const router = express.Router()
const Help = require('../models/Help')
const Updates = require('../models/Updates')
const User = require('../models/User')
const _ = require('underscore')
var VerifyToken = require('./VerifyToken')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')

// post queries done by mobile user
router.post('/', VerifyToken, [
  check('question.userId').isNumeric().withMessage('Accept only numbers'),
  check('question.priority').isNumeric().withMessage('Accept only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.body.question, function (err, rows) {
      if (err) throw err
      if (rows.length === 1) {
        Help.addHelpQuery(req.body.question, function (err, result) {
          if (err) throw err
          const attachments = []
          _.each(req.body.attachment, image => {
            attachments.push([result.insertId, image])
          })
          if (req.body.attachment.length) {
            Help.addAttachment(attachments, (err, result1) => {
              if (err) throw err
            })
          }
          res.json({
            type: 'success',
            msg: 'Added Successfully',
            id: result.insertId
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'Not found'
        })
      }
    })
  })
})

// post reply done by web admin
router.post('/answer', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Help.addHelpAnswer(req.body.answer, (err, result) => {
      if (err) throw err
      const attachments = []
      _.each(req.body.ansimageUrl, image => {
        attachments.push([result.insertId, image])
      })
      if (req.body.ansimageUrl.length) {
        Help.addansAttachment(attachments, (err, result1) => {
          if (err) throw err
        })
      }
      const helD = {
        queryId: req.body.answer.queryId,
        status: req.body.helpStatus,
        answered: req.body.helpStatus ==4 ? 1 : 0
      }
      Help.updateHelpStatus(helD, async (err, result2) => {
        if (err) throw err
        var contver = await getVersion(14)
        var dataVer = parseInt(contver.slice(5, 11)) + 1
        contver = contver.slice(0, 5) + '' + dataVer
        Updates.editUpdates(14, contver, function (err, row) {
          if (err) return res.status(500).send('Server error!')
          res.json({
            type: 'success',
            msg: 'Updated successfully'
          })
        })
      })
    })
  })
})

router.get('/queries', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Help.getQueries(req.query.searchParam, (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

router.get('/fetchQuery', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Help.getAQuery(req.query.queryId, (err, result) => {
      if (err) throw err
      const attachment = []
      _.each(result, data => {
        attachment.push(data.imageUrl)
      })
      const question = {
        id: result[0].id,
        query: result[0].query,
        priority: result[0].qPriority,
        attachment: _.compact(attachment)
      }
      res.json(question)
    })
  })
})

router.get('/HelpDeskChat', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const userCount = await userCheck(req.query.userId)
    if ((userCount > 0)) {
      Help.getChat(req.query.userId, (err, result) => {
        if (err) throw err
        const queries = []
        _.each(_.groupBy(result, 'qid'), question => {
          const answers = []
          const attachment = []
          _.each(_.groupBy(question, 'aid'), val1 => {
            if(val1[0].aid) {
              const ansattachment = []
              if(val1[0].ansattachmentId) {
                ansattachment.push(val1[0].ansimageUrl)
              }
              answers.push({
                answer: val1[0].answer,
                id: val1[0].aid,
                ansattachment: ansattachment,
                createdAt: (typeof (req.query.cms) === 'string') ? val1[0].webatime : val1[0].appatime,
              })
            }
          })
          _.each(_.groupBy(question, 'attachmentId'), val1 => {
            attachment.push(val1[0].imageUrl)
          })
          queries.push({
            queryId: question[0].qid,
            query: question[0].query,
            status: question[0].status,
            answered: question[0].answered,
            createdAt: (typeof (req.query.cms) === 'string') ? question[0].webqtime : question[0].appqtime,
            answers,
            attachment: _.compact(attachment)
          })
        })
        res.json(queries)
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

router.get('/HelpAnswered', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Help.getAllAnswered('', (err, result) => {
      if (err) throw err
      const queries = []
      _.each(_.groupBy(result, 'qid'), question => {
        const answers = []
        const attachment = []
        const ansattachment = []
        _.each(_.groupBy(question, 'aid'), val1 => {
          _.each(_.groupBy(val1, 'ansattachmentId'), val2 => {
            ansattachment.push(val2[0].ansimageUrl)
          })
          answers.push({
            adminuser_fname: val1[0].adminuser_fname,
            adminuser_lname: val1[0].adminuser_lname,
            answer: val1[0].answer,
            id: val1[0].aid,
            createdAt: val1[0].atime,
            ansattachment: _.compact(ansattachment)
          })
        })
        _.each(_.groupBy(question, 'attachmentId'), val1 => {
          attachment.push(val1[0].imageUrl)
        })
        queries.push({
          appuser_fname: question[0].appuser_fname,
          appuser_lname: question[0].appuser_lname,
          phone: question[0].appuser_phone,
          queryId: question[0].qid,
          query: question[0].query,
          createdAt: question[0].qtime,
          answers,
          attachment: _.compact(attachment)
        })
      })
      res.json(queries)
    })
  })
})

router.get('/allqueries', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Help.allQuery((err, result) => {
      if (err) throw err
      const rows = [];
      _.each(_.groupBy(result, 'id'), hd => {
        const helpData = {
          id: hd[0].id, 
          userId: hd[0].userId, 
          priorityId: hd[0].priorityId, 
          priority: hd[0].priority, 
          query: hd[0].query, 
          createdAt: (typeof (req.query.cms) === 'string') ? hd[0].webCreatedAt : hd[0].appCreatedAt,
          answered: hd[0].answered, 
          status: hd[0].status,
          firstname: hd[0].firstname, 
          lastname: hd[0].lastname, 
          phone: hd[0].phone, 
          profilePic: hd[0].profilePic
        }
        rows.push(helpData);
      })
      res.json(rows)
    })
  })
})

module.exports = router

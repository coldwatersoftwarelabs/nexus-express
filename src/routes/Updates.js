const express = require('express')
const router = express.Router()
const Updates = require('../models/Updates')
const User = require('../models/User')
const {
  check,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')

// web app sync
router.get('/', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectById(req.query.userId, function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (rows.length === 1) {
        Updates.getUpdates(function (err, row) {
          if (err) return res.status(500).send('Server error!')
          res.json(row)
        })
      } else {
        res.json([])
      }
    })
  })
})

router.post('/applocalsync', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('name').notEmpty().withMessage('name should not be empty'),
  check('lastUpdateDate').isISO8601().toDate().withMessage('Must be in correct format yyyy-mm-dd hh:mm:ss ')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        Updates.addAppUpdates(req.body, (err, result) => {
          if (err) throw err
          res.json({
            type: 'success',
            msg: 'Updates added successfully'
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// mobile app sync
router.get('/applocalsync', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectById(req.query.userId, function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (rows.length === 1) {
        Updates.getAppUpdates(function (err, row) {
          if (err) return res.status(500).send('Server error!')
          res.json(row)
        })
      } else {
        res.json([])
      }
    })
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const Updates = require('../models/Updates')
const Bible = require('../models/Bible')
const User = require('../models/User')
const _ = require('underscore')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')

// to get xml generated bible
router.get('/', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check user exist by userId
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const userCount = await userCheck(req.query.userId)
    if ((userCount > 0)) {
      // if user exist then get xml generated bible
      Bible.GetXmlBible(req.query.userId, (err, result) => {
        if (err) throw err
        if (result.length > 0) {
          const rows = [];
          _.each(_.groupBy(result, 'id'), c => {
            const bibleData = {
              id: c[0].id,
              name: c[0].name,
              xmlurl: c[0].xmlurl, 
              langType: c[0].langType, 
              langId: c[0].langId, 
              tblName: c[0].tblName, 
              downloadStatus: c[0].downloadStatus
            }
            if(typeof (req.query.cms === 'string')) {
              bibleData.userCount = c[0].userCount
            }
            rows.push(bibleData);
          })
          res.json(rows)
        } else {
          res.json([])
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// to get unique chapter based on book
router.get('/distinctchapter', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Bible.GetDistinctBible('', (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

// create or add bible
router.post('/', VerifyToken, [
  check('langId').isNumeric().withMessage('Must be only numbers'),
  check('name').notEmpty().withMessage('Should not be empty'),
  check('xmlurl').notEmpty().withMessage('xmlurl should not be empty'),
  check('tblName').notEmpty().withMessage('tblName should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check user existence
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // call langCheck check language existence
    const langCount = await langCheck(req.body.langId)
    if ((langCount > 0)) {
      Bible.addBible(req.body, async function (err, data) {
        if (err) {
          res.json(err)
        } else {
          req.body.ID = data.insertId
          if (data.insertId !== '') {
            var contver = await getVersion(9)
            var dataVer = parseInt(contver.slice(5, 11)) + 1
            contver = contver.slice(0, 5) + '' + dataVer
            Updates.editUpdates(9, contver, function (err, row) {
              if (err) return res.status(500).send('Server error!')
              if (row.length > 0) {
                res.json(req.body)
              } else {
                res.json(req.body)
              }
            })
          } else {
            res.json(req.body)
          }
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// update bible
router.put('/:id', VerifyToken, [
  check('id').isNumeric().withMessage('Must be only numbers'),
  check('langId').isNumeric().withMessage('Must be only numbers'),
  check('name').notEmpty().withMessage('Should not be empty'),
  check('xmlurl').notEmpty().withMessage('xmlurl should not be empty'),
  check('tblName').notEmpty().withMessage('tblName should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check user existence
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check language and bible existence
    const langCount = await langCheck(req.body.langId)
    const bibleCount = await bibleCheck(req.body.id)
    if ((langCount > 0) && (bibleCount > 0)) {
      Bible.updateBible(req.body, async function (err, row) {
        if (err) {
          res.json(err)
        } else {
          var contver = await getVersion(9)
          var dataVer = parseInt(contver.slice(5, 11)) + 1
          contver = contver.slice(0, 5) + '' + dataVer
          Updates.editUpdates(9, contver, function (err, rrr) {
            if (err) return res.status(500).send('Server error!')
            if (rrr.length > 0) {
              res.json(req.body)
            } else {
              res.json(req.body)
            }
          })
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// delete bible
router.post('/deletebible', VerifyToken, [
  check('id').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  // check user existence
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check bible existence
    const bibleCount = await bibleCheck(req.body.id)
    if ((bibleCount > 0)) {
      Bible.deleteBible(req.body.id, function (err, row) {
        if (err) {
          res.json(err)
        } else {
          res.json(row)
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// if app user click to download bible, then it will map.
router.post('/mapBibleToUser', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('bibleId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check bible existence
        Bible.fetchBible(req.body.bibleId, (err, row) => {
          if (err) throw err
          if (row.length > 0) {
            // check bible already mapped with that particular user.
            Bible.checkBibleMappedorNot(req.body, (err, maped) => {
              if (err) throw err
              if (maped.length === 0) {
                // map bible to user
                Bible.addBibleToUser(req.body, (err, response) => {
                  if (err) throw err
                  res.json({
                    type: 'success',
                    msg: "Added bible to user's list! downloading"
                  })
                })
              } else {
                res.json({
                  type: 'success',
                  msg: "Added bible to user's list! downloading"
                })
              }
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No bible exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// if bible downloaded, then it'll call this api.
router.put('/bible/downloadstatus', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('bibleId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check bible existence
        Bible.fetchBible(req.body.bibleId, (err, row) => {
          if (err) throw err
          if (row.length > 0) {
            // update download status to 2
            Bible.updateDownloadStatus(req.body, (err, result) => {
              if (err) throw err
              if (result.affectedRows === 1) {
                res.json({
                  type: 'success',
                  msg: 'Updated sccessfully!'
                })
              }
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No bible exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// call when bible selected
router.put('/bible/selectedbible', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('bibleId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check bible existence
        Bible.fetchBible(req.body.bibleId, (err, row) => {
          if (err) throw err
          if (row.length > 0) {
            Bible.updateAlreadySelectedBible(req.body, (err, re) => {
              if (err) throw err
              // update download status to 3
              Bible.updateDownloadStatus(req.body, (err, result) => {
                if (err) throw err
                res.json({
                  type: 'success',
                  msg: 'Updated sccessfully!'
                })
              })
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No bible exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

module.exports = router

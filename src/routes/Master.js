const express = require('express')
const router = express.Router()
const User = require('../models/User')
const Master = require('../models/Master')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')


// get all existing role type
router.get('/getrole', function (req, res) {
  Master.getAllRoleType(function (err, rows) {
    if (err) return res.status(500).send('Server error!')
    if (!rows) {
      return res.status(404).send({
        type: 'failure',
        msg: 'User not found!'
      })
    }
    if (rows.length > 0) {
      res.json(rows)
    } else {
      res.json([])
    }
  })
})

// add new role type
router.post('/addrole', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createRole(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update role type by roleId
router.put('/role/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateRole(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete role
router.post('/deleterole', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.roleStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})


// get all existing level
router.get('/getlevel', function (req, res) {
  Master.getAllLevel(function (err, rows) {
    if (err) return res.status(500).send('Server error!')
    if (!rows) {
      return res.status(404).send({
        type: 'failure',
        msg: 'User not found!'
      })
    }
    if (rows.length > 0) {
      res.json(rows)
    } else {
      res.json([])
    }
  })
})

// add new level
router.post('/addlevel', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createLevel(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update level by levelId
router.put('/level/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateLevel(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete level
router.post('/deletelevel', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.levelStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})


// get all existing category
router.get('/getcategory', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllCategory(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'User not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new category
router.post('/addcategory', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createCategory(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update category type by categoryId
router.put('/category/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateCategory(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete category
router.post('/deletecategory', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.categoryStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})


// get all country list
router.get('/getcountry', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllCountry(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'Country not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new country
router.post('/addcountry', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createCountry(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update country by id
router.put('/country/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateCountry(req.body, function (err, row) {
      if (err) {
      res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete country
router.post('/deletecountry', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.countryStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// get all state list
router.get('/getstate', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllState(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'State not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new state
router.post('/addstate', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createState(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update state by id
router.put('/state/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateState(req.body, function (err, row) {
      if (err) {
      res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete state
router.post('/deletestate', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.stateStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// get all city list
router.get('/getcity', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllCity(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'City not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new city
router.post('/addcity', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createCity(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update city by id
router.put('/city/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateCity(req.body, function (err, row) {
      if (err) {
      res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete city
router.post('/deletecity', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.cityStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// get all denomination list
router.get('/getdenomination', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllDenomination(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'Denomination not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new denomination
router.post('/addDenomination', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createDenomination(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// update denomination by Id
router.put('/denomination/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateDenomination(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete denomination
router.post('/deletedenomination', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.denominationStatusForDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// get all media classification
router.get('/mediaClassification', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllMediaClassify(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'Country not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

// add new media classification
router.post('/mediaClassification', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createMediaClassify(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

// edit media classification by Id
router.put('/mediaClassification/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updateMediaClassification(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// delete media classification
router.post('/mediaClassification/delete', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.mediaClassificationDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

// priority

router.get('/priority', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.getAllPriority(function (err, rows) {
      if (err) return res.status(500).send('Server error!')
      if (!rows) {
        return res.status(404).send({
          type: 'failure',
          msg: 'Priority not found!'
        })
      }
      if (rows.length > 0) {
        res.json(rows)
      } else {
        res.json([])
      }
    })
  })
})

router.post('/addpriority', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.createPriority(req.body, function (err, count) {
      if (err) {
        res.json(err)
      } else {
        res.json(req.body) // or return count for 1 &ampampamp 0
      }
    })
  })
})

router.put('/priority/:id', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.updatePriority(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

router.post('/deletepriority', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Master.priorityDelete(req.body, function (err, row) {
      if (err) {
        res.json(err)
      } else {
        res.json(row)
      }
    })
  })
})

module.exports = router

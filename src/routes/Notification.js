const express = require('express')
const router = express.Router()
const Notification = require('../models/Notification')
const User = require('../models/User')
const _ = require('underscore')
const fcm = require('../../fcm_push-notification')
const {
  check,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')

// send notification to all app users
router.post('/all', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Notification.getAllUsers((err, result) => {
      if (err) throw err
      fcm.sendNotification(_.pluck(result, 'deviceToken'), req.body.title, req.body.message, 'bulk')
        .then((msg) => {
          res.json(msg)
        })
        .catch((err) => {
          res.json(err)
        })
    })
  })
})

// send notification based on location
router.post('/location', VerifyToken, [
  check('message').notEmpty().withMessage('Message field should not be empty'),
  check('location').notEmpty().withMessage('Location field should not be empty')
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Notification.getUserBasedOnLocation(req.body.location, (err, result) => {
      if (err) throw err
      fcm.sendNotification(_.pluck(result, 'deviceToken'), req.body.title, req.body.message, 'bulk')
        .then((msg) => {
          res.json(msg)
        })
        .catch((err) => {
          res.json(err)
        })
    })
  })
})

// send notification to a single user
router.post('/user', VerifyToken, [
  check('message').notEmpty().withMessage('Message field should not be empty'),
  check('userId').isNumeric().withMessage('UserId field should be integer')
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectByIdEmail(req.body, function (err, rows) {
      if (err) throw err
      if (rows.length === 1) {
        Notification.getAUserToken(req.body.userId, (err, result) => {
          if (err) throw err
          fcm.sendNotification(_.pluck(result, 'deviceToken'), req.body.title, req.body.message, 'single')
            .then((msg) => {
              res.json(msg)
            })
            .catch((err) => {
              res.json(err)
            })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'Not found'
        })
      }
    })
  })
})

// send notification to a group
router.post('/group', VerifyToken, [
  check('message').notEmpty().withMessage('Message field should not be empty'),
  check('groupId').isNumeric().withMessage('groupId field should be integer')
], (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Group.checkGroupExist(req.body.groupId, function (err, rows) {
      if (err) throw err
      if (rows.length === 1) {
        Notification.getUserBasedOnGroup(req.body.groupId, (err, result) => {
          if (err) throw err
          fcm.sendNotification(_.pluck(result, 'deviceToken'), req.body.title, req.body.message, 'bulk')
            .then((msg) => {
              res.json(msg)
            })
            .catch((err) => {
              res.json(err)
            })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'Not found'
        })
      }
    })
  })
})

module.exports = router

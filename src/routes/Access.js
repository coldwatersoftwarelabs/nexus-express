const express = require('express')
const router = express.Router()
const Access = require('../models/Access')
const User = require('../models/User')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')

router.post('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      Access.addAccess(req.body, function (err, count) {
        if (err) {
          res.json(err)
        } else {
          res.json(req.body) // or return count for 1 &ampampamp 0
        }
      })
    } else {
      res.status(403).end()
    }
  })
})

router.get('/', VerifyToken, function (req, res) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      User.selectById(req.query.userId, function (err, rows) {
        if (err) return res.status(500).send('Server error!')
        if (rows.length === 1) {
          Access.getAccess(function (err, row) {
            if (err) return res.status(500).send('Server error!')
            res.json(row)
          })
        }
      })
    } else {
      res.status(403).end()
    }
  })
})

router.put('/', VerifyToken, jsonParser, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms === 'string') && req.query.userId) {
      User.selectById(req.body.userId, function (err, rows) {
        if (err) return res.status(500).send('Server error!')
        if (rows.length === 1) {
          Access.editAccess(req.params.id, req.body, function (err, row) {
            if (err) {
              res.json(err)
            } else {
              res.json(row)
            }
          })
        } else {
          res.status(403).end()
        }
      })
    } else {
      res.status(403).end()
    }
  })
})

module.exports = router
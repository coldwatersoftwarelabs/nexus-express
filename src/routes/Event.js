const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()
const _ = require('underscore')
const Event = require('../models/Event')
const Label = require('../models/Label')
const Updates = require('../models/Updates')
const User = require('../models/User')
const jsonParser = bodyParser.json()
var VerifyToken = require('./VerifyToken')
const { Validator } = require('node-input-validator');


// get published event data
getEventData = (lang, row) => {
  return new Promise((resolve, reject) => {
    const rows = []
    _.each(_.groupBy(lang, 'langId'), l => {
        const eventArray = []
        _.each(row.filter(x => x.language==l[0].langId), ev => {
          eventArray.push({
            ID: ev.ID,
            title: ev.title,
            excerpt: ev.excerpt,
            content: ev.content,
            bannerImageUrl: ev.bannerImageUrl,
            status: ev.status,
            location: ev.location,
            latitude: ev.latitude,
            longitude: ev.longitude,
            featured: ev.featured,
            startDate: ev.startDate,
            endDate: ev.endDate,
            order: ev.order,
            editedBy: ev.editedBy,
            addedBy: ev.addedBy,
            createdAt: ev.createdAt,
            editedAt: ev.editedAt,
            expire: ev.expire,
            langType: ev.langType
          })
        })
        const eventData = {
          langId: l[0].langId,
          data: eventArray
        }
        rows.push(eventData)
      })
    resolve(rows)
  })
}


// Event create, update & delete
router.post('/', VerifyToken, jsonParser, function (req, res, next) {
  // if deleteAccess = 1, then we need to delete. 
  // if ID not exist then add. otherwise update
  if(req.body.deleteAccess == 0) {
    let v = new Validator(req.body, {
      editedBy: 'required|numeric',
      excerpt: 'required',
      language: 'required|numeric',
      status: 'required',
      title: 'required'
    });
    if(!req.body.ID) {
      v = new Validator(req.body, {
        addedBy: 'required|numeric'
      });
    } else {
      v = new Validator(req.body, {
        ID: 'required|numeric'
      });
    }

    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(404).send('No user found.')
          if ((typeof (req.query.cms) === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            const langCount = await langCheck(req.body.language)
            if ((userCount > 0) && (langCount > 0)) {
              if(!req.body.ID) {
                delete req.body.deleteAccess
                // add new event
                Event.addEvent(req.body, async function (err, data) {
                  if (err) {
                    res.json(err)
                  } else {
                    req.body.ID = data.insertId
                    if (data.insertId !== '') {
                      // add updates in update table
                      var contver = await getVersion(3)
                      var dataVer = parseInt(contver.slice(5, 11)) + 1
                      contver = contver.slice(0, 5) + '' + dataVer
                      Updates.editUpdates(3, contver, function (err, row) {
                        if (err) return res.status(500).send('Server error!')
                        res.json(req.body)
                      })
                    } else {
                      res.json(req.body)
                    }
                  }
                })
              } else {
                // update event
                Event.updateEvent(req.body.ID, req.body, async function (err, row) {
                  if (err) {
                    res.json(err)
                  } else {
                    // add updates in update table
                    var contver = await getVersion(3)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(3, contver, function (err, rrr) {
                      if (err) return res.status(500).send('Server error!')
                      res.json({
                        type: 'success',
                        msg: 'Updated successfully'
                      })
                    })
                  }
                })
              }
            } else {
              res.json({
                type: 'failure',
                msg: 'incorrect data'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    })
  } else {
    let v = new Validator(req.body, {
      ID: 'required|numeric'
    });

    v.check().then((matched) => {
      if (!matched) {
        res.status(422).send(v.errors);
      } else {
        User.selectById(req.userId, async (err, user) => {
          if (err) return res.status(500).send('There was a problem finding the user.')
          if (!user) return res.status(404).send('No user found.')
          if ((typeof (req.query.cms) === 'string') && req.query.userId) {
            // check user and language existence
            const userCount = await userCheck(req.query.userId)
            if ((userCount > 0)) {
              // delete event
              Event.deleteEvent(req.body.ID, async function (err, row) {
                if (err) {
                  res.json(err)
                } else {
                  // add updates in update table
                  var contver = await getVersion(3)
                  var dataVer = parseInt(contver.slice(5, 11)) + 1
                  contver = contver.slice(0, 5) + '' + dataVer
                  Updates.editUpdates(3, contver, function (err, rrr) {
                    if (err) return res.status(500).send('Server error!')
                    res.json({
                      type: 'success',
                      msg: 'Deleted successfully'
                    })
                  })
                }
              })
            } else {
              res.json({
                type: 'failure',
                msg: 'incorrect data. userId not found'
              })
            }
          } else {
            res.status(403).end()
          }
        })
      }
    })
  }
})

// get all event data
router.get('/', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    if (typeof (req.query.cms) === 'string') {
      //if web application
      Event.viewAllEvent(async (err, row) => {
        if (err) return res.status(500).send('Server error!')
        if(row.length>0){
          res.json({ type: 'success', msg: 'Got successfully', row})
        } else {
          res.json({type: 'success', msg: 'No data exist', row})
        }
      })
    } else {
      //if mobile application
      Label.getLanguage(function (err, lang) {
        if (err) return res.status(500).send('Server error!')
        Event.viewAllPublishedEvent(async (err, row) => {
          if (err) return res.status(500).send('Server error!')
          if(row.length>0){
            const rows = await getEventData(lang, row)
            res.json({ type: 'success', msg: 'Got successfully', rows})
          } else {
            res.json({type: 'success', msg: 'No data exist', rows})
          }
        })
      })
    }
  })
})

module.exports = router

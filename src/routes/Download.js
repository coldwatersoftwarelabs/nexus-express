const express = require('express')
const router = express.Router()
const Download = require('../models/Download')
const _ = require('underscore')
const User = require('../models/User')
const xml = require('../../xml')
var VerifyToken = require('./VerifyToken')
const Updates = require('../models/Updates')
const {
  check,
  validationResult
} = require('express-validator')

const bibleXMLFormat = (bibleContent, bibleInfo) => {
  return new Promise((resolve, reject) => {
    const bibleArray = []
    const bookArray = []
    const vNumberTag = '@vnumber'
    const verseTag = 'VERSE'
    const textTag = '#text'
    const chapterTag = 'CHAPTER'
    const chapterNumberTag = '@cnumber'
    const bookTag = 'BIBLEBOOK'
    const bookNumberTag = '@bnumber'
    const bookNameTag = '@bname'
    const infoTag = 'INFORMATION'
    const titleTag = 'title'
    const creatorTag = 'creator'
    const descriptionTag = 'description'
    const publisherTag = 'publisher'
    const dateTag = 'date'
    const typeTag = 'type'
    const identifierTag = 'identifier'
    const sourceTag = 'source'
    const languageTag = 'language'
    const coverageTag = 'coverage'
    const rightsTag = 'rights'
    _.each(_.groupBy(bibleContent, 'id'), book => {
      const chapterArray = []
      const bookName = book[0].name
      const bookId = book[0].id
      _.each(_.groupBy(book, 'chapterId'), chapter => {
        const chapterNumber = chapter[0].chapterId
        const verseArray = []
        _.each(chapter, ch => {
          verseArray.push({
            [verseTag]: {
              [vNumberTag]: ch.verseId,
              [textTag]: ch.verse
            }
          })
          delete ch.chapterId
          delete ch.name
          delete ch.id
        })
        chapterArray.push({
          [chapterTag]: {
            [chapterNumberTag]: chapterNumber,
            [verseTag]: verseArray
          }
        })
      })
      bookArray.push({
        [bookTag]: {
          [bookNumberTag]: bookId,
          [bookNameTag]: bookName,
          [chapterTag]: chapterArray
        }
      })
    })
    bibleArray.push({
      [infoTag]: {
        [titleTag]: {
          [textTag]: bibleInfo[0].name
        },
        [creatorTag]: {
          [textTag]: bibleInfo[0].creator
        },
        [descriptionTag]: {
          [textTag]: bibleInfo[0].description
        },
        [publisherTag]: {
          [textTag]: bibleInfo[0].publisher
        },
        [typeTag]: {
          [textTag]: bibleInfo[0].type
        },
        [identifierTag]: {
          [textTag]: bibleInfo[0].identifier
        },
        [sourceTag]: {
          [textTag]: bibleInfo[0].source
        },
        [languageTag]: {
          [textTag]: bibleInfo[0].langType
        },
        [coverageTag]: {
          [textTag]: bibleInfo[0].coverage
        },
        [rightsTag]: {
          [textTag]: bibleInfo[0].rights
        }
      },
      [bookTag]: bookArray
    })
    resolve(bookArray)
  })
}

// generate xml for bible
router.get('/bible/generatexml', VerifyToken, [
  check('bibleId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Download.getBibleTableName(req.query.bibleId, (err, bibleDBName) => {
      if (err) throw err
      if (bibleDBName.length > 0) {
        Download.getBibleContent(bibleDBName[0].dbname, async (err, bibleContent) => {
          if (err) throw err
          const bookArray = await bibleXMLFormat(bibleContent, bibleDBName)
          xml.generateXML({
            content: bookArray,
            name: bibleDBName[0].name,
            source: 'bible'
          }).then(result => {
            const mapPayload = {
              bibleId: req.query.bibleId,
              xmlurl: result
            }
            Download.updateBibleDownloadedUrl(mapPayload, async (err, mapResult) => {
              if (err) {
                res.json(err)
              } else {
                // add updates in update table
                var contver = await getVersion(9)
                var dataVer = parseInt(contver.slice(5, 11)) + 1
                contver = contver.slice(0, 5) + '' + dataVer
                Updates.editUpdates(9, contver, function (err, rrr) {
                  if (err) return res.status(500).send('Server error!')
                  if (rrr.length > 0) {
                    res.json(mapPayload.xmlurl)
                  } else {
                    res.json(mapPayload.xmlurl)
                  }
                })
              }
            })
          })
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'bible not exist'
        })
      }
    })
  })
})

// generate xml for plan
router.get('/plan/generatexml', VerifyToken, [
  check('planId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Download.getReadingPlan(req.query.planId, (err, planDetails) => {
      if (err) throw err
      if (planDetails.length > 0) {
        const textTag = '#text'
        const infoTag = 'INFORMATION'
        const titleTag = 'title'
        const plandurationTag = 'Duration'
        const makeupdayTag = 'make-up-day'
        const readingsperdayTag = 'readings-per-day'
        const creatorTag = 'creator'
        const descriptionTag = 'description'
        const publisherTag = 'publisher'
        const typeTag = 'type'
        const identifierTag = 'identifier'
        const sourceTag = 'source'
        const coverageTag = 'coverage'
        const rightsTag = 'rights'
        const planTag = 'plans'
        const dateNumTag = '@dayOrDate'
        const readingPlanTag = '@readingPlanId'
        let planArray = {}
        const planInfo = []
        const first_Reading = 'first_Reading'
        const readingtextTag = '#text'
        const second_Reading = 'second_Reading'
        const third_Reading = 'third_Reading'
        const fourth_Reading = 'fourth_Reading'
        const dArray = []
        let planName
        planArray = _.map(_.groupBy(planDetails, 'dayOrDate'), plan => {
          const planArr = []
          // const DayOrDate = ((planDetails[0].byDate) === 0) ?
          //     'Day' : 'Date'
          // const dayTag = `${DayOrDate}-${plan[0].dayOrDate}`
          const dayTag = 'DayOrDate'
          planName = plan[0].name
          for (var p = 0; p < plan.length; p++) {
            // if (plan[p]) {
            var checkSamePn1 = plan[p].fromBook === plan[p].toBook && plan[p].fromChapter === plan[p].toChapter && plan[p].fromVerse === 1 && plan[p].toVerse === plan[p].toVerseCount
            var checkSamePn1Mv = plan[p].fromBook === plan[p].toBook && plan[p].fromChapter === plan[p].toChapter && (plan[p].fromVerse !== 1 || plan[p].toVerse < plan[p].toVerseCount)
            var checkSamebkDifchapPn1 = plan[p].fromBook === plan[p].toBook && plan[p].fromChapter !== plan[p].toChapter && plan[p].fromVerse === 1 && plan[p].toVerse === plan[p].toVerseCount
            var checkSamebkDifchapPn1Mv = plan[p].fromBook === plan[p].toBook && plan[p].fromChapter !== plan[p].toChapter && (plan[p].fromVerse === 1 && plan[p].toVerse < plan[p].toVerseCount)
            var checkSamebkDifchapPn1Mv2 = plan[p].fromBook === plan[p].toBook && plan[p].fromChapter !== plan[p].toChapter && (plan[p].fromVerse !== 1 || plan[p].toVerse < plan[p].toVerseCount)
            var checkDifbkSamechapPn1Mv1 = plan[p].fromBook !== plan[p].toBook && plan[p].fromChapter !== plan[p].toChapter && (plan[p].fromVerse === 1 && plan[p].toVerse === plan[p].toVerseCount)
            var checkDifbkSamechapPn1Mv2 = plan[p].fromBook !== plan[p].toBook && plan[p].fromChapter !== plan[p].toChapter && (plan[p].fromVerse !== 1 || plan[p].toVerse < plan[p].toVerseCount)
            if (checkSamePn1) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter
                }
              }
            } else if (checkSamePn1Mv) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toVerse
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toVerse
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toVerse
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toVerse
                }
              }
            } else if (checkSamebkDifchapPn1) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter
                }
              }
            } else if (checkSamebkDifchapPn1Mv) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              }
            } else if (checkSamebkDifchapPn1Mv2) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              }
            } else if (checkDifbkSamechapPn1Mv1) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toAbb + ' ' + plan[p].toChapter
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              }
            } else if (checkDifbkSamechapPn1Mv2) {
              if (p === 0) {
                planArr.push({
                  [first_Reading]: {
                    [readingPlanTag]: plan[p].readingPlanId,
                    [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                  },
                  second_Reading: '',
                  third_Reading: '',
                  fourth_Reading: ''
                })
              } else if (p === 1) {
                planArr[0].second_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 2) {
                planArr[0].third_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              } else if (p === 3) {
                planArr[0].fourth_Reading = {
                  [readingPlanTag]: plan[p].readingPlanId,
                  [readingtextTag]: plan[p].fromAbb + ' ' + plan[p].fromChapter + ':' + plan[p].fromVerse + '-' + plan[p].toAbb + ' ' + plan[p].toChapter + ':' + plan[p].toVerse
                }
              }
            }
            // }
          }
          const obj = {
            [dayTag]: {
              [dateNumTag]: planDetails[0].byDate === 0 ? (plan[0].dayOrDate) : (new Date(plan[0].dayOrDate).toUTCString().substring(5, 11)),
              planArr
            }
          }
          return obj
        })
        planInfo.push({
          [infoTag]: {
            [titleTag]: {
              [textTag]: planDetails[0].name
            },
            [plandurationTag]: {
              [textTag]: planDetails[0].planduration
            },
            [makeupdayTag]: {
              [textTag]: planDetails[0].makeupday
            },
            [readingsperdayTag]: {
              [textTag]: planDetails[0].readingsperday
            },
            [creatorTag]: {
              [textTag]: planDetails[0].creator
            },
            [descriptionTag]: {
              [textTag]: planDetails[0].description
            },
            [publisherTag]: {
              [textTag]: planDetails[0].publisher
            },
            [typeTag]: {
              [textTag]: planDetails[0].type
            },
            [identifierTag]: {
              [textTag]: planDetails[0].identifier
            },
            [sourceTag]: {
              [textTag]: planDetails[0].source
            },
            [coverageTag]: {
              [textTag]: planDetails[0].coverage
            },
            [rightsTag]: {
              [textTag]: planDetails[0].rights
            }
          },
          [planTag]: planArray
        })
        xml.generateXML({
          planDetails: planInfo,
          source: 'plan',
          plan: planName
        }).then(result => {
          const mapPayload = {
            planId: req.query.planId,
            xmlurl: result
          }
          Download.updatePlanDownloadedUrl(mapPayload, async (err, mapResult) => {
            if (err) {
              res.json(err)
            } else {
              var contver = await getVersion(10)
              var dataVer = parseInt(contver.slice(5, 11)) + 1
              contver = contver.slice(0, 5) + '' + dataVer
              Updates.editUpdates(10, contver, function (err, rrr) {
                if (err) return res.status(500).send('Server error!')
                if (rrr.length > 0) {
                  res.json(mapPayload.xmlurl)
                } else {
                  res.json(mapPayload.xmlurl)
                }
              })
            }
          })
        })
      } else {
        res.json({
          type: 'failure',
          msg: 'Plan not exist'
        })
      }
    })
  })
})

// download bible by bibleId
router.get('/bible', VerifyToken, [
  check('bibleId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Download.selectByBibleId(req.query.bibleId, (err, results) => {
      if (err) throw err
      if (results.length === 1) {
        Download.getXmlUrl(req.query.bibleId, (err, result) => {
          if (err) throw err
          if (result.length > 0) {
            res.json({
              type: 'success',
              msg: 'Got xml url successfully',
              result: result
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No bible xml exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No bible exist with this id'
        })
      }
    })
  })
})

// download plan by planId
router.get('/plan', VerifyToken, [
  check('planId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Download.selectByPlanId(req.query.planId, (err, results) => {
      if (err) throw err
      if (results.length === 1) {
        Download.getPlanXmlUrl(req.query.planId, (err, result) => {
          if (err) throw err
          if (result.length > 0) {
            res.json({
              type: 'success',
              msg: 'Got xml url successfully',
              result: result
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No plan xml exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No plan exist with this id'
        })
      }
    })
  })
})

module.exports = router

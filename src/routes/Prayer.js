const express = require('express')
const router = express.Router()
const Prayer = require('../models/Prayer')
const Updates = require('../models/Updates')
const _ = require('underscore')
const User = require('../models/User')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')
const { Validator } = require('node-input-validator');


// get prayer description by titleId
getPrdescdata = (gId, tId, uId, cms) => {
  return new Promise((resolve, reject) => {
    let arrData
    const params = {
      prGroupId: gId,
      prTitleId: tId,
      userId: uId
    }
    if(cms=='yes') {
      Prayer.getDescriptionForTitleByAdmin(params, (err, t) => {
        if (err) throw err
        t = (cms == 'yes') ? t : t.filter(x=>x.status=='Published');
        arrData = t
        resolve(arrData)
      })
    } else {
      Prayer.getDescriptionForTitle(params, (err, t) => {
        if (err) throw err
        t = (cms == 'yes') ? t : t.filter(x=>x.status=='Published');
        arrData = t
        resolve(arrData)
      })
    }
  })
}

// get prayer title by groupId
getPrtitdata = (gId, uId, cms) => {
  return new Promise((resolve, reject) => {
    const paramsT = {
      prGroupId: gId,
      userId: uId
    }
    Prayer.getTitleForGroup(paramsT, (err, tit) => {
      if (err) throw err
      const titleArray = []
      const promiseArray = []
      tit = (cms == 'yes') ? tit : tit.filter(x=>x.status=='Published');
      _.each(_.groupBy(tit, 'id'), title => {
        async function getData() {
          // call getPrdescdata func
          return await getPrdescdata(gId, title[0].prTitleId, uId, cms);
        }
        promiseArray.push(
          getData().then(async result=> {
            const titleData = {
              titleId: title[0].prTitleId,
              name: title[0].name,
              status: title[0].status,
              specificDate: title[0].specificDate,
              startDate: title[0].startDate,
              endDate: title[0].endDate,
              deleteAccess: title[0].deleteAccess,
              editedAt: cms=='yes' ? title[0].updatedOn : title[0].editedAt,
              createdAt: title[0].createdAt,
              prayerTitSpecificDateStatus: title[0].prayerTitSpecificDateStatus,
              description: await result
            }
            titleArray.push(titleData)
          })
        )
      })
      Promise.all(promiseArray).then(() => {
        // resolve(_.sortBy(Array.prototype.concat.apply([], titleArray), 'titleId'))
        // titleArray.sort((a, b) => b.createdAt - a.createdAt)
        resolve(titleArray.sort((a, b) => b.createdAt - a.createdAt))
      })
    })
  })
}

// get thankful prayer data by userId
getThankPrData = (uId) => {
  return new Promise((resolve, reject) => {
    let arrData
    Prayer.getAnsweredPrayerByuId(uId, (err, t) => {
      if (err) throw err
      arrData = t
      resolve(arrData)
    })
  })
}


// get prayer data
getPrayerData = (row, uId, cms) => {
  return new Promise(async (resolve, reject) => {
    const groupArray = []
    const promiseArray = []
    _.each(_.groupBy(row, 'id'), group => {
      // fetch title data by prayer groupId
      async function getData() {
        if(group[0].thankGroup==0) {
          // call getPrtitdata func
          return await getPrtitdata(group[0].prGroupId, uId, cms);
        } else {
          return await getThankPrData(uId);
        }
      }
      promiseArray.push(
        getData().then(async result=> {
          const prayerData = {
            groupId: group[0].prGroupId,
            name: group[0].name,
            status: group[0].status,
            groupImage: group[0].groupImage,
            specificDate: group[0].specificDate,
            startDate: group[0].startDate,
            endDate: group[0].endDate,
            adminAccess: group[0].adminAccess,
            editedAt: cms=='yes' ? group[0].updatedOn : group[0].editedAt,
            createdAt: group[0].createdAt,
            thankGroup: group[0].thankGroup,
            prayerGrpSpecificDateStatus: group[0].prayerGrpSpecificDateStatus
          }
          if(group[0].thankGroup==1) {
            prayerData.answeredPrayer = await result
          } else {
            prayerData.title = await result
          }
          groupArray.push(prayerData)
        })
      )
    })
    Promise.all(promiseArray).then(() => {
      // resolve(groupArray.sort((a, b) => b.adminAccess - a.adminAccess && b.createdAt - a.createdAt))
      resolve(_.sortBy(Array.prototype.concat.apply([], groupArray), 'createdAt'))
    })
  })
}


// update prayer group
const updatePrGrpPromise = (grp, uId) => {
  return new Promise(async (resolve, reject) => {
    const specData = {
      userId: uId,
      prGroupId: grp[0].groupId,
      startDate: grp[0].startDate,
      endDate: grp[0].endDate
    }
    Prayer.getPrGrpSpeciDate(specData, (err, specLen) => {
        if (err) throw err
        if (specLen[0].specificDate == 1) {
          Prayer.findspecificdateAlreadyExist(specLen[0].id, uId, (err, sdate) => {
            if (err) throw err
            if (sdate.length > 0) {
              delete specData.prGroupId
              specData.groupId = specLen[0].id
              Prayer.prGrpSpecDateUpdate(specData, (err, spresult) => {
                if (err) throw err
              })
            } else {
              const mapPayload1 = {
                userId: uId,
                groupId: specLen[0].id,
                startDate: grp[0].startDate,
                endDate: grp[0].endDate
              }
              Prayer.mapGroupSpecDate(mapPayload1, (err, mapResult1) => {
                if (err) throw err
              })
            }
          })
        } else {
            Prayer.findspecificdateAlreadyExist(specLen[0].id, uId, (err, sdate) => {
              if (err) throw err
              if (sdate.length > 0) {
                Prayer.deletePrGrpSpecificDate(sdate[0].id, (err, up) => {
                  if (err) throw err
                })
              }
            })
        }
    })
    resolve()
  })
}

// delete prayer group
const deletePrGrpPromise = (specData, specLen) => {
  return new Promise(async (resolve, reject) => {
    var contver = await getVersion(11)
    var dataVer = parseInt(contver.slice(5, 11)) + 1
    contver = contver.slice(0, 5) + '' + dataVer
    Updates.editUpdates(11, contver, function(err, rrr) {
      if (err) return res.status(500).send('Server error!')
      if (specLen[0].specificDate == 1) {
        Prayer.findspecificdateAlreadyExist(specLen[0].id, specData.userId, (err, sdate) => {
          if (err) throw err
          if (sdate.length > 0) {
            Prayer.deletePrGrpSpecificDate(sdate[0].id, (err, up) => {
              if (err) throw err
              Prayer.deleteGroup(specData, (err, result) => {
                if (err) throw err
              })
            })
          }
        })
      } else {
        Prayer.deleteGroup(specData, (err, result) => {
          if (err) throw err
        })
      }
    })
    resolve()
  })
}

// add, update & delete prayer group functionality
const cudPrayerGroup = (req, res, dataValid, uId, prGrp) => {
  return new Promise((resolve, reject) => {
    const rows = {errors: [], failedId: []}
    _.each(_.groupBy(prGrp, 'groupId'), grp => {
      const indx = prGrp.findIndex(x=>x.groupId==grp[0].groupId)
      const role = {
        userId: uId,
        admin: req.query.cms == 'yes' ? 1 : 0,
        superAdmin: req.query.cms == 'yes' ? 99 : 0
      }
      Prayer.checkGrpIdExist(role, async (err, gData) => {
          if (err) throw err
          // checking grp id exist or not
          var checkgrpId = gData.filter(x => x.prGroupId == grp[0].groupId)
          // if deleteAccess is 0, then check checkgrpId's length. if it's length is zero then add prayer group
          if (grp[0].deleteAccess == 0) {
              if (checkgrpId.length == 0) {
                  var v = new Validator(dataValid.group[indx], {
                    name: 'required',
                    specificDate: 'required|boolean',
                    createdAt: 'required|datetime'
                }, {
                  name: 'The group['+indx+'].name is required',
                  specificDate: 'The group['+indx+'].specificDate is required. It must be a boolean value',
                  createdAt: 'The group['+indx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                });
                  v.check().then((matched) => {
                    if (!matched) {
                      rows.errors.push(v.errors)
                      resolve(rows)
                        // res.status(422).send(v.errors);
                    } else {
                      grp[0].groupImage = (grp[0].groupImage == '' || grp[0].groupImage == null) ? 'https://twrradio.s3.amazonaws.com/images/1613570166199_prayer.png' : grp[0].groupImage;
                      const grpData = {
                        userId: uId,
                        prGroupId: grp[0].groupId,
                        name: grp[0].name,
                        groupImage: grp[0].groupImage,
                        specificDate: grp[0].specificDate,
                        createdAt: grp[0].createdAt,
                        editedAt: grp[0].createdAt
                      }
                      if (typeof (req.query.cms) === 'string') {
                        grpData.adminAccess = grp[0].adminAccess
                        grpData.status = grp[0].status
                      }
                      Prayer.addPrayerGroup(grpData, async (err, result) => {
                        if (err) throw err
                        if (result.insertId !== '') {
                          if (grp[0].specificDate == 1) {
                            const mapPayload = {
                              userId: uId,
                              groupId: result.insertId,
                              startDate: grp[0].startDate,
                              endDate: grp[0].endDate
                            }
                            Prayer.mapGroupSpecDate(mapPayload, (err, mapResult) => {
                              if (err) throw err
                            })
                          }
                        }
                      })
                    }
                  })
              } else {
                  var v = new Validator(dataValid.group[indx], {
                      name: 'required',
                      specificDate: 'required|boolean',
                      editedAt: 'required|datetime'
                  }, {
                    name: 'The group['+indx+'].name is required',
                    specificDate: 'The group['+indx+'].specificDate is required. It must be a boolean value',
                    editedAt: 'The group['+indx+'].editedAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                  });
                  v.check().then((matched) => {
                    if (!matched) {
                      rows.errors.push(v.errors)
                      resolve(rows)
                    } else {
                      grp[0].groupImage = (grp[0].groupImage == '' || grp[0].groupImage == null) ? 'https://twrradio.s3.amazonaws.com/images/1613570166199_prayer.png' : grp[0].groupImage;
                      const grpupdateData = {
                        userId: uId,
                        prGroupId: grp[0].groupId,
                        name: grp[0].name,
                        groupImage: grp[0].groupImage,
                        specificDate: grp[0].specificDate,
                        editedAt: grp[0].editedAt
                      }
                      if (typeof (req.query.cms) === 'string') {
                        grpupdateData.adminAccess = grp[0].adminAccess
                        grpupdateData.status = grp[0].status
                      }
                      Prayer.updateGroup(grpupdateData, async (err, uresult) => {
                          if (err) throw err
                          await updatePrGrpPromise(grp, uId)
                      })
                    }
                  })
              }
          } else if (checkgrpId.length == 1 && grp[0].deleteAccess == 1) {
                const specData = {
                  userId: uId,
                  prGroupId: grp[0].groupId,
                  admin: (req.query.cms == 'yes') ? 1 : 0,
                  superAdmin: (req.query.cms == 'yes') ? 99 : 0
                }
                Prayer.getPrGrpSpeciDate(specData, async (err, specLen) => {
                  if (err) throw err
                  await deletePrGrpPromise(specData, specLen)
                })
          } else {
            rows.failedId.push(grp[0].groupId)
          }
      })
    })
    resolve(rows)
  })
}


// update prayer title
const updatePrTitPromise = (req, grp, gId, uId) => {
  return new Promise(async (resolve, reject) => {
      const specData = {
        userId: uId,
        prGroupId: gId,
        prTitleId: grp.titleId,
        startDate: grp.startDate,
        endDate: grp.endDate,
        admin: req.query.cms == 'yes' ? 1 : 0,
        superAdmin: req.query.cms == 'yes' ? 99 : 0
      }
      Prayer.getPrTitSpeciDate(specData, (err, specLen) => {
          if (err) throw err
          if (specLen[0].specificDate == 1) {
            Prayer.findspecificdateForTitleAlreadyExist(specLen[0].id, (err, sdate) => {
              if (err) throw err
              if (sdate.length > 0) {
                delete specData.userId
                delete specData.prGroupId
                delete specData.prTitleId
                delete specData.admin
                delete specData.superAdmin
                specData.titleId = specLen[0].id
                Prayer.prTitSpecDateUpdate(specData, (err, spresult) => {
                  if (err) throw err
                })
              } else {
                const mapPayload1 = {
                  titleId: specLen[0].id,
                  startDate: grp.startDate,
                  endDate: grp.endDate
                }
                Prayer.mapTitleSpecDate(mapPayload1, (err, mapResult1) => {
                  if (err) throw err
                })
              }
            })
          } else {
              Prayer.findspecificdateForTitleAlreadyExist(specLen[0].id, (err, sdate) => {
                  if (err) throw err
                  if (sdate.length > 0) {
                      Prayer.deletePrTitSpecificDate(specLen[0].id, (err, up) => {
                        if (err) throw err
                      })
                  }
              })
          }
      })
    resolve()
  })
}

// delete prayer title
const deletePrTitPromise = (specData, specLen) => {
  return new Promise(async (resolve, reject) => {
    if (specLen[0].specificDate == 1) {
      Prayer.findspecificdateForTitleAlreadyExist(specLen[0].id, (err, sdate) => {
        if (err) throw err
        if (sdate.length > 0) {
          Prayer.deletePrTitSpecificDate(specLen[0].id, (err, up) => {
            if (err) throw err
            Prayer.deleteTitle(specData, (err, result) => {
              if (err) throw err
            })
          })
        }
      })
    } else {
      Prayer.deleteTitle(specData, (err, result) => {
        if (err) throw err
      })
    }
    resolve()
  })
}


// add, update & delete prayer title functionality
const cudPrayerTitle = (req, res, dataValid, uId, prTit) => {
  return new Promise((resolve, reject) => {
    const rows = {errors: [], failedId: []}
    _.each(prTit, grp => {
      const role = {
        admin: req.query.cms == 'yes' ? 1 : 0,
        superAdmin: req.query.cms == 'yes' ? 99 : 0,
        titleId: grp.titleId
      }
      Prayer.checkUserIdGrpIdExist(uId, grp.groupId, role, async (err, gData) => {
        if (err) throw err
        if(gData.length == 1) {
          Prayer.checkTitIdExist(uId, grp.groupId, role, async (err, tData) => {
              if (err) throw err
              // checking tit id exist or not
              const titindx = prTit.findIndex(x=>x.titleId==grp.titleId && x.groupId==grp.groupId)
              var checktitId = tData.filter(x => x.prTitleId == grp.titleId)
              // if deleteAccess is 0, then check checktitId's length. if it's length is zero then add prayer title
              if (grp.deleteAccess == 0) {
                if (checktitId.length == 0) {
                    v = new Validator(dataValid.title[titindx], {
                      name: 'required',
                      specificDate: 'required|boolean',
                      createdAt: 'required|datetime'
                    }, {
                      name: 'The title['+titindx+'].name is required',
                      specificDate: 'The title['+titindx+'].specificDate is required. It must be a boolean value',
                      createdAt: 'The title['+titindx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                    });
                    v.check().then((matched) => {
                      if (!matched) {
                        rows.errors.push(v.errors)
                        resolve(rows)
                      } else {
                        const titData = {
                          userId: uId,
                          prGroupId: grp.groupId,
                          prTitleId: grp.titleId,
                          name: grp.name,
                          specificDate: grp.specificDate,
                          createdAt: grp.createdAt,
                          editedAt: grp.createdAt
                        }
                        if (typeof (req.query.cms) === 'string') {
                          titData.adminAccess = grp.adminAccess
                          titData.status = grp.status
                        }
                        Prayer.addPrayerTitle(titData, async (err, result) => {
                          if (err) throw err
                          if (result.insertId !== '') {
                            if (grp.specificDate == 1) {
                              const mapPayload = {
                                titleId: result.insertId,
                                startDate: grp.startDate,
                                endDate: grp.endDate
                              }
                              Prayer.mapTitleSpecDate(mapPayload, (err, mapResult) => {
                                if (err) throw err
                              })
                            }
                          }
                        })
                      }
                    })
                } else {
                  v = new Validator(dataValid.title[titindx], {
                    name: 'required',
                    specificDate: 'required|boolean',
                    editedAt: 'required|datetime'
                  }, {
                    name: 'The title['+titindx+'].name is required',
                    specificDate: 'The title['+titindx+'].specificDate is required. It must be a boolean value',
                    editedAt: 'The title['+titindx+'].editedAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                  });
                  v.check().then((matched) => {
                    if (!matched) {
                      rows.errors.push(v.errors)
                      resolve(rows)
                    } else {
                      // if checktitId's length is 1 then update prayer title
                      const titupdateData = {
                        userId: uId,
                        prGroupId: grp.groupId,
                        prTitleId: grp.titleId,
                        name: grp.name,
                        specificDate: grp.specificDate,
                        editedAt: grp.editedAt
                      }
                      if (typeof (req.query.cms) === 'string') {
                        titupdateData.adminAccess = grp.adminAccess
                        titupdateData.status = grp.status
                      }
                      Prayer.updateTitle(titupdateData, async (err, uresult) => {
                          if (err) throw err
                          await updatePrTitPromise(req, grp, grp.groupId, uId)
                      })
                    }
                  })
                }
              } else if (checktitId.length == 1 && grp.deleteAccess == 1) {
                const specData = {
                  userId: uId,
                  prGroupId: grp.groupId,
                  prTitleId: grp.titleId,
                  admin: req.query.cms == 'yes' ? 1 : 0,
                  superAdmin: req.query.cms == 'yes' ? 99 : 0
                }
                Prayer.getPrTitSpeciDate(specData, async (err, specLen) => {
                  if (err) throw err
                  await deletePrTitPromise(specData, specLen)
                })
              } else {
                rows.failedId.push({
                  groupId: grp.groupId,
                  titleId: grp.titleId
                })
              }
          })
        } else {
          rows.failedId.push({
            groupId: grp.groupId,
            titleId: ''
          })
        }
      })
    })
      resolve(rows)
  })
}


// add, update & delete prayer description functionality
const cudPrayerDescription = (req, res, dataValid, uId, prDesc) => {
  return new Promise((resolve, reject) => {
    const rows = {errors: [], failedId: []}
    _.each(prDesc, desc => {
      const role = {
        admin: req.query.cms == 'yes' ? 1 : 0,
        superAdmin: req.query.cms == 'yes' ? 99 : 0,
        titleId: desc.titleId
      }
      Prayer.checkUserIdGrpIdExist(uId, desc.groupId, role, async (err, gData) => {
        if (err) throw err
        if(gData.length == 1) {
          Prayer.checkTitIdExist(uId, desc.groupId, role, async (err, tData) => {
            if (err) throw err
            if(tData.length == 1) {
              Prayer.checkDescIdExist(uId, desc.groupId, desc.titleId, role, async (err, dData) => {
               if (err) throw err
                // checking desc id exist or not
                var checkdescId = dData.filter(x => x.prDescriptionId == desc.descriptionId && x.prTitleId == desc.titleId && x.prGroupId == desc.groupId)
                var descInx = prDesc.findIndex(x => x.descriptionId == desc.descriptionId && x.titleId == desc.titleId && x.groupId == desc.groupId)
                // if deleteAccess is 0, then check checkdescId's length. if it's length is zero then add prayer title
                if (desc.deleteAccess == 0) {
                  if (checkdescId.length == 0) {
                      v = new Validator(dataValid.description[descInx], {
                        name: 'required',
                        createdAt: 'required|datetime'
                      }, {
                        name: 'The description['+descInx+'].name is required',
                        createdAt: 'The description['+descInx+'].createdAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                      });
                      v.check().then((matched) => {
                        if (!matched) {
                          rows.errors.push(v.errors)
                          resolve(rows)
                        } else {
                          const descData = {
                            userId: uId,
                            prGroupId: desc.groupId,
                            prTitleId: desc.titleId,
                            prDescriptionId: desc.descriptionId,
                            name: desc.name,
                            createdAt: desc.createdAt,
                            editedAt: desc.createdAt
                          }
                          if (typeof (req.query.cms) === 'string') {
                            descData.adminAccess = desc.adminAccess
                            descData.status = desc.status
                          }
                          Prayer.addPrayerDescription(descData, async (err, result) => {
                            if (err) throw err
                          })
                        }
                      })
                  } else {
                    // if checkdescId's length is 1 then update prayer description
                    v = new Validator(dataValid.description[descInx], {
                      name: 'required',
                      editedAt: 'required|datetime'
                    }, {
                      name: 'The description['+descInx+'].name is required',
                      editedAt: 'The description['+descInx+'].editedAt must be a valid datetime(YYYY-MM-DD HH:mm:ss).'
                    });
                    v.check().then((matched) => {
                      if (!matched) {
                        rows.errors.push(v.errors)
                        resolve(rows)
                      } else {
                        const descupdateData = {
                          userId: uId,
                          prGroupId: desc.groupId,
                          prTitleId: desc.titleId,
                          prDescriptionId: desc.descriptionId,
                          name: desc.name,
                          editedAt: desc.editedAt
                        }
                        if (typeof (req.query.cms) === 'string') {
                          descupdateData.adminAccess = desc.adminAccess
                          descupdateData.status = desc.status
                        }
                        Prayer.updateDescription(descupdateData, async (err, uresult) => {
                          if (err) throw err
                        })
                      }
                    })
                  }
                } else if (checkdescId.length == 1 && desc.deleteAccess == 1) {
                  const specData = {
                    userId: uId,
                    prGroupId: desc.groupId,
                    prTitleId: desc.titleId,
                    prDescriptionId: desc.descriptionId
                  }
                  Prayer.getPrDescSpeciDate(specData, async (err, specLen) => {
                    if (err) throw err
                    Prayer.deleteDescription(specData, (err, result) => {
                      if (err) throw err
                    })
                  })
                } else {
                  rows.failedId.push({
                    groupId: desc.groupId,
                    titleId: desc.titleId,
                    descriptionId: desc.descriptionId
                  })
                }
             })
            } else {
              rows.failedId.push({
                groupId: desc.groupId,
                titleId: desc.titleId,
                descriptionId: ''
              })
            }
          })
        } else {
          rows.failedId.push({
            groupId: desc.groupId,
            titleId: '',
            descriptionId: ''
          })
        }
      })
    })
    resolve(rows)
  })
}


// update answered prayer functionality
const updateAnswered = (res, uId, answeredPrayer) => {
  return new Promise((resolve, reject) => {
    _.each(_.groupBy(answeredPrayer, 'descriptionId'), ans => {
      const data = {
        userId: uId,
        descriptionId: ans[0].descriptionId,
        answeredat: ans[0].answeredat
      }
      Prayer.requestAnswered(data, async (err, result) => {
        if (err) throw err
        var contver = await getVersion(11)
        var dataVer = parseInt(contver.slice(5, 11)) + 1
        contver = contver.slice(0, 5) + '' + dataVer
        Updates.editUpdates(11, contver, function (err, rrr) {
          if (err) return res.status(500).send('Server error!')
        })
      })
    })
    resolve()
  })
}

// update answered prayer functionality for admin
const updateAnsweredForAdmin = (res, uId, answeredPrayer) => {
  return new Promise((resolve, reject) => {
    _.each(_.groupBy(answeredPrayer, 'descriptionId'), ans => {
      const data = {
        userId: uId,
        groupId: ans[0].groupId,
        titleId: ans[0].titleId,
        descriptionId: ans[0].descriptionId,
        answeredat: ans[0].answeredat
      }
      Prayer.requestAnsweredForAdmin(data, async (err, result) => {
        if (err) throw err
        var contver = await getVersion(11)
        var dataVer = parseInt(contver.slice(5, 11)) + 1
        contver = contver.slice(0, 5) + '' + dataVer
        Updates.editUpdates(11, contver, function (err, rrr) {
          if (err) return res.status(500).send('Server error!')
        })
      })
    })
    resolve()
  })
}


// add/update/delete a prayer group
router.post('/group', VerifyToken, function(req, res, next) {
  var v = new Validator(req.body, {
      'userId': 'required|integer',
      'group': 'required|array',
      'group.*.groupId': 'required',
      'group.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
      if (!matched) {
          res.status(422).send(v.errors);
      } else {
          User.selectById(req.userId, function(err, user) {
              if (err) return res.status(500).send('There was a problem finding the user.')
              if (!user) return res.status(404).send('No user found.')
              User.selectById(req.body.userId, async (err, results) => {
                  if (err) throw err
                  if (results.length > 0) {
                    const resp = await cudPrayerGroup(req, res, req.body, req.body.userId, req.body.group)
                    // add updates in update table
                    var contver = await getVersion(11)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(11, contver, function (err, row) {
                      if (err) return res.status(500).send('Server error!')
                      if(resp.errors.length == 0 && resp.failedId.length == 0) {
                        res.json({
                          type: 'success',
                          msg: 'Successfully synced'
                        })
                      } else {
                        res.json({
                          type: 'failure',
                          msg: 'Not success',
                          rows: resp
                        })
                      }
                    })
                  } else {
                      res.json({
                          type: 'failure',
                          msg: 'No User exist with this user id'
                      })
                  }
              })
          })
      }
  })
})

// add/update/delete prayer title
router.post('/title', VerifyToken, function(req, res, next) {
  var v = new Validator(req.body, {
      'userId': 'required|integer',
      'title': 'required|array',
      'title.*.groupId': 'required',
      'title.*.titleId': 'required',
      'title.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
      if (!matched) {
          res.status(422).send(v.errors);
      } else {
          User.selectById(req.userId, function(err, user) {
              if (err) return res.status(500).send('There was a problem finding the user.')
              if (!user) return res.status(404).send('No user found.')
              User.selectById(req.body.userId, async (err, results) => {
                  if (err) throw err
                  if (results.length > 0) {
                    const resp = await cudPrayerTitle(req, res, req.body, req.body.userId, req.body.title)
                    // add updates in update table
                    var contver = await getVersion(11)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(11, contver, function (err, row) {
                      if (err) return res.status(500).send('Server error!')
                      if(resp.errors.length == 0 && resp.failedId.length == 0) {
                        res.json({
                          type: 'success',
                          msg: 'Successfully synced'
                        })
                      } else {
                        res.json({
                          type: 'failure',
                          msg: 'Not success',
                          rows: resp
                        })
                      }
                    })
                  } else {
                      res.json({
                          type: 'failure',
                          msg: 'No User exist with this user id'
                      })
                  }
              })
          })
      }
  })
})

// add/update/delete prayer description
router.post('/description', VerifyToken, function(req, res, next) {
  var v = new Validator(req.body, {
      'userId': 'required|integer',
      'description': 'required|array',
      'description.*.groupId': 'required',
      'description.*.titleId': 'required',
      'description.*.descriptionId': 'required',
      'description.*.deleteAccess': 'required|boolean'
  })
  v.check().then((matched) => {
      if (!matched) {
          res.status(422).send(v.errors);
      } else {
          User.selectById(req.userId, function(err, user) {
              if (err) return res.status(500).send('There was a problem finding the user.')
              if (!user) return res.status(404).send('No user found.')
              User.selectById(req.body.userId, async (err, results) => {
                  if (err) throw err
                  if (results.length > 0) {
                    const resp = await cudPrayerDescription(req, res, req.body, req.body.userId, req.body.description)
                    // add updates in update table
                    var contver = await getVersion(11)
                    var dataVer = parseInt(contver.slice(5, 11)) + 1
                    contver = contver.slice(0, 5) + '' + dataVer
                    Updates.editUpdates(11, contver, function (err, row) {
                      if (err) return res.status(500).send('Server error!')
                      if(resp.errors.length == 0 && resp.failedId.length == 0) {
                        res.json({
                          type: 'success',
                          msg: 'Successfully synced'
                        })
                      } else {
                        res.json({
                          type: 'failure',
                          msg: 'Not success',
                          rows: resp
                        })
                      }
                    })
                  } else {
                      res.json({
                          type: 'failure',
                          msg: 'No User exist with this user id'
                      })
                  }
              })
          })
      }
  })
})


// get prayer group by userId
router.get('/group', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectById(req.query.userId, (err, results) => {
      if (err) throw err
      var cms = (req.query.cms == 'yes') ? req.query.cms : 'no';
      if (results.length === 1) {
        Prayer.getGroupForAUser(req.query.userId, async (err, result) => {
          if (err) throw err
          result = (cms == 'yes') ? result : result.filter(x=>x.status=='Published');
          var rows = await getPrayerData(result, req.query.userId, cms)
          res.json({
            type: 'success',
            msg: 'Got Prayer group details based on userId successfully',
            rows
          })
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// post answered status
router.put('/answered', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('answeredPrayer.*.descriptionId').notEmpty().withMessage('Must not be empty'),
  check('answeredPrayer.*.answeredat').isISO8601().toDate().withMessage('Must be in correct format yyyy-mm-dd hh:mm:ss ')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const ans = []
    _.each(req.body.answeredPrayer, a => {
      ans.push(a.descriptionId)
    })
    Prayer.selectDescriptionById(req.body.userId, ans, async (err, results) => {
      if (err) throw err
      if (results.length == ans.length) {
        const resp = await updateAnswered(res, req.body.userId, req.body.answeredPrayer)
        res.json({
          type: 'success',
          msg: 'Updated successfully'
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'Some Description Id not exist.'
        })
      }
    })
  })
})

// post answered status for admin
router.put('/admin/answered', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('answeredPrayer.*.groupId').notEmpty().withMessage('Must not be empty'),
  check('answeredPrayer.*.titleId').notEmpty().withMessage('Must not be empty'),
  check('answeredPrayer.*.descriptionId').notEmpty().withMessage('Must not be empty'),
  check('answeredPrayer.*.answeredat').isISO8601().toDate().withMessage('Must be in correct format yyyy-mm-dd hh:mm:ss ')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const desc = []
    _.each(req.body.answeredPrayer, a => {
      desc.push(a.descriptionId)
    })
    const title = []
    _.each(req.body.answeredPrayer, a => {
      title.push(a.titleId)
    })
    const group = []
    _.each(req.body.answeredPrayer, a => {
      group.push(a.groupId)
    })
    const data = {
      group: group,
      title: title,
      desc: desc
    }
    Prayer.selectDescriptionByIdForAdmin(req.body.userId, data, async (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        const resp = await updateAnsweredForAdmin(res, req.body.userId, req.body.answeredPrayer)
        res.json({
          type: 'success',
          msg: 'Updated successfully'
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'Some Description Id not exist.'
        })
      }
    })
  })
})

module.exports = router

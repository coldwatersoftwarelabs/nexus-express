const express = require('express')
const router = express.Router()
const PlanDetails = require('../models/PlanDetails')
const _ = require('underscore')
const async = require('async')
const xml = require('../../xml')
const User = require('../models/User')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')
var VerifyToken = require('./VerifyToken')

const processVerse = (data) => {
  const verses = []
  return new Promise((resolve, reject) => {
    _.each(_.groupBy(data, 'name'), book => {
      _.each(_.groupBy(book, 'chapterId'), chapter => {
        const verseArr = []
        _.each(chapter, c1 => {
          verseArr.push({
            verseId: c1.verseId,
            verse: c1.verse
          })
        })
        verses.push({
          verses: verseArr,
          chapter: book[0].chapterId,
          book: book[0].name
        })
      })
    })
    resolve(verses)
  })
}

const fetchReadingPlanPerModule = (planModId) => {
  return new Promise((resolve, reject) => {
    PlanDetails.getReadingPlanDetail(planModId, (err, result) => {
      if (err) throw err
      if (result.length) {
        if (result[0].fromBook === result[0].toBook) {
          if (result[0].fromChapter === result[0].toChapter) {
            const req = {
              bookId: result[0].fromBook,
              chapterId: result[0].fromChapter,
              fromVerse: result[0].fromVerse,
              toVerse: result[0].toVerse
            }
            PlanDetails.getVerseFromSameChapter(req, async (err, verse) => {
              if (err) throw err
              const verseList = await processVerse(verse)
              resolve(verseList)
            })
          } else {
            PlanDetails.getVerseFromDiffChapter(result[0], async (err, verse) => {
              if (err) throw err
              const verseList = await processVerse(verse)
              resolve(verseList)
            })
          }
        } else {
          PlanDetails.getVerseFromDiffChapter(result[0], async (err, verse) => {
            if (err) throw err
            const verseList = await processVerse(verse)
            resolve(verseList)
          })
        }
      } else {
        resolve()
      }
    })
  })
}

const getPlanreadingPlanPerPlan = (id) => {
  return new Promise((resolve, reject) => {
    const verseArr = []
    PlanDetails.getReadingPlanPerPlan(id, (err, result) => {
      if (err) throw err
      if (result.length) {
        async.map(result, function (r, callback) {
          fetchReadingPlanPerModule(r.id).then(data => {
            const key = r.id
            callback(null, {
              planModule: r.planModuleId,
              readingPlan: data
            })
          })
        }, function (err, results) {
          resolve({
            verses: results,
            plan: result[0].name
          })
        })
      } else {
        resolve([])
      }
    })
  })
}

const readingList = (readings) => {
  return new Promise(async (resolve, reject) => {
    const planData = []
    _.each(_.groupBy(readings, 'planId'), async reading => {
      const readData = {
        planId: reading[0].planId,
        name: reading[0].name,
        planduration: reading[0].planduration,
        makeupday: reading[0].makeupday,
        readingsperday: reading[0].readingsperday,
        byDate: reading[0].byDate,
        completed: reading[0].completed,
        xmlurl: reading[0].xmlurl,
        readingPlans: []
      }
      _.each(reading, red => {
        readData.readingPlans.push(red)
      })
      planData.push(readData)
    })
    resolve(planData)
  })
}

// create plans
router.post('/', VerifyToken, [
  check('plan.readingsperday').isNumeric().withMessage('Must be only numbers'),
  check('plan.planduration').notEmpty().withMessage('Should not be empty'),
  check('plan.name').notEmpty().withMessage('Plan name should not be empty'),
  check('plan.status').notEmpty().withMessage('Status should not be empty'),
  check('plan.tblName').notEmpty().withMessage('tblName should not be empty'),
  check('plan.ismakeupday').isNumeric().withMessage('Must be only numbers'),
  check('plan.byDate').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.addPlan(req.body.plan, (err, result) => {
      if (err) throw err
      res.json({
        planId: result.insertId
      })
    })
  })
})

// update plan
router.put('/', VerifyToken, [
  check('plan.readingsperday').isNumeric().withMessage('Must be only numbers'),
  check('plan.id').isNumeric().withMessage('Must be only numbers'),
  check('plan.planduration').notEmpty().withMessage('Should not be empty'),
  check('plan.name').notEmpty().withMessage('Plan name should not be empty'),
  check('plan.status').notEmpty().withMessage('Status should not be empty'),
  check('plan.tblName').notEmpty().withMessage('tblName should not be empty'),
  check('plan.ismakeupday').isNumeric().withMessage('Must be only numbers'),
  check('plan.byDate').isNumeric().withMessage('Must be only numbers'),
  check('userId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user and plan existence
    const userCount = await userCheck(req.body.userId)
    const planCount = await planCheck(req.body.plan.id)
    if ((userCount > 0) && (planCount > 0)) {
      PlanDetails.editPlan(req.body.plan, (err, result) => {
        if (err) throw err
        res.json({
          status: 'Updated sccessfully!'
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// delete plan
router.post('/deleteplan', VerifyToken, [
  check('id').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check plan existence
    const planCount = await planCheck(req.body.id)
    if ((planCount > 0)) {
      PlanDetails.deletePlan(req.body.id, (err, result) => {
        if (err) throw err
        res.json({
          status: 'Deleted sccessfully!'
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// list all plans
router.get('/', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.listPlan('', (err, result) => {
      if (err) throw err
      const rows = [];
      _.each(_.groupBy(result, 'id'), c => {
        const planData = {
          id: c[0].id,
          name: c[0].name,
          xmlurl: c[0].xmlurl, 
          tblName: c[0].tblName,
          byDate: c[0].byDate,
          contributors: c[0].contributors,
          coverage: c[0].coverage,
          createdAt: c[0].createdAt,
          creator: c[0].creator,
          deleted: c[0].deleted,
          description: c[0].description,
          editedAt: c[0].editedAt,
          identifier: c[0].identifier,
          ismakeupday: c[0].ismakeupday,
          makeupday: c[0].makeupday,
          planduration: c[0].planduration,
          publisher: c[0].publisher,
          readingsperday: c[0].readingsperday,
          rights: c[0].rights,
          source: c[0].source,
          status: c[0].status,
          subject: c[0].subject,
          type: c[0].type,
          updatedOn: c[0].updatedOn
        }
        if(typeof (req.query.cms === 'string')) {
          planData.userCount = c[0].userCount
        }
        rows.push(planData);
      })
      res.json(rows)
    })
  })
})

// get xml generated plans
router.get('/xmlGenerated', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    const userCount = await userCheck(req.query.userId)
    if ((userCount > 0)) {
      PlanDetails.listXmlPlans(req.query.userId, (err, result) => {
        if (err) throw err
        res.json(result)
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

// update download status if plan downloaded
router.put('/downloadstatus', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('planId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check plan existence
        PlanDetails.fetchPlan(req.body.planId, (err, row) => {
          if (err) throw err
          if (row.length > 0) {
            // update download status to 2
            PlanDetails.updateDownloadStatus(req.body, (err, result) => {
              if (err) throw err
              if (result.affectedRows === 1) {
                res.json({
                  type: 'success',
                  status: 'Updated sccessfully!'
                })
              }
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No plan exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// update download status if plan selected
router.put('/selectedplan', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('planId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check plan existence
        PlanDetails.fetchPlan(req.body.planId, (err, row) => {
          if (err) throw err
          if (row.length > 0) {
            PlanDetails.updateAlreadySelectedPlan(req.body, (err, re) => {
              if (err) throw err
              // update download status to 3
              PlanDetails.updateDownloadStatus(req.body, (err, result) => {
                if (err) throw err
                res.json({
                  type: 'success',
                  status: 'Updated sccessfully!'
                })
              })
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No plan exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

router.get('/book/list', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getBooksList('', (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

router.get('/chapter/list', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getChaptersList(req.query.bookId, (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

router.get('/verse/list', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getVerseList(req.query, (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

// create readings for the plan
router.post('/readingPlan/', VerifyToken, [
  check('planDetail.planId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check plan existence
    const planCount = await planCheck(req.body.planDetail.planId)
    if ((planCount > 0)) {
      // add plan modules
      PlanDetails.addPlanModule(req.body.planDetail, (err, result) => {
        if (err) throw err
        const planArray = []
        _.each(req.body.readingPlan, plan => {
          planArray.push([result.insertId, plan.fromBook, plan.toBook, plan.fromChapter,
            plan.toChapter, plan.fromVerse, plan.toVerse
          ])
        })
        // add plan readings
        PlanDetails.addReadingPlan(planArray, (err, resultdata) => {
          if (err) throw err
          res.json({
            readingPlanId: result.insertId
          })
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

router.get('/readingPlan/list', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.ReadingPlanList(req.query.planId, (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

// load all bible
router.get('/allbible', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.GetWholeBible('', (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

router.get('/distinctchapter', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.GetDistinctBible('', (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

// get all readings based on planId
router.get('/allReading/:planId?', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getReadingByPlanId(req.query.planId, (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        let planArray = {}
        let planName = ''
        // get readings as per day or date
        planArray = _.map(_.groupBy(result, 'dayOrDate'), plan => {
          const planArr = []
          const DayOrDate = ((result[0].byDate) === 0)
            ? 'Day' : 'Date'
          const dayTag = `${DayOrDate}${plan[0].dayOrDate}`
          planName = plan[0].name
          _.each(plan, data => {
            planArr.push({
              Module: {
                readingId: data.readingId,
                planModuleId: data.planModuleId,
                fromChapter: data.fromChapter,
                toChapter: data.toChapter,
                fromVerse: data.fromVerse,
                toVerse: data.toVerse,
                fromBook: data.fromBook,
                toBook: data.toBook
              }
            })
          })
          const obj = {
            dayOrDate: plan[0].dayOrDate,
            plan: planArr
          }

          return obj
        })
        const resultSet = {
          planDetail: {
            name: result[0].name,
            makeUpDay: result[0].makeUpDay,
            planduration: result[0].planduration,
            readingsperday: result[0].readingsperday
          },
          readingPlan: planArray
        }
        res.json(resultSet)
      } else {
        res.json([])
      }
    })
  })
})

// list read status by user Id
router.get('/userReadings/:userId?', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getReadingByUserId(req.query.userId, async (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        const userReadings = await readingList(result)
        res.json(userReadings)
      } else {
        res.json([])
      }
    })
  })
})

router.get('/readingPlan/:planId', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getPlanModDetails(req.query.planId, (err, result) => {
      if (err) throw err
      const planArray = []
      _.each(result, planData => {
        planArray.push({
          readingPlanId: planData.id,
          fromBook: planData.fromBook,
          toBook: planData.toBook,
          fromChapter: planData.fromChapter,
          toChapter: planData.toChapter,
          fromVerse: planData.fromVerse,
          toVerse: planData.toVerse
        })
      })
      const resultSet = {
        planDetail: {
          startDate: result[0].startDate,
          makeUpDay: result[0].makeUpDay,
          noOfReading: result[0].noOfReading,
          id: result[0].id
        },
        readingPlan: planArray
      }
      res.json(resultSet)
    })
  })
})

router.delete('/readingPlan/', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.deletePlanMod(req.body.planModId, (err, result) => {
      if (err) throw err
      res.json({
        status: 'Deleted successfully!'
      })
    })
  })
})

// edit reading plan
router.put('/readingPlan/', VerifyToken, [
  check('planDetail.id').isNumeric().withMessage('Must be only numbers'),
  check('planDetail.planId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check plan and plan module
    const planCount = await planCheck(req.body.planDetail.planId)
    const planModCount = await planModCheck(req.body.planDetail.id)
    if ((planCount > 0) && (planModCount > 0)) {
      // update plan module
      PlanDetails.updatePlanMod(req.body.planDetail, (err, result) => {
        if (err) throw err
        if (req.body.addNewPlan) {
          const planArray = []
          _.each(req.body.addNewPlan, plan => {
            planArray.push([req.body.planDetail.id, plan.fromBook, plan.toBook, plan.fromChapter,
              plan.toChapter, plan.fromVerse, plan.toVerse
            ])
          })
          // add new plan readings
          PlanDetails.addReadingPlan(planArray, (err, resultdata1) => {
            if (err) throw err
          })
        }

        if (req.body.update) {
          _.each(req.body.update, updateRecord => {
            // update plan readings
            PlanDetails.updateReadinPlan(updateRecord, (err, resultdata2) => {
              if (err) throw err
            })
          })
        }
        res.json({
          status: 'Updated successfully!'
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

router.get('/readingPlan/list/versePerReadingPlan', async (req, res) => {
  const verses = await fetchReadingPlanPerModule(req.query.readingPlanId)
  res.json({
    verses
  })
})

router.get('/readingPlan/list/booksPerModule', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    PlanDetails.getReadingPlanBooks(req.query.planModId, (err, result) => {
      if (err) throw err
      res.json(result)
    })
  })
})

router.get('/readingPlan/list/VersePerPlan', VerifyToken, (req, res) => {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    getPlanreadingPlanPerPlan(req.query.planId).then(data => {
      xml.generateXML(data).then(result => {
        res.json(result)
      })
    })
  })
})

// map plan to user
router.post('/mapPlanToUser', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('planId').isNumeric().withMessage('Must be only numbers'),
  check('downloadStatus').notEmpty().withMessage('Should not be empty')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user existence
    User.selectById(req.body.userId, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        // check plan existence
        PlanDetails.getReadingPlans(req.body.planId, (err, readingPlans) => {
          if (err) throw err
          if (readingPlans.length > 0) {
            // check plan mapped or not
            PlanDetails.checkPlanMappedorNot(req.body, (err, maped) => {
              if (err) throw err
              if (maped.length === 0) {
                // map plan to user
                PlanDetails.addPlanToUser(req.body, (err, response) => {
                  if (err) throw err
                })
                const readingPlanArray = []
                _.each(readingPlans, plan => {
                  readingPlanArray.push([req.body.planId, plan.readingPlanId, req.body.userId])
                })
                // map readings to user
                PlanDetails.addReadingPlanToUser(readingPlanArray, (err, response) => {
                  if (err) throw err
                  res.json({
                    type: 'success',
                    msg: "Added plan to user's list! downloading"
                  })
                })
              } else {
                res.json({
                  type: 'success',
                  msg: "Added plan to user's list! downloading"
                })
              }
            })
          } else {
            res.status(200).send({
              type: 'failure',
              msg: 'No plan exist with this id'
            })
          }
        })
      } else {
        res.status(200).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// mark plan read status
router.post('/markPlanRead', VerifyToken, [
  check('userId').isNumeric().withMessage('Must be only numbers'),
  check('planId').isNumeric().withMessage('Must be only numbers'),
  check('readingPlanId').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    // check user, plan and readings existence
    const userCount = await userCheck(req.body.userId)
    const planCount = await planxmlCheck(req.body.planId)
    const readingCount = await readingCheck(req.body.readingPlanId)
    if ((userCount > 0) && (planCount > 0) && (readingCount > 0)) {
      // mark reading plan as read
      PlanDetails.markReadingPlanAsRead(req.body, (err, response) => {
        if (err) throw err
        PlanDetails.getPlanFromReadingPlan(req.body.readingPlanId, (err, response) => {
          if (err) throw err
          if (response.length) {
            PlanDetails.getReadingPlanStatus({
              planId: response[0].id,
              userId: req.body.userId
            }, (err, response2) => {
              if (err) throw err
              if (!response2.length) {
                // mark plan as read
                PlanDetails.markPlanasRead({
                  userId: req.body.userId,
                  planId: response[0].id
                }, (err, result => {
                  if (err) throw err
                  res.json('Updated status!')
                }))
              } else {
                res.json('Updated status!')
              }
            })
          }
        })
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const Testimony = require('../models/Testimony')
const {
  check,
  checkSchema,
  validationResult
} = require('express-validator')
const User = require('../models/User')
var VerifyToken = require('./VerifyToken')

// add testimony
router.post('/', VerifyToken, [
  check('testimonyBy').isNumeric().withMessage('Must be only numbers')
], function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    User.selectById(req.body.testimonyBy, (err, results) => {
      if (err) throw err
      if (results.length > 0) {
        Testimony.addTestimony(req.body, function (err, result) {
          if (err) throw err
          res.json({
            type: 'success',
            msg: 'Testimony added successfully',
            id: result.insertId
          })
        })
      } else {
        res.status(401).send({
          type: 'failure',
          msg: 'No User exist with this user id'
        })
      }
    })
  })
})

// get all testimony
router.get('/alltestimony', VerifyToken, function (req, res, next) {
  User.selectById(req.userId, function (err, user) {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    Testimony.getAllTestimony('', (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        res.json(result)
      } else {
        res.json([])
      }
    })
  })
})

// get testimony by userId
router.get('/', VerifyToken, checkSchema({
  userId: {
    in: ['query'],
    errorMessage: 'Must be only numbers',
    isInt: true
  }
}), function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      type: 'failure',
      msg: 'Something went wrong!! Check the errors below',
      errors: errors.array()
    })
  }
  User.selectById(req.userId, async (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    const userCount = await userCheck(req.query.userId)
    if ((userCount > 0)) {
      Testimony.getTestimonyById(req.query.userId, (err, result) => {
        if (err) throw err
        if (result.length > 0) {
          res.json({
            type: 'success',
            msg: 'Got all testimonies successfully',
            result: result
          })
        } else {
          res.status(401).send({
            type: 'failure',
            msg: 'Not found'
          })
        }
      })
    } else {
      res.json({
        type: 'failure',
        msg: 'Incorrect data'
      })
    }
  })
})

module.exports = router
